package com.mtechsoft.socialceo.network;


import com.mtechsoft.socialceo.adapters.UpComingEventsResponseModel;
import com.mtechsoft.socialceo.model.ActiveUsersResponseModel;
import com.mtechsoft.socialceo.model.BuySellResponseModel;
import com.mtechsoft.socialceo.model.CommentResponseModel;
import com.mtechsoft.socialceo.model.ConnectPostResponseModel;
import com.mtechsoft.socialceo.model.CreatePostResponseModel;
import com.mtechsoft.socialceo.model.EventResponseModel;
import com.mtechsoft.socialceo.model.EventsResponseModel;
import com.mtechsoft.socialceo.model.GetCommentsResponseModel;
import com.mtechsoft.socialceo.model.GetPodCastResponseModel;
import com.mtechsoft.socialceo.model.LikeResponseModel;
import com.mtechsoft.socialceo.model.NearbyResponseModel;
import com.mtechsoft.socialceo.model.NetworkResponseModel;
import com.mtechsoft.socialceo.model.PodCastResponseModel;
import com.mtechsoft.socialceo.model.PostToSellResponseModel;
import com.mtechsoft.socialceo.model.RegisterResponseModel;
import com.mtechsoft.socialceo.model.SavePostResponseModel;
import com.mtechsoft.socialceo.model.StartupPostResponseModel;
import com.mtechsoft.socialceo.model.StartupResponseModel;
import com.mtechsoft.socialceo.model.UserDetailsResponseModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface GetDataService {

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("signup")
    Call<RegisterResponseModel> registerUser( @Field("name") String name,
                                              @Field("email") String email,
                                              @Field("phone") String phone,
                                              @Field("password") String password,
                                              @Field("password_confirmation") String password_confirmation);
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("login")
    Call<RegisterResponseModel> loginUser(    @Field("email") String email,
                                              @Field("password") String password,
                                              @Field("token") String token);
    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("verify_password")
    Call<RegisterResponseModel> verifyPassword(@Field("user_id") String user_id,
                                               @Field("password") String password);
    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("change_password")
    Call<RegisterResponseModel> changePassword(@Field("user_id") String user_id,
                                               @Field("password") String password,
                                               @Field("confirm_password") String confirm_password
    );

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("update_profile_image")
    Call<RegisterResponseModel>  updateUserProfileImage(@Field("user_id") String user_id,
                                                        @Field("image") String image

    );

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("create_post")
    Call<CreatePostResponseModel>  createPostForConnect(@Field("user_id") String user_id,
                                                      @Field("description") String description,
                                                      @Field("image") String image,
                                                      @Field("post_type") String post_type

    );

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("create_post")
    Call<CreatePostResponseModel>  createPostForNetwork(@Field("user_id") String user_id,
                                                        @Field("image") String image,
                                                        @Field("network_post_type") String network_post_type,
                                                        @Field("description") String description,
                                                        @Field("who_can_collaborate") String who_can_collaborate,
                                                        @Field("post_type") String post_type);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_connect_posts")
    Call<ConnectPostResponseModel> getConnectPosts(@Field("user_id") String user_id);

@Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("save_post")
    Call<SavePostResponseModel> savePosts(@Field("user_id") String user_id,
                                          @Field("post_id") String post_id);
@Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("nearby_members")
    Call<NearbyResponseModel> getallNearByUser (@Field("user_id") String user_id,
                                                @Field("latitude") String latitude,
                                                @Field("longitude") String longitude);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_saved_connect_posts")
    Call<ConnectPostResponseModel> getSavedConnectPosts(@Field("user_id") String user_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_network_posts")
    Call<NetworkResponseModel> getNetworkPosts(@Field("user_id") String user_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("forgot_password")
    Call<UserDetailsResponseModel> forgetPassword(@Field("email") String email);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("verify_code")
    Call<UserDetailsResponseModel> verifyCode(@Field("email") String email,
                                              @Field("code") String code);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("reset_password")
    Call<UserDetailsResponseModel> resetPassword(@Field("email") String email,
                                                 @Field("password") String password,
                                                 @Field("confirm_password") String confirm_password);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_saved_network_posts")
    Call<NetworkResponseModel> getSavedNetworkPosts(@Field("user_id") String user_id);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_buy_and_sell_posts")
    Call<BuySellResponseModel> getBuyAndSellPosts(@Field("user_id") String user_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_saved_buy_and_sell_posts")
    Call<BuySellResponseModel> getSavedBuyAndSellPosts(@Field("user_id") String user_id);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_events_posts")
    Call<EventsResponseModel> getEventPosts(@Field("user_id") String user_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_saved_events_posts")
    Call<EventsResponseModel> getSavedEventPosts(@Field("user_id") String user_id);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_startup_posts")
    Call<StartupResponseModel> getStartupPosts(@Field("user_id") String user_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_saved_startups_posts")
    Call<StartupResponseModel> getSavedStartupPosts(@Field("user_id") String user_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_saved_podcast_posts")
    Call<GetPodCastResponseModel> gteSavedPodCastPost(@Field("user_id") String user_id);

 @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("post_comments")
    Call<GetCommentsResponseModel> getComments(@Field("post_id") String post_id);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("like_post")
    Call<LikeResponseModel> likePost(@Field("liked_by") String user_id,
                                     @Field("post_id") String post_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("comment_on_post")
    Call<CommentResponseModel> commentOnPost (@Field("user_id") String user_id,
                                             @Field("post_id") String post_id,
                                             @Field("comment") String comment);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("active_members")
    Call<ActiveUsersResponseModel> getAllActiveUsers(@Field("user_id") String user_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("user_details")
    Call<UserDetailsResponseModel> getUserDetails(@Field("user_id") String user_id);


    @GET("upcoming_events")
    Call<UpComingEventsResponseModel> getUpComingEvents();

    @Headers({
            "Accept: application/json",
    })
    @Multipart
    @POST("create_post")
    Call<ResponseBody>  createPostToSell (
            @Part MultipartBody.Part file,
            @Part("user_id") RequestBody user_id,
            @Part("title") RequestBody title,
            @Part("category") RequestBody category,
            @Part("price") RequestBody price,
            @Part("link_of_product_or_service") RequestBody link_of_product_or_service,
            @Part("description") RequestBody description,
            @Part("post_type") RequestBody post_type,
            @Part("location") RequestBody location

    );

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("create_post")
    Call<PodCastResponseModel>  createPodCast(@Field("user_id") String user_id,
                                              @Field("image") String image,
                                              @Field("title") String title,
                                              @Field("category") String category,
                                              @Field("description") String link,
                                              @Field("tags") String tags,
                                              @Field("post_type") String post_type

    );



    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("create_post")
    Call<EventResponseModel>  createEvent     (@Field("user_id") String user_id,
                                               @Field("image") String image,
                                               @Field("title") String title,
                                               @Field("category") String category,
                                               @Field("price") String price,
                                               @Field("event_date") String event_date,
                                               @Field("description") String description,
                                               @Field("tags") String tags,
                                               @Field("post_type") String post_type,
                                               @Field("location") String location,
                                               @Field("available_seats") String available_seats


    );
    @FormUrlEncoded
    @POST("create_post")
    Call<StartupPostResponseModel>  createStartup     (@Field("user_id") String user_id,
                                                     @Field("image") String image,
                                                     @Field("title") String title,
                                                     @Field("startup_investment") String startup_investment,
                                                     @Field("pledge_goal_amount") String pledge_goal_amount,
                                                     @Field("startup_end_date") String startup_end_date,
                                                     @Field("description") String description,
                                                     @Field("investment_type") String investment_type,
                                                     @Field("next_step_for_startup") String next_step_for_startup,
                                                     @Field("feel_about_next_step") String feel_about_next_step,
                                                     @Field("startup_option_1") String startup_option_1,
                                                     @Field("startup_option_2") String startup_option_2,
                                                     @Field("startup_option_3") String startup_option_3,
                                                     @Field("how_far_along_startup") String how_far_along_startup,
                                                     @Field("money_need_for_startup") String money_need_for_startup,
                                                     @Field("planned_time_for_startup") String planned_time_for_startup,
                                                     @Field("do_you_have_enough_money") String do_you_have_enough_money,
                                                     @Field("access_to_startup_network") String access_to_startup_network,
                                                     @Field("service_fee") String service_fee,
                                                     @Field("post_type") String post_type,
                                                       @Field("location") String location

    );



    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("get_podcast_posts")
    Call<GetPodCastResponseModel> get_podcast_posts(@Field("user_id") String user_id
    );

    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("buy_and_sell_filter")
    Call<BuySellResponseModel> buyAndSellfilter (@Field("user_id") String user_id,
                                                    @Field("post_type") String post_type,
                                                    @Field("category") String category,
                                                    @Field("is_free") String is_free,
                                                    @Field("min_price") String min_price,
                                                    @Field("max_price") String max_price


    );

}
