package com.mtechsoft.socialceo.SMS;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdaptor extends RecyclerView.Adapter<ChatAdaptor.ChatHolder> {

    List<UserChat> list;
    OnItemClickListener onItemClickListener;
    Context context;

    public ChatAdaptor(List<UserChat> list, OnItemClickListener onItemClickListene, Context context) {
        this.list = list;
        this.onItemClickListener = onItemClickListene;
        this.context = context;
    }

    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chats_items, viewGroup, false);
        ChatHolder viewHolder = new ChatHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatHolder chatHolder, final int i) {

        chatHolder.user_name.setText(list.get(i).getUid());
        String message = list.get(i).getLastMessage();

        if (message.contains("image")) {
            chatHolder.user_message.setText("Photo");
            Drawable img = context.getResources().getDrawable(R.drawable.ic_camera_alt_black_24dp);
            img.setBounds(0, 0, 0, 0);
            //  chatHolder.user_message.setCompoundDrawables(img, null, null, null);
            // chatHolder.user_message.setCompoundDrawablePadding(10);

        }
//        else if (message.contains("video")) {
//            chatHolder.user_message.setText("Video");
//            Drawable img = context.getResources().getDrawable(R.drawable.ic_videocam_black_24dp);
//            // img.setBounds(0, 0, 60, 60);
//            // chatHolder.user_message.setCompoundDrawables(img, null, null, null);
//
//        }
        else {

            chatHolder.user_message.setText(list.get(i).getLastMessage());
        }

        try {

            String image = list.get(i).getUser_image();

            Glide.with(context)
                    .load(image)
                    .thumbnail(0.25f)
                    .error(R.drawable.ic_man)
                    .into(chatHolder.image);

//            Picasso.get().load(list.get(i).getUser_image())
//                    .error(R.drawable.ic_man)
//                    .into(chatHolder.image);

        } catch (Exception error) {
            error.printStackTrace();
        }



        if (list.get(i).getRead().equals("true")) {

            chatHolder.un_read_count.setVisibility(View.GONE);

        } else {
            chatHolder.un_read_count.setVisibility(View.VISIBLE);

        }
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        try {
            Date date = inputFormat.parse(list.get(i).getLastMessageTime());
            PrettyTime p = new PrettyTime();
            long millis = date.getTime();
            String datetime = p.format(new Date(millis));
            chatHolder.time.setText(datetime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        chatHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, i);
            }
        });

        chatHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ChatHolder extends RecyclerView.ViewHolder {
        public CircleImageView image;
        public ImageView un_read_count;
        public TextView user_name, user_message, time;


        public ChatHolder(@NonNull View itemView) {
            super(itemView);
            un_read_count = itemView.findViewById(R.id.un_read_count);
            image = itemView.findViewById(R.id.image);
            user_name = (TextView) itemView.findViewById(R.id.text1);
            user_message = itemView.findViewById(R.id.dtext1);
            time = itemView.findViewById(R.id.Ttx1);

        }
    }
}
