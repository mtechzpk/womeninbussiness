package com.mtechsoft.socialceo.SMS;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserChat implements Parcelable {

    @SerializedName("uid")
    @Expose
    private String uid = "";

    @SerializedName("unique_key")
    @Expose
    private String unique_key = "";

    @SerializedName("key")
    @Expose
    private String key = "";

    @SerializedName("user_image")
    @Expose
    private String user_image = "";


    @SerializedName("read")
    @Expose
    private String read = "";

    @SerializedName("token")
    @Expose
    private String token = "";


    @SerializedName("lastMessage")
    @Expose
    private String lastMessage = "";
    @SerializedName("unreadMessageCount")
    @Expose
    private String unreadMessageCount = "";
    @SerializedName("lastMessageTime")
    @Expose
    private String lastMessageTime = "";

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getUnreadMessageCount() {
        return unreadMessageCount;
    }

    public void setUnreadMessageCount(String unreadMessageCount) {
        this.unreadMessageCount = unreadMessageCount;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public String getUser_image() {
        return user_image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static Creator<UserChat> getCREATOR() {
        return CREATOR;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.lastMessage);
        dest.writeString(this.unreadMessageCount);
        dest.writeString(this.lastMessageTime);
        dest.writeString(this.user_image);
        dest.writeString(this.unique_key);
        dest.writeString(this.token);
    }

    public UserChat() {
    }


    protected UserChat(Parcel in) {
        this.uid = in.readString();
        this.lastMessage = in.readString();
        this.unreadMessageCount = in.readString();
        this.lastMessageTime = in.readString();
        this.user_image = in.readString();
        this.unique_key = in.readString();
        this.token = in.readString();
    }


    public String getUnique_key() {
        return unique_key;
    }

    public void setUnique_key(String unique_key) {
        this.unique_key = unique_key;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public static final Creator<UserChat> CREATOR = new Creator<UserChat>() {
        @Override
        public UserChat createFromParcel(Parcel source) {
            return new UserChat(source);
        }

        @Override
        public UserChat[] newArray(int size) {
            return new UserChat[size];
        }
    };

}
