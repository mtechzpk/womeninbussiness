package com.mtechsoft.socialceo.SMS;

import android.view.View;

public interface OnItemClickListener {

    void onItemClick(View item, int position);
}
