package com.mtechsoft.socialceo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.utilities.Utilities;

public class StartupNextStepActivity extends AppCompatActivity {

    Button button_continue;
    ImageView back;
    RadioGroup radioGroupNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup_next_step);

        button_continue = (Button) findViewById(R.id.button_continue);
        back = (ImageView) findViewById(R.id.button_back);
        radioGroupNext = findViewById(R.id.radio_group_next);

        radioGroupNext.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = findViewById(checkedId);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedId = radioGroupNext.getCheckedRadioButtonId();
                if (selectedId == -1){
                    Toast.makeText(StartupNextStepActivity.this, "Select Any Option", Toast.LENGTH_SHORT).show();
                }else
                {
                    RadioButton radioButton = findViewById(selectedId);
                    Toast.makeText(StartupNextStepActivity.this, radioButton.getText(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(),PostStartActivity.class);
                    String text = radioButton.getText().toString();
                    Utilities.saveString(getApplicationContext(),"nextStep",text);
                    startActivity(intent);
                }

            }
        });

    }
}