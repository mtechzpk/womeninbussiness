package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.UserProfilePostsAdapter;
import com.mtechsoft.socialceo.model.SinglePostModel;
import com.mtechsoft.socialceo.model.UserDetailedDataModel;
import com.mtechsoft.socialceo.model.UserDetailsResponseModel;
import com.mtechsoft.socialceo.model.UserProfilePostsModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends AppCompatActivity {
    private RecyclerView recyclerView_userPosts;
    private RecyclerView.Adapter adapter;
    private List<UserProfilePostsModel> listItems;
    private CircleImageView img_userProfile;
    TextView text_userName, text_userEmail, text_PostsTotal;
    GetDataService service;
    String userId;
    ProgressDialog progressDialog;
    ImageView back;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        userId = String.valueOf( getIntent().getIntExtra("userId", 0));
        requestForUserPosts(userId);
        progressDialog = new ProgressDialog(UserProfileActivity.this);
        progressDialog.setMessage("Loading userPosts....");
        progressDialog.show();
        recyclerView_userPosts = findViewById(R.id.recyclerView_userPosts);
        img_userProfile = findViewById(R.id.img_userProfile);
        back = (ImageView) findViewById(R.id.back_ic);
        text_userName = findViewById(R.id.text_userName);
        text_userEmail = findViewById(R.id.text_userEmail);
        text_PostsTotal = findViewById(R.id.text_PostsTotal);

        back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
    }


    private void requestForUserPosts(String userId) {

        Call<UserDetailsResponseModel> call = service.getUserDetails(userId);

        call.enqueue(new Callback<UserDetailsResponseModel>() {
            @Override
            public void onResponse(Call<UserDetailsResponseModel> call, Response<UserDetailsResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                UserDetailedDataModel data = response.body().getData();
                List<SinglePostModel> dataList = data.getUserPosts();

                if (status == 200){
                    showUserPosts(recyclerView_userPosts, dataList);
                    text_PostsTotal.setText(String.valueOf(dataList.size()));
                    text_userEmail.setText(data.getUserEmail());
                    text_userName.setText(data.getUserName());

                    Glide.with(getApplicationContext())
                            .load(Images_BaseUrl+data.getProfile_image())
                            .fitCenter()
                            .placeholder(R.drawable.ariana_user_ic)
                            .into(img_userProfile);

                }
                Toast.makeText(UserProfileActivity.this, response.body().getMessage(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<UserDetailsResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(UserProfileActivity.this, t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showUserPosts(RecyclerView recyclerView_userPosts, List<SinglePostModel> listItems) {
        recyclerView_userPosts.setHasFixedSize(true);
        recyclerView_userPosts.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new UserProfilePostsAdapter(listItems, this);
        recyclerView_userPosts.setAdapter(adapter);
    }
}