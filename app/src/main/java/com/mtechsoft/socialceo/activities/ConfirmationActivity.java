package com.mtechsoft.socialceo.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.StartupPostResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmationActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    Button button_continue;
    ImageView back;
    TextView textView_investment,textView_goal,textView_endDate,textView_fees;
    String location,userId,postImageUrl,title,investment,goal,desc,endDate,nextStep,nextFeel,option1,option2,option3,alongis,moneyPlan,timePlan,fundPlan,networkPlan,serviceFees,type,investmentType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        button_continue =  findViewById(R.id.button_continue_startup);
        back =  findViewById(R.id.button_back_startup);
        textView_fees = findViewById(R.id.serviceFees);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating Post...");

        textView_endDate = findViewById(R.id.tv_EndDate);
        textView_goal = findViewById(R.id.tv_goal);
        textView_investment = findViewById(R.id.tv_investment);



        userId = String.valueOf(Utilities.getInt(getApplicationContext(), "userId"));
        postImageUrl = Utilities.getString(getApplicationContext(),"postImageUrl");
        title = Utilities.getString(getApplicationContext(),"title");
        investment = Utilities.getString(getApplicationContext(),"investment");
        goal = Utilities.getString(getApplicationContext(),"goal");
        desc = Utilities.getString(getApplicationContext(),"desc");
        endDate = Utilities.getString(getApplicationContext(),"endDate");
        nextStep = Utilities.getString(getApplicationContext(),"nextStep");
        nextFeel = Utilities.getString(getApplicationContext(),"nextFeel");
        option1 = Utilities.getString(getApplicationContext(),"corporated");
        option2 = Utilities.getString(getApplicationContext(),"cofounders");
        option3 = Utilities.getString(getApplicationContext(),"funding");
        alongis = Utilities.getString(getApplicationContext(),"alongis");
        moneyPlan = Utilities.getString(getApplicationContext(),"moneyPlan");
        timePlan = Utilities.getString(getApplicationContext(),"timePlan");
        fundPlan = Utilities.getString(getApplicationContext(),"fundPlan");
        networkPlan = Utilities.getString(getApplicationContext(),"networkPlan");
        investmentType = Utilities.getString(getApplicationContext(),"investmentType");
        location = Utilities.getString(getApplicationContext(),"location_startup");
        serviceFees = textView_fees.getText().toString();
        type="startup";


        textView_investment.setText(investment);
        textView_goal.setText(goal);
        textView_endDate.setText(endDate);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<StartupPostResponseModel> call = service.createStartup(userId,postImageUrl,title,investment,goal,endDate,desc,investmentType,
                        nextStep,nextFeel,option1,option2,option3,alongis,moneyPlan,timePlan,fundPlan,networkPlan,serviceFees,type,location);

                call.enqueue(new Callback<StartupPostResponseModel>() {
                    @Override
                    public void onResponse(Call<StartupPostResponseModel> call, Response<StartupPostResponseModel> response) {
                        progressDialog.dismiss();
                        int status = response.body().getStatus();
                        if (status == 200 )
                        {
                            Toast.makeText(ConfirmationActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ConfirmationActivity.this,MainActivity.class);
                            Utilities.saveInt(ConfirmationActivity.this, "Network", 4);
                            startActivity(intent);
                            finish();
                        }
                        else {
                            progressDialog.dismiss();
                            Toast.makeText(ConfirmationActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<StartupPostResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        t.printStackTrace();
                    }
                });

            }
        });
    }
}