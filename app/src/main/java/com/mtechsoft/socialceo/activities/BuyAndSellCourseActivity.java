package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.CourseAdapter;
import com.mtechsoft.socialceo.adapters.UserDetailsReviewTabAdapter;
import com.mtechsoft.socialceo.model.CourseModel;
import com.google.android.material.tabs.TabLayout;
import com.mtechsoft.socialceo.model.SavePostResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyAndSellCourseActivity extends AppCompatActivity {
    Button btn_BuyNow;
    TabLayout tabLayout;
    ViewPager viewPagerDetailsReviews;
    private ViewPager ViewPager_courseImage;
    private CourseAdapter adapter;
    private List<CourseModel> listItems;
    private  ImageView courseImage,back_ic;
    ImageButton bookmark;
    CircleImageView img_userProfile;
    TextView text_Location_sell,text_courseName,text_coursePrice,text_userName,text_activeAgo;
    String image,title,price,userName,userActive,userImage,location_sell,postId,userId,isSaved,link;
    String save_status = "true";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_and_sell_course);
        back_ic = (ImageView) findViewById(R.id.back_ic);
        userId = String.valueOf(Utilities.getInt(getApplicationContext(), "userId"));

        courseImage = findViewById(R.id.courseImage);
        text_courseName = findViewById(R.id.text_courseName);
        text_coursePrice = findViewById(R.id.text_coursePrice);
        text_userName = findViewById(R.id.text_userName);
        text_activeAgo = findViewById(R.id.text_activeAgo);
        text_Location_sell = findViewById(R.id.text_Location_sell);
        img_userProfile = findViewById(R.id.img_userProfile);
        bookmark = findViewById(R.id.btn_Save);

        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        image = intent.getStringExtra("postImage");
        title = intent.getStringExtra("title");
        price = intent.getStringExtra("price");
        userName = intent.getStringExtra("userName");
        userActive = intent.getStringExtra("hours");
        userImage = intent.getStringExtra("userProfile");
        location_sell = intent.getStringExtra("location_sell");
        postId = intent.getStringExtra("postId");
        isSaved = intent.getStringExtra("isSaved");
        link = intent.getStringExtra("link");


        checkForSaveStatus();

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                savePost(userId, postId);
                if (save_status.equals("false")) {
                    bookmark.setImageResource(R.drawable.fillded_save_ic);
                    save_status = "true";
                } else {
                    bookmark.setImageResource(R.drawable.bookmark_ic);
                    save_status = "false";
                }
            }
        });

        Glide.with(this).load(image).centerCrop().into(courseImage);
        Glide.with(this).load(userImage).into(img_userProfile);

        text_userName.setText(userName);

        if (price.equals("free")) {
            text_coursePrice.setText(price);
        }else
        {
            text_coursePrice.setText("$"+price);
        }
        text_courseName.setText(title);
        text_Location_sell.setText(location_sell);


        Utilities.TimeAgo2 timeAgo2 = new Utilities.TimeAgo2();
        String MyFinalValue = timeAgo2.covertTimeToText(userActive);
        text_activeAgo.setText(MyFinalValue);


        // Setting tab bar
        tabLayout = (TabLayout)findViewById(R.id.tabLayout);
        viewPagerDetailsReviews = (ViewPager)  findViewById(R.id.viewPagerDetailsReviews);
        UserDetailsReviewTabAdapter myPagerAdapter = new UserDetailsReviewTabAdapter(getSupportFragmentManager());
        viewPagerDetailsReviews.setAdapter(myPagerAdapter);
        tabLayout.setupWithViewPager(viewPagerDetailsReviews);

        // Show You are secure Alert
        btn_BuyNow = findViewById(R.id.btn_BuyNow);
        btn_BuyNow.setOnClickListener(v -> {

            Uri uri = Uri.parse(link); // missing 'http://' will cause crashed
            Intent linkInent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(linkInent);
            
        });
    }

    private void savePost(String userId, String postId) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<SavePostResponseModel> modelCall = service.savePosts(userId,postId);
        modelCall.enqueue(new Callback<SavePostResponseModel>() {
            @Override
            public void onResponse(Call<SavePostResponseModel> call, Response<SavePostResponseModel> response) {
                int status = response.body().getStatus();
                if (status == 200){
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<SavePostResponseModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
    private void checkForSaveStatus() {
        if (isSaved.equals("true")) {
            bookmark.setImageResource(R.drawable.fillded_save_ic);
        } else if (isSaved.equals("false")) {
            bookmark.setImageResource(R.drawable.bookmark_ic);
        }
    }
    //    public void showCourseImage(ViewPager viewPager, TextView textView){
//        listItems = new ArrayList<>();
//        for (int i = 0; i <= 3; i++) {
//            CourseModel listItem = new CourseModel();
//            listItem.setCourseImage(R.drawable.post_laptop);
//            listItems.add(listItem);
//        }
//
//        textView.setText("/" + listItems.size());
//        adapter = new CourseAdapter(listItems, this);
//        viewPager.setAdapter(adapter);
//    }
}