package com.mtechsoft.socialceo.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.RegisterResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();
    public static final int REQUEST_IMAGE = 100;
    String userProfileImage;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";
    // declare Shared Preferences Variables here
    String userEmail, userName, userPhone;
    GetDataService service;
    ProgressDialog progressDialog;
    CoordinatorLayout parentView;
    AlertDialog passwordChangedAlert;
    ImageView verifyPasswordCross, arrow_changePassword, changePasswordCross, edit_ProfileButton;
    View verifyPasswordSheet, changePasswordBottomSheet;
    Button btn_Verify, btn_Save, btn_Done;
    TextView text_userName, text_UserMainName, text_userEmail, text_userPhoneNumber, text_LogOut;
    CircleImageView userProfile;
    BottomSheetBehavior verifyPasswordBottomBehavior, changePasswordBottomBehavior;
    EditText verifyPassword_Field, edit_EnterNewPassword, edit_ConfirmPassword;
    String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        parentView = findViewById(R.id.parentView);
        initUi();
        userId = String.valueOf(Utilities.getInt(getApplicationContext(), "userId"));
        progressDialog = new ProgressDialog(SettingsActivity.this);
        progressDialog.setMessage("Loading....");
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        verifyPasswordCross = findViewById(R.id.cross_btn);
        verifyPassword_Field = findViewById(R.id.edit_enterPassword);
        edit_EnterNewPassword = findViewById(R.id.edit_EnterNewPassword);
        edit_ConfirmPassword = findViewById(R.id.edit_ConfirmPassword);
        text_LogOut = findViewById(R.id.text_LogOut);
        edit_ProfileButton = findViewById(R.id.edit_ProfileButton);
        edit_ProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(SettingsActivity.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    showImagePickerOptions();
                                }

                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
            }
        });
        // Setting UserProfileImage Here
        String userProfileImage = Utilities.getString(SettingsActivity.this, "userProfileImage");
        userProfile = findViewById(R.id.userProfileImage);
        if (!userProfileImage.isEmpty()){
            Glide.with(getApplicationContext())
                    .load(userProfileImage)
                    .fitCenter()
                    .into(userProfile);
        }

        text_LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });

        verifyPasswordSheet = findViewById(R.id.layout_VerifyPassword);
        verifyPasswordSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                return;
            }
        });
        changePasswordBottomSheet = findViewById(R.id.layout_ChangePassword);
        changePasswordBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                return;
            }
        });
        arrow_changePassword = findViewById(R.id.arrow_changePassword);
        changePasswordCross = findViewById(R.id.crossBtn_ChangePassword);
        btn_Save = findViewById(R.id.btn_Save);
        btn_Verify = findViewById(R.id.btn_Verify);
        verifyPasswordBottomBehavior = BottomSheetBehavior.from(verifyPasswordSheet);
        changePasswordBottomBehavior = BottomSheetBehavior.from(changePasswordBottomSheet);

        arrow_changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verifyPasswordBottomBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    verifyPasswordBottomBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    verifyPasswordBottomBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });
        verifyPasswordCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyPasswordBottomBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        btn_Verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyPasswordBottomBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                Utilities.hideKeyboard(v, getApplicationContext());
                String userPasswordField = verifyPassword_Field.getText().toString();
                if (userPasswordField.isEmpty() ){
                    Snackbar.make(parentView, "Please Enter Your Previous Password", Snackbar.LENGTH_LONG).show();
                }
                else {
                    requestForVerifyPassword(userId, userPasswordField);
                    verifyPassword_Field.setText("");
                }
            }
        });

        changePasswordCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePasswordBottomBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        btn_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePasswordBottomBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                Utilities.hideKeyboard(v, getApplicationContext());
                String newPassword = edit_EnterNewPassword.getText().toString();
                String confirmPassword = edit_ConfirmPassword.getText().toString();
                if (newPassword.isEmpty() || confirmPassword.isEmpty()){
                    Snackbar.make(parentView, "Please Enter All Fields", Snackbar.LENGTH_LONG).show();
                }
                else {
                    requestForChangePassword(userId, newPassword, confirmPassword );
                    edit_EnterNewPassword.setText("");
                    edit_ConfirmPassword.setText("");
                }
            }
        });
    }
    //Image Capture Code


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                String imageEncoded, input;
                Bitmap bitmapImage = null;
                // You can update this bitmap to your server
                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                input = imageEncoded;
                input = input.replace("\n", "");
                input = input.trim();
                input = "data:image/png;base64,"+input;
                // loading profile image from local cache
                assert uri != null;
                loadPhotos(uri.toString());
                userProfileImage = input;

                updateUserProfileImage(userId, userProfileImage);
            }
        }
    }
    private void loadPhotos(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(userProfile);
        userProfile.setColorFilter(ContextCompat.getColor(SettingsActivity.this, android.R.color.transparent));

    }
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(SettingsActivity.this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }
    private void launchCameraIntent() {
        Intent intent = new Intent(SettingsActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void launchGalleryIntent() {
        Intent intent = new Intent(SettingsActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", SettingsActivity.this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    public void showPasswordChangedSuccessfullyAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_password_change, null);
        builder.setView(customLayout);
        passwordChangedAlert= builder.create();
        passwordChangedAlert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        passwordChangedAlert.show();
        btn_Done = customLayout.findViewById(R.id.btn_Done);
        btn_Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
    }
    private void initUi(){
        userEmail = Utilities.getString(getApplicationContext(), "userEmail");
        userName = Utilities.getString(getApplicationContext(), "userName");
        userPhone = Utilities.getString(getApplicationContext(), "userPhone");
        text_userName = findViewById(R.id.text_userName);
        text_userName.setText(userName);
        text_UserMainName = findViewById(R.id.text_UserMainName);
        text_UserMainName.setText(userName);
        text_userEmail =  findViewById(R.id.text_userEmail);
        text_userEmail.setText(userEmail);
        text_userPhoneNumber =  findViewById(R.id.text_userPhoneNumber);
        text_userPhoneNumber.setText(userPhone);

    }
    private void requestForVerifyPassword(String userId, String password) {

        Call<RegisterResponseModel> call = service.verifyPassword( userId, password);

        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    Snackbar.make(parentView, response.body().getMessage() + "Success" , Snackbar.LENGTH_LONG).show();
                    if (changePasswordBottomBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                        changePasswordBottomBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        changePasswordBottomBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                }
                else if (status == 400){
                    Snackbar.make(parentView, response.body().getMessage()  + "Failed" , Snackbar.LENGTH_LONG).show();
                }

            }
            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void requestForChangePassword(String userId, String newPassword, String confirmPassword) {

        Call<RegisterResponseModel> call = service.changePassword( userId, newPassword, confirmPassword);

        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    Snackbar.make(parentView, response.body().getMessage() + "Success" , Snackbar.LENGTH_LONG).show();
                    showPasswordChangedSuccessfullyAlert();
                }
                else if (status == 400){
                    Snackbar.make(parentView, response.body().getMessage()  + "Failed" , Snackbar.LENGTH_LONG).show();
                }

            }
            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }
    public void logoutUser(){
        Utilities.clearSharedPref(getApplicationContext());
        Intent mainIntent = new Intent(SettingsActivity.this, SocialConnectActivity.class);
        startActivity(mainIntent);
        finish();
    }


    private void updateUserProfileImage(String userId, String imageUrl) {
        progressDialog.setMessage("Up loading Your Profile....");
        progressDialog.show();
        Call<RegisterResponseModel> call = service.updateUserProfileImage( userId, imageUrl);

        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    Snackbar.make(parentView, response.body().getMessage() + "Success" , Snackbar.LENGTH_LONG).show();
                    String userProfileImage = response.body().getData().getProfile_image();
                    if (userProfileImage != null && !userProfileImage.isEmpty()){
                        Utilities.saveString(SettingsActivity.this,"userProfileImage",Images_BaseUrl+userProfileImage);
                    }
                }
                else if (status == 400){
                    Snackbar.make(parentView, response.body().getMessage()  + "Failed" , Snackbar.LENGTH_LONG).show();
                }

            }
            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }
}