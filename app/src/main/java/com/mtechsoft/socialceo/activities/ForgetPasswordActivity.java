package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.UserDetailsResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {

    EditText forget_Email;
    Button btn_Send;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        forget_Email = findViewById(R.id.forget_Email);
        btn_Send = findViewById(R.id.btn_Send);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading..");


        btn_Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = forget_Email.getText().toString().trim();
                if (email.isEmpty())
                {
                    forget_Email.setError("Please Enter Valid Email");
                }
                else
                {
                    progressDialog.show();
                    sendVerifyCode(email);
                }
            }
        });
    }

    private void sendVerifyCode(String email) {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<UserDetailsResponseModel> call = service.forgetPassword(email);
        call.enqueue(new Callback<UserDetailsResponseModel>() {
            @Override
            public void onResponse(Call<UserDetailsResponseModel> call, Response<UserDetailsResponseModel> response) {

                progressDialog.dismiss();
                int status = response.body().getStatus();
                if (status == 200)
                {
                    Toast.makeText(ForgetPasswordActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ForgetPasswordActivity.this,VerificationCodeActivity.class);

                    Utilities.saveString(ForgetPasswordActivity.this,"forget_email",email);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(ForgetPasswordActivity.this, "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserDetailsResponseModel> call, Throwable t) {

                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }
}