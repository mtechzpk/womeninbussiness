package com.mtechsoft.socialceo.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.utilities.GPSTracker;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;

    NavController navController;
    ImageView ivDrawer;
    private DrawerLayout drawerLayout;
    RelativeLayout rlToolbar, main_Container;
    NavigationView navigationView;
    private static final float END_SCALE = 0.7f;
    CircleImageView user_ic;
    String latitude,longitude,country,postalCode,city,address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.bottom_Nav);
        drawerLayout = findViewById(R.id.drawer_layout);
        rlToolbar = findViewById(R.id.layout_topBar);
        main_Container = findViewById(R.id.main_Container);
        navigationView = findViewById(R.id.drawer_Nav);
        ivDrawer = findViewById(R.id.menu_ic);
        user_ic = findViewById(R.id.user_ic);
        initNavigation();





        // Setting userName, ProfileImage and UserEmail on Nav Header View

        View headerView =  navigationView.getHeaderView(0);
        TextView userName = headerView.findViewById(R.id.text_userName);
        TextView userEmail = headerView.findViewById(R.id.text_userEmail);
        String userProfileImage = Utilities.getString(MainActivity.this, "userProfileImage");
        userName.setText(Utilities.getString(MainActivity.this, "userName"));
        userEmail.setText(Utilities.getString(MainActivity.this, "userEmail"));
        CircleImageView  userProfile = headerView.findViewById(R.id.userProfileImage);
        // Set User Profile Images here
        if (!userProfileImage.isEmpty()){
            Glide.with(getApplicationContext())
                    .load(userProfileImage)
                    .fitCenter()
                    .into(userProfile);
            Glide.with(getApplicationContext())
                    .load(userProfileImage)
                    .fitCenter()
                    .into(user_ic);
        }

        // Scale the View based on current slide offset
        // Translate the View, accounting for the scaled width

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.open,
                R.string.closed
        ) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // Scale the View based on current slide offset
                final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                final float offsetScale = 1 - diffScaledOffset;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    main_Container.setScaleX(offsetScale);
                    main_Container.setScaleY(offsetScale);
                }


                // Translate the View, accounting for the scaled width
                final float xOffset = drawerView.getWidth() * slideOffset;
                final float yOffset = drawerView.getHeight() * slideOffset;

                final float xOffsetDiff = main_Container.getWidth() * diffScaledOffset / 2;
                final float xTranslation = xOffset - xOffsetDiff;
                final float yOffsetDiff = main_Container.getHeight() * diffScaledOffset / 15;
                final float yTranslation = yOffset - yOffsetDiff;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    main_Container.setTranslationX(xTranslation);
                }
            }
        };

        drawerLayout.addDrawerListener(mDrawerToggle);

        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        /*Remove navigation drawer shadow/fadding*/
        drawerLayout.setDrawerElevation(0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawerLayout.setElevation(0f);
            main_Container.setElevation(10.0f);
        }

    }
    private void initNavigation() {
        ivDrawer.setOnClickListener(v -> drawerLayout.openDrawer(GravityCompat.START, true));
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);


        navigationView.getMenu().findItem(R.id.navigation_settings).setOnMenuItemClickListener(item -> {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            drawerLayout.closeDrawer(GravityCompat.START, false);
            return true;
        });

        navigationView.getMenu().findItem(R.id.navigation_profile).setOnMenuItemClickListener(item -> {
            startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
            drawerLayout.closeDrawer(GravityCompat.START, false);
            return true;
        });
        navigationView.getMenu().findItem(R.id.navigation_privacy).setOnMenuItemClickListener(item -> {
            startActivity(new Intent(MainActivity.this, PrivacyPolicyActivity.class));
            drawerLayout.closeDrawer(GravityCompat.START, false);
            return true;
        });
        navigationView.getMenu().findItem(R.id.navigation_history).setOnMenuItemClickListener(item -> {
            startActivity(new Intent(MainActivity.this, BuyAndSellHistoryActivity.class));
            drawerLayout.closeDrawer(GravityCompat.START, false);
            return true;
        });

        navigationView.getMenu().findItem(R.id.navigation_savedpost).setOnMenuItemClickListener(item -> {
            startActivity(new Intent(MainActivity.this, SavedPostsActivity.class));
            drawerLayout.closeDrawer(GravityCompat.START, false);
            return true;
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
}