package com.mtechsoft.socialceo.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.PostToSellResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostForSaleActivity extends AppCompatActivity {

    private static final String TAG = PostForNetworkActivity.class.getSimpleName();
    public static final int REQUEST_IMAGE = 100;
    String userId, userEmail, postImageUrl, category;
    MaterialSpinner spinner_Category;
    Button btn_PostForSale;
    private ImageView upload_ImageForSale,back_ic;
    File filee;
    String urii = "";

    EditText edit_ProductName, edit_Price,edit_LinkOfProduct,edit_Description,edit_location;
    CheckBox checkbox_Free;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_for_sale);
        spinner_Category = findViewById(R.id.spinner_Category);
        btn_PostForSale = findViewById(R.id.btn_PostForSale);

        edit_ProductName = findViewById(R.id.edit_ProductName);
        edit_Price = findViewById(R.id.edit_Price);
        edit_LinkOfProduct = findViewById(R.id.edit_LinkOfProduct);
        edit_Description =findViewById(R.id.edit_Description);
        checkbox_Free = findViewById(R.id.checkbox_Free);
        back_ic = (ImageView) findViewById(R.id.back_ic);

        back_ic.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });

        spinner_Category.setItems("Courses", "Digital Products", "Marketing Services", "Programs", "Other");
        userId = String.valueOf(Utilities.getInt(PostForSaleActivity.this, "userId"));
        spinner_Category.setOnItemSelectedListener((MaterialSpinner.OnItemSelectedListener<String>) (view, position, id, item) ->
                category = item
        );

        btn_PostForSale.setOnClickListener(v -> {
            Log.d ("PostForSale","Button Clicked");
            String productName = edit_ProductName.getText().toString();
            String price = edit_Price.getText().toString();
            String link = edit_LinkOfProduct.getText().toString();
            String desc = edit_Description.getText().toString();

            if (checkbox_Free.isChecked() && price.isEmpty())
            {
                Log.d ("PostForSale","price runing");
                price = "free";
            }
            else if(price.isEmpty())
                {
                    edit_Price.setError("Enter Price");
                }

            if (productName.isEmpty()){
                edit_ProductName.setError("Enter Product Name");
            }
            if (link.isEmpty()){
                edit_LinkOfProduct.setError("Enter Link");
            }
            if (desc.isEmpty()){
                edit_Description.setError("Enter Description");
            }
            else {
                //Log.d ("PostForSale","else runing");
                Intent intent = new Intent(getApplicationContext(),PostForSaleConfirmationActivity.class);
                intent.putExtra("postImage", urii);
                intent.putExtra("userId", userId);
                intent.putExtra("category", category);
                intent.putExtra("productName", productName);
                intent.putExtra("price", price);
                intent.putExtra("link", link);
                intent.putExtra("desc", desc);
                intent.putExtra("type", "buy_and_sell");
                startActivity(intent);
                //finish ();
            }

        });

        upload_ImageForSale = findViewById(R.id.upload_Image);

        upload_ImageForSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                galleryIntent.setType("image/*,video/*");
                startActivityForResult(galleryIntent, REQUEST_IMAGE);
            }
        });

//        upload_ImageForSale.setOnClickListener(v -> Dexter.withContext(PostForSaleActivity.this)
//                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                .withListener(new MultiplePermissionsListener() {
//                    @Override
//                    public void onPermissionsChecked(MultiplePermissionsReport report) {
//                        if (report.areAllPermissionsGranted()) {
//                            showImagePickerOptions();
//                        }
//
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//                            showSettingsDialog();
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
//                }).check());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Uri uri = data.getData();
                urii = uri.toString();
//                if (urii.contains("image")){
//
//                    filee = new File(uri.getPath());
//
//                }else {
//
//                    filee = new File(uri.getPath());
//                }

//                String imageEncoded, input;
//                Bitmap bitmapImage = null;
//
//
//                // You can update this bitmap to your server
//                try {
//                    bitmapImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                assert bitmapImage != null;
//                bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//                byte[] b = baos.toByteArray();
//                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
//
//                input = imageEncoded;
//                input = input.replace("\n", "");
//                input = input.trim();
//                input = "data:image/png;base64,"+input;
//                // loading profile image from local cache
//                assert uri != null;
                loadPhotos(uri.toString());
//                postImageUrl = input;

            }
        }
    }
    private void loadPhotos(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(upload_ImageForSale);
        upload_ImageForSale.setColorFilter(ContextCompat.getColor(PostForSaleActivity.this, android.R.color.transparent));

    }
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(PostForSaleActivity.this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }
    private void launchCameraIntent() {
        Intent intent = new Intent(PostForSaleActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void launchGalleryIntent() {
        Intent intent = new Intent(PostForSaleActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(PostForSaleActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", PostForSaleActivity.this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
    @Override
    protected void onStop() {
        super.onStop ();

    }
}

