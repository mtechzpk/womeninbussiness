package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.SavePostResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PodCastDetailActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView_name,textView_tags;
    String userId,isSaved,postEventId,imageEventDetails,nameEventDetails,priceEventDetails,dateEventDetails,tagsEventDetails,locationEventDetails,seatEventDetails;
    String save_status = "false";
    ImageButton bookmark;

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pod_cast_detail);


        imageView = findViewById(R.id.podCast_detail_Image);
        textView_name = findViewById(R.id.podCast_detail_Name);
        textView_tags = findViewById(R.id.podCast_tags_event);
        bookmark = findViewById(R.id.btn_Save_podCast);

        userId = String.valueOf(Utilities.getInt(getApplicationContext(), "userId"));

        imageEventDetails = Utilities.getString(getApplicationContext(),"imagePodCastDetails");
        nameEventDetails = Utilities.getString(getApplicationContext(),"namePodCast");
//        priceEventDetails = Utilities.getString(getApplicationContext(),"priceEventDetails");
//        dateEventDetails = Utilities.getString(getApplicationContext(),"dateEventDetails");
        tagsEventDetails = Utilities.getString(getApplicationContext(),"tagsPodDetails");
//        locationEventDetails = Utilities.getString(getApplicationContext(),"locationEventDetails");
//        seatEventDetails = Utilities.getString(getApplicationContext(),"seatEventDetails");
        postEventId = Utilities.getString(getApplicationContext(),"podCastId");
        isSaved = Utilities.getString(getApplicationContext(),"isSavedPodCast");

        Glide.with(this).load(imageEventDetails).centerCrop().into(imageView);

        textView_name.setText(nameEventDetails);
        textView_tags.setText(tagsEventDetails);
//        textView_price.setText(priceEventDetails);
//        textView_location.setText(locationEventDetails);
//        textView_seats.setText(seatEventDetails);
//
//        DateFormat theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = null;
//
//        try {
//            date = theDateFormat.parse(dateEventDetails);
//        } catch (ParseException parseException) {
//            // Date is invalid. Do what you want.
//        } catch (Exception exception) {
//            // Generic catch. Do what you want.
//        }
//
//        theDateFormat = new SimpleDateFormat("MMM dd, yyyy");
//        String[] arrayString = dateEventDetails.split(";");
//
//        String time = arrayString[0];
//
////        time = time.substring(time.indexOf("2020-10-12T") + 12, time.length());
////        tvDate.setText(theDateFormat.format(date));
//        DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
//        Date d = null;
//        try {
//            d = f1.parse(time)
//            ;
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        DateFormat f2 = new SimpleDateFormat("h:mm a");
////        String timeFormate=f2.format(d).toLowerCase(); // "12:18am"
//
//        textView_date.setText(theDateFormat.format(date));


        checkForSaveStatus();

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                savePost(userId,postEventId);

                if (save_status.equals("false")) {
                    bookmark.setImageResource(R.drawable.fillded_save_ic);
                    save_status = "true";
                } else {
                    bookmark.setImageResource(R.drawable.bookmark_ic);
                    save_status = "false";
                }
            }

        });
    }

    private void checkForSaveStatus() {
        if (isSaved.equals("true")) {
            bookmark.setImageResource(R.drawable.fillded_save_ic);
        } else if (isSaved.equals("false")) {
            bookmark.setImageResource(R.drawable.bookmark_ic);
        }
    }

    private void savePost(String userId, String postId) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<SavePostResponseModel> modelCall = service.savePosts(userId,postId);
        modelCall.enqueue(new Callback<SavePostResponseModel>() {
            @Override
            public void onResponse(Call<SavePostResponseModel> call, Response<SavePostResponseModel> response) {
                int status = response.body().getStatus();
                if (status == 200){
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                    Log.d("TAG",response.body ().getMessage ());

//                    if (isSaved !=null && isSaved.equals("true")){
//                        bookmark.setImageResource(R.drawable.fillded_save_ic);
//                    }
//                    else {
//                        bookmark.setImageResource(R.drawable.bookmark_ic);
//                    }
                }
            }

            @Override
            public void onFailure(Call<SavePostResponseModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}