package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class CollaborateMessageActivity extends AppCompatActivity {
    TextView text_UserName;
    CircleImageView img_userProfile;
    ImageView back_ic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collaborate_message);

        back_ic=findViewById (R.id.back_ic);
        String userName = getIntent().getStringExtra("userName");
        String userProfile = getIntent().getStringExtra("userProfile");
        text_UserName = findViewById(R.id.text_UserName);
        text_UserName.setText(userName);
        img_userProfile = findViewById(R.id.img_userProfile);
        back_ic.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        Glide.with(CollaborateMessageActivity.this).load(userProfile).fitCenter().into(img_userProfile);
    }
}