package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.RegisterResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.TextUtilities;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.google.android.material.snackbar.Snackbar;
import com.shasin.notificationbanner.Banner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {
    TextView txtSignUp, text_ForgetPassword;
    Button btnSignIn;
    GetDataService service;
    ProgressDialog progressDialog;
    EditText edit_Email, edit_Password;
    RelativeLayout parentView;
    String trial;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        // ParentView
        parentView = findViewById(R.id.parentView);
        //
        trial = Utilities.getString(getApplicationContext(), "trial");
        txtSignUp = findViewById(R.id.txtSignUp);
        btnSignIn = findViewById(R.id.btn_SignIn);
        progressDialog = new ProgressDialog(SignInActivity.this);
        progressDialog.setMessage("Loading....");

        edit_Email = findViewById(R.id.edit_Email);
        edit_Password = findViewById(R.id.edit_Password);
        text_ForgetPassword = findViewById(R.id.text_ForgetPassword);


        text_ForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(SignInActivity.this, ForgetPasswordActivity.class);
                startActivity(mainIntent);
            }
        });

        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(mainIntent);
            }
        });

        btnSignIn.setOnClickListener(v -> {
            CharSequence mailText = edit_Email.getText();
            String textPassword = edit_Password.getText().toString();
            String mailString = mailText.toString().trim();
            boolean isValidEmail = TextUtilities.isValidEmail(mailText);

            if (mailString.isEmpty()) {
                Banner.make(v, SignInActivity.this, Banner.ERROR, "Your Email is Empty", Banner.TOP);
                Banner.getInstance().setDuration(2000);
                Banner.getInstance().show();
            } else if (!isValidEmail) {
                Banner.make(v, SignInActivity.this, Banner.ERROR, "Your Email is invalid", Banner.TOP);
                Banner.getInstance().setDuration(2000);
                Banner.getInstance().show();
            } else if (textPassword.isEmpty()) {
                Banner.make(v, SignInActivity.this, Banner.ERROR, "Your Password is invalid", Banner.TOP);
                Banner.getInstance().setDuration(2000);
                Banner.getInstance().show();
            } else if (textPassword.length() < 6) {
                Banner.make(v, SignInActivity.this, Banner.ERROR, "Invalid Password", Banner.TOP);
                Banner.getInstance().setDuration(2000);
                Banner.getInstance().show();
            } else {
                progressDialog.show();
                sendLoginRequest(mailString, textPassword);
            }

        });

    }

    private void sendLoginRequest(String email, String password) {


        SharedPreferences sharedPreferences = getSharedPreferences("device_token_social", Context.MODE_PRIVATE);
        String device_token = sharedPreferences.getString("device_token", "");


        Call<RegisterResponseModel> call = service.loginUser(email, password, device_token);

        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.dismiss();
//                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
                    Snackbar.make(parentView, response.body().getMessage() + "Success", Snackbar.LENGTH_LONG).show();
                    int userId = response.body().getData().getId();
                    String userEmail = response.body().getData().getEmail();
                    String userFName = response.body().getData().getName();
                    String userPhoneNumber = response.body().getData().getPhone();
                    String userProfileImage = response.body().getData().getProfile_image();
                    String token = response.body().getData().getToken();

                    Utilities.saveInt(getApplicationContext(), "userId", userId);
                    Utilities.saveString(getApplicationContext(), "userEmail", userEmail);
                    Utilities.saveString(getApplicationContext(), "userName", userFName);
                    Utilities.saveString(getApplicationContext(), "userPhone", userPhoneNumber);
                    Utilities.saveString(getApplicationContext(), "current_token", token);
                    Utilities.saveString(getApplicationContext(), "login_status", "yes");
                    if (userProfileImage != null && !userProfileImage.isEmpty()) {
                        Utilities.saveString(SignInActivity.this, "userProfileImage", Images_BaseUrl + userProfileImage);
                    }

                        if (trial.equals("yes"))
                        {
                        String currentDateTime = java.text.DateFormat.getDateTimeInstance().format(new Date());
                        Calendar c = Calendar.getInstance();
                        Log.d("YAM", "Date : " + c.getTime());
                        c.add(Calendar.DATE, 14);
                        Date edate = c.getTime();
                        Log.d("YAM", "Date : " + c.getTime());

                        SimpleDateFormat format = new SimpleDateFormat("DD MM yyyy");
                        Utilities.saveString(getApplicationContext(), "expire_date", format.format(edate));
                        }

                    Intent mainIntent = new Intent(SignInActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();
                } else if (status == 400) {

                    Banner.make(btnSignIn, SignInActivity.this, Banner.ERROR, "Password or email is incorrect", Banner.TOP);
                    Banner.getInstance().setDuration(2000);
                    Banner.getInstance().show();
                }

            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Banner.make(btnSignIn, SignInActivity.this, Banner.ERROR, "Must be at least 6 characters", Banner.TOP);
                Banner.getInstance().setDuration(2000);
                Banner.getInstance().show();
            }
        });
    }
}