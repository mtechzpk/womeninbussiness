package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.UserDetailsResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AppCompatActivity {

    String email;
    EditText editText_pass,editText_confirmPass;
    Button btn_reset;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        btn_reset = findViewById(R.id.btn_reset);
        editText_confirmPass = findViewById(R.id.reset_confirm_password);
        editText_pass = findViewById(R.id.reset_password);
        email  = Utilities.getString(ResetPasswordActivity.this,"forget_email");


        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass = editText_pass.getText().toString();
                String con_pass = editText_confirmPass.getText().toString();

                if (pass.isEmpty())
                {
                    editText_pass.setError("Please Enter Password");
                }
                if (con_pass.isEmpty())
                {
                    editText_pass.setError("Please Re-enter password");
                }
                if (!pass.equals(con_pass))
                {
                    editText_confirmPass.setError("Password Don't Match");
                }
                else
                {
                    progressDialog.show();
                    resetPassword(email,pass,con_pass);
                }
            }
        });


    }

    private void resetPassword(String email, String pass, String con_pass) {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<UserDetailsResponseModel> call = service.resetPassword(email,pass,con_pass);
        call.enqueue(new Callback<UserDetailsResponseModel>() {
            @Override
            public void onResponse(Call<UserDetailsResponseModel> call, Response<UserDetailsResponseModel> response) {

                progressDialog.dismiss();
                int status = response.body().getStatus();
                if (status == 200)
                {
                    Toast.makeText(ResetPasswordActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ResetPasswordActivity.this,SignInActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(ResetPasswordActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserDetailsResponseModel> call, Throwable t) {

                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}