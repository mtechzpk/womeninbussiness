package com.mtechsoft.socialceo.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.UserProfilePostsAdapter;
import com.mtechsoft.socialceo.model.RegisterResponseModel;
import com.mtechsoft.socialceo.model.SinglePostModel;
import com.mtechsoft.socialceo.model.UserDetailsResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileActivity extends AppCompatActivity {
    private static final String TAG = MyProfileActivity.class.getSimpleName();
    public static final int REQUEST_IMAGE = 100;
    private RecyclerView recyclerView_userPosts;
    private RecyclerView.Adapter adapter;
    private CircleImageView img_userProfile;
    TextView text_userName, text_userEmail, text_PostsTotal;
    GetDataService service;
    String userId;
    CoordinatorLayout parentView;
    ImageView back,btn_edit;
    String userProfileImage;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";

    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        recyclerView_userPosts = findViewById(R.id.recyclerView_userPosts);

        back=(ImageView)findViewById (R.id.back_ic);
        btn_edit=(ImageView)findViewById (R.id.btn_edit);
        parentView = findViewById(R.id.parent_view);
        //Initialized Service here
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        userId = String.valueOf(Utilities.getInt(MyProfileActivity.this, "userId"));
        requestForUserPosts(userId);
        progressDialog = new ProgressDialog(MyProfileActivity.this);
        progressDialog.setMessage("Loading userPosts....");
        progressDialog.show();
        img_userProfile = findViewById(R.id.img_userProfile);
        text_userName = findViewById(R.id.text_userName);
        text_userEmail = findViewById(R.id.text_userEmail);
        text_PostsTotal = findViewById(R.id.text_PostsTotal);
        text_userName.setText(Utilities.getString(MyProfileActivity.this, "userName"));
        text_userEmail.setText(Utilities.getString(MyProfileActivity.this, "userEmail"));
//        text_PostsTotal.setText();

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(MyProfileActivity.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    showImagePickerOptions();
                                }

                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
            }
        });
        back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        // Set userProfileImage
        String userProfileImage = Utilities.getString(MyProfileActivity.this, "userProfileImage");
        if (!userProfileImage.isEmpty()){
            Glide.with(getApplicationContext())
                    .load(userProfileImage)
                    .fitCenter()
                    .into(img_userProfile);
        }
    }


    private void requestForUserPosts(String userId) {

        Call<UserDetailsResponseModel> call = service.getUserDetails(userId);

        call.enqueue(new Callback<UserDetailsResponseModel>() {
            @Override
            public void onResponse(Call<UserDetailsResponseModel> call, Response<UserDetailsResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                List<SinglePostModel> dataList = response.body().getData().getUserPosts();

                if (status == 200){
                    showUserPosts(recyclerView_userPosts, dataList);
                    text_PostsTotal.setText(String.valueOf(dataList.size()));
                }
                Toast.makeText(MyProfileActivity.this, response.body().getMessage(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<UserDetailsResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MyProfileActivity.this, t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
    private void showUserPosts(RecyclerView recyclerView_userPosts, List<SinglePostModel> listItems) {
        recyclerView_userPosts.setHasFixedSize(true);
        recyclerView_userPosts.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new UserProfilePostsAdapter(listItems, this);
        recyclerView_userPosts.setAdapter(adapter);
    }

    public void onClick(View view) {

        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                String imageEncoded, input;
                Bitmap bitmapImage = null;
                // You can update this bitmap to your server
                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                input = imageEncoded;
                input = input.replace("\n", "");
                input = input.trim();
                input = "data:image/png;base64,"+input;
                // loading profile image from local cache
                assert uri != null;
                loadPhotos(uri.toString());
                userProfileImage = input;

                updateUserProfileImage(userId, userProfileImage);
            }
        }
    }
    private void loadPhotos(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(btn_edit);
        btn_edit.setColorFilter(ContextCompat.getColor(MyProfileActivity.this, android.R.color.transparent));

    }
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(MyProfileActivity.this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }
    private void launchCameraIntent() {
        Intent intent = new Intent(MyProfileActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void launchGalleryIntent() {
        Intent intent = new Intent(MyProfileActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(MyProfileActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", MyProfileActivity.this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
    private void updateUserProfileImage(String userId, String imageUrl) {
        progressDialog.setMessage("Up loading Your Profile....");
        progressDialog.show();
        Call<RegisterResponseModel> call = service.updateUserProfileImage( userId, imageUrl);

        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    Snackbar.make(parentView, response.body().getMessage() + "Success" , Snackbar.LENGTH_LONG).show();
                    String userProfileImage = response.body().getData().getProfile_image();
                    if (userProfileImage != null && !userProfileImage.isEmpty()){
                        Utilities.saveString(MyProfileActivity.this,"userProfileImage",Images_BaseUrl+userProfileImage);
                    }
                }
                else if (status == 400){
                    Snackbar.make(parentView, response.body().getMessage()  + "Failed" , Snackbar.LENGTH_LONG).show();
                }

            }
            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }
}