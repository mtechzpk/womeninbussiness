package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.BuyAndSellHistoryTabsAdapter;
import com.google.android.material.tabs.TabLayout;

public class BuyAndSellHistoryActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager_history;
    ImageView back_ic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_and_sell_history);
        back_ic=(ImageView)findViewById (R.id.back_ic);
        tabLayout = (TabLayout)findViewById(R.id.tabLayout);
        viewPager_history = (ViewPager)  findViewById(R.id.viewPager_history);
        BuyAndSellHistoryTabsAdapter myPagerAdapter = new BuyAndSellHistoryTabsAdapter(getSupportFragmentManager());
        viewPager_history.setAdapter(myPagerAdapter);
        tabLayout.setupWithViewPager(viewPager_history);
        back_ic.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
    }

    public void onClick(View view) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}