package com.mtechsoft.socialceo.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

public class PodCastActivity extends AppCompatActivity {
    private static final String TAG = PostForNetworkActivity.class.getSimpleName();
    public static final int REQUEST_IMAGE = 100;
    String userId, userEmail, postImageUrl, category;
    MaterialSpinner spinner_Category_event;
    Button btn_PostForEvent;
    private ImageView upload_ImageForEvent,img_btnback;
    EditText edit_Title,edit_seats, edit_Price_event,edit_EventDate,edit_Link_Pod,edit_Tags,edit_location_event;
    CheckBox event_checkbox_Free;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pod_cast);

        spinner_Category_event = findViewById(R.id.spinner_Category_pod);
        btn_PostForEvent = findViewById(R.id.btn_PostForEvent);
        img_btnback = (ImageView) findViewById(R.id.back_ic);
        edit_Title = findViewById(R.id.edit_Title_pod);
//        edit_Price_event = findViewById(R.id.edit_Price_event);
//        edit_EventDate = findViewById(R.id.edit_EventDate);
        edit_Link_Pod =findViewById(R.id.edit_Link_Pod);
//        edit_location_event =findViewById(R.id.edit_location_event);
        edit_Tags = findViewById(R.id.edit_Tags_pod);
//        event_checkbox_Free = findViewById(R.id.event_checkbox_Free);
        upload_ImageForEvent = findViewById(R.id.upload_Image_pod);

        img_btnback.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        spinner_Category_event.setItems("Masterclass", "Online Event","Speaker","Summit","Webinar","Workshop","Other");
        userId = String.valueOf(Utilities.getInt(PodCastActivity.this, "userId"));


        spinner_Category_event.setOnItemSelectedListener((MaterialSpinner.OnItemSelectedListener<String>) (view, position, id, item) ->
                category = item
        );


        btn_PostForEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = edit_Title.getText().toString();
//                String price = edit_Price_event.getText().toString();
//                String date = edit_EventDate.getText().toString();
                String tags = edit_Tags.getText().toString();
                String link = edit_Link_Pod.getText().toString();
//                String location = edit_location_event.getText().toString();
//                String seat = edit_seats.getText().toString();

//                if (event_checkbox_Free.isChecked() && price.isEmpty()) {
//                    price = "free";
//                } else if (price.isEmpty()) {
//                    edit_Price_event.setError("Enter Price");
//                }

                if (title.isEmpty()) {
                    edit_Title.setError("Enter Title");
                }
//                if (seat.isEmpty()) {
//                    edit_seats.setError("Enter Available Seats");
//                }
//                if (date.isEmpty()) {
//                    edit_EventDate.setError("Enter date");
//                }
                if (link.isEmpty()) {
                    edit_Link_Pod.setError("Enter Description");
                }
                if (tags.isEmpty()) {
                    edit_Tags.setError("Enter Tag");
                }
//                if (location.isEmpty()) {
//                    edit_location_event.setError("Enter Location");
//                }
                else {
                    Intent intent = new Intent(PodCastActivity.this, PodCastPostConfirmationActivity.class);
                    Utilities.saveString(PodCastActivity.this,"podImage",postImageUrl);
                    Utilities.saveString(PodCastActivity.this,"userPostId",userId);
                    Utilities.saveString(PodCastActivity.this,"podCategory",category);
                    Utilities.saveString(PodCastActivity.this,"podtitle",title);
                    Utilities.saveString(PodCastActivity.this,"podTags",tags);
                    Utilities.saveString(PodCastActivity.this,"podLink",link);
                    Utilities.saveString(PodCastActivity.this,"podType","podcast");
                    startActivity(intent);
                }


            }
        });


        upload_ImageForEvent.setOnClickListener(v -> Dexter.withActivity(PodCastActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Uri uri = data.getParcelableExtra("path");
                String imageEncoded, input;
                Bitmap bitmapImage = null;
                // You can update this bitmap to your server
                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                assert bitmapImage != null;
                bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                input = imageEncoded;
                input = input.replace("\n", "");
                input = input.trim();
                input = "data:image/png;base64,"+input;
                // loading profile image from local cache
                assert uri != null;
                loadPhotos(uri.toString());
                postImageUrl = input;

            }
        }
    }
    private void loadPhotos(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(upload_ImageForEvent);
        upload_ImageForEvent.setColorFilter(ContextCompat.getColor(PodCastActivity.this, android.R.color.transparent));

    }
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(PodCastActivity.this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }
    private void launchCameraIntent() {
        Intent intent = new Intent(PodCastActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void launchGalleryIntent() {
        Intent intent = new Intent(PodCastActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(PodCastActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", PodCastActivity.this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}
