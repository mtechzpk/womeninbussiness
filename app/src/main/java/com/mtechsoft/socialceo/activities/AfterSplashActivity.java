package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.shasin.notificationbanner.Banner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AfterSplashActivity extends AppCompatActivity {
    String loginStatus,e;
    Button btn_getStarted;
    Date expiryDate = null;
    Boolean checkExpiry = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_splash);
        btn_getStarted = findViewById(R.id.btn_getStarted);
        loginStatus = Utilities.getString(getApplicationContext(), "login_status");
        e = Utilities.getString(getApplicationContext(), "expire_date");
        Log.d("YAM","from utils : "+e);

        Date current = Calendar.getInstance().getTime();
        SimpleDateFormat format = new SimpleDateFormat("DD MM yyyy");

        try {
            expiryDate = format.parse(e);
        } catch (ParseException parseException) {
            parseException.printStackTrace();
            checkExpiry=false;
            expiryDate=current;

        }

        Log.d("YAM","final : "+expiryDate.toString());



        if(current.before(expiryDate)){
            Log.d("YAM","Good : "+expiryDate.toString());
        } else {
            Log.d("YAM","Good : "+expiryDate.toString());
        }


        btn_getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkExpiry) {
                    if (current.before(expiryDate)) {
                        if (loginStatus.equals("yes")) {
                            Intent mainIntent = new Intent(AfterSplashActivity.this, MainActivity.class);
                            startActivity(mainIntent);
                            finish();
                        } else {
                            Intent mainIntent = new Intent(AfterSplashActivity.this, SocialConnectActivity.class);
                            startActivity(mainIntent);
                            finish();
                        }
                    } else {
                        Banner.make(btn_getStarted, AfterSplashActivity.this, Banner.ERROR, "Your Trial Version is Expired ", Banner.TOP);
                        Banner.getInstance().setDuration(5000);
                        Banner.getInstance().show();
                    }

                }
                else
                {
                    if (loginStatus.equals("yes")) {
                        Intent mainIntent = new Intent(AfterSplashActivity.this, MainActivity.class);
                        startActivity(mainIntent);
                        finish();
                    } else {
                        Intent mainIntent = new Intent(AfterSplashActivity.this, SocialConnectActivity.class);
                        startActivity(mainIntent);
                        finish();
                    }

                }
            }
        });
    }
}