package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.SavedPostsAdapter;
import com.google.android.material.tabs.TabLayout;

public class SavedPostsActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPagerSavedPostsTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_posts);

        tabLayout = findViewById(R.id.home_tabLayout);
        viewPagerSavedPostsTabs = findViewById(R.id.viewPagerSavedPostsTabs);

        final SavedPostsAdapter myPagerAdapter = new SavedPostsAdapter(SavedPostsActivity.this.getSupportFragmentManager());
        viewPagerSavedPostsTabs.setAdapter(myPagerAdapter);
        tabLayout.setupWithViewPager(viewPagerSavedPostsTabs);
        setMarginBetweenTabs(tabLayout);
    }

    private void setMarginBetweenTabs(TabLayout tabLayout) {
        ViewGroup tabs = (ViewGroup) tabLayout.getChildAt(0);
        for (int i = 0; i < tabs.getChildCount() - 1; i++) {
            View tab = tabs.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            layoutParams.setMarginEnd(15);
            tab.setLayoutParams(layoutParams);
            tabLayout.requestLayout();
        }
    }
}