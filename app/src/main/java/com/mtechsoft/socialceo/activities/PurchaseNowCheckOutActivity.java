package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mtechsoft.socialceo.R;

public class PurchaseNowCheckOutActivity extends AppCompatActivity {
    AlertDialog secureAlert, successAlert;
    Button btn_Continue, btn_PurchaseNow, btn_BackToHome;
    TextView textView_CoursePrice,textView_totalprice;
    ImageView back_ic;
    String image,title,price,userName,userActive,userImage,location_sell,postId,userId,isSaved;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_now_check_out);
        textView_CoursePrice=(TextView)findViewById (R.id.text_CoursePrice);
        textView_totalprice=(TextView)findViewById (R.id.tv_totalprice);
        back_ic=(ImageView) findViewById (R.id.back_ic);
        //getting putextra values
        Intent intent = getIntent();
        price = intent.getStringExtra("PriceBS");
        Log.d ("TAGA","Price"+price);
        textView_CoursePrice.setText (price);
        textView_totalprice.setText (price);
        // show Secure Alert on Activity Created
            showSecureAlert();
        btn_PurchaseNow = findViewById(R.id.btn_PurchaseNow);
        btn_PurchaseNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSuccessfullyPaidAlert();
            }
        });

        back_ic.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
    }
    public void showSecureAlert(){

        AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseNowCheckOutActivity.this);
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_youaresecure, null);
        builder.setView(customLayout);
        secureAlert= builder.create();
        secureAlert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        secureAlert.show();
        btn_Continue = customLayout.findViewById(R.id.btn_Continue);
        btn_Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secureAlert.cancel();
            }
        });
    }

    public void showSuccessfullyPaidAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseNowCheckOutActivity.this);
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_successfullypaid, null);
        builder.setView(customLayout);
        successAlert= builder.create();
        successAlert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        successAlert.show();
        btn_BackToHome = customLayout.findViewById(R.id.btn_BackToHome);
        btn_BackToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successAlert.cancel();
                Intent mainIntent = new Intent(PurchaseNowCheckOutActivity.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });
    }
}