package com.mtechsoft.socialceo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.utilities.Utilities;

public class InvestmentTypeActivity extends AppCompatActivity {

    RadioGroup radioGroup_type;
    Button button_continue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investment_type);


        radioGroup_type = (RadioGroup) findViewById(R.id.radio_group_type);
        button_continue = findViewById(R.id.button_continue_type);




          radioGroup_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = findViewById(checkedId);
            }
        });

        button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = radioGroup_type.getCheckedRadioButtonId();
                if (selectedId == -1){
                    Toast.makeText(InvestmentTypeActivity.this, "Select Any Option", Toast.LENGTH_SHORT).show();
                }else
                {
                    RadioButton radioButton = findViewById(selectedId);
                    Toast.makeText(InvestmentTypeActivity.this, radioButton.getText(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(),ConfirmationActivity.class);
                    String text = radioButton.getText().toString();
                    Utilities.saveString(getApplicationContext(),"investmentType",text);
                    startActivity(intent);
                }

            }
        });
    }
}