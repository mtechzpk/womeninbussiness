package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.UserDetailsResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificationCodeActivity extends AppCompatActivity {
    Button btn_Confirm;
    OtpTextView otp_view;
    String email;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);
        btn_Confirm = findViewById(R.id.btn_Confirm);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading..");


        email  = Utilities.getString(VerificationCodeActivity.this,"forget_email");
        otp_view = findViewById(R.id.otp_view);
        btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = otp_view.getOTP();
                if (otp.isEmpty())
                {
                    Toast.makeText(VerificationCodeActivity.this, "Please Enter Verification Code", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    progressDialog.show();
                    checkOtp(otp);
                }

            }
        });
    }

    private void checkOtp(String otp) {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<UserDetailsResponseModel> call = service.verifyCode(email,otp);
        call.enqueue(new Callback<UserDetailsResponseModel>() {
            @Override
            public void onResponse(Call<UserDetailsResponseModel> call, Response<UserDetailsResponseModel> response) {

                progressDialog.dismiss();
                int status = response.body().getStatus();
                if (status == 200)
                {
                    Toast.makeText(VerificationCodeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(VerificationCodeActivity.this,ResetPasswordActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    progressDialog.dismiss();
                    Toast.makeText(VerificationCodeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserDetailsResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    public void onClick(View view) {
        Intent intent = new Intent(VerificationCodeActivity.this,ForgetPasswordActivity.class);
        startActivity(intent);
        finish();
    }
}