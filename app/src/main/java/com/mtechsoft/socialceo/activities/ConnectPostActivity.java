package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.BuySellAdapter;
import com.mtechsoft.socialceo.adapters.CommentsAdapter;
import com.mtechsoft.socialceo.model.BuyAndSellModel;
import com.mtechsoft.socialceo.model.CommentResponseModel;
import com.mtechsoft.socialceo.model.CommentsModel;
import com.mtechsoft.socialceo.model.GetCommentsResponseModel;
import com.mtechsoft.socialceo.model.LikeResponseModel;
import com.mtechsoft.socialceo.model.SavePostResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectPostActivity extends AppCompatActivity {
    private RecyclerView recyclerView_userComments;
    private RecyclerView.Adapter adapter;
    private CircleImageView userProfileImage;
    private ImageView img_Post;
    private TextView text_userName, post_detailComment, post_Likes, post_comments,text_activeAgo;
    EditText edit_Comment_post;
    ImageView heart_ic,bookmark;
    String favourite_status, userId, postId, postLikes, comment,saved_status;
    String fav_status = "true";
    String save_status = "true";
    GetDataService service;
    ImageButton imageButton;
    ImageView back_ic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_post);
        back_ic=(ImageView)findViewById (R.id.back_ic);
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        userId = String.valueOf(Utilities.getInt(ConnectPostActivity.this, "userId"));

        init();
        heart_ic = findViewById(R.id.heart_ic);
        checkForFavouriteStatus();
        bookmark = findViewById(R.id.bookmark);
        checkForSaveStatus();


        getComments(postId);
        back_ic.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                comment = edit_Comment_post.getText().toString();
                if (comment.isEmpty()) {
                    Toast.makeText(ConnectPostActivity.this, "Enter comment", Toast.LENGTH_SHORT).show();
                } else {
                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<CommentResponseModel> call = service.commentOnPost(userId, postId, comment);
                    call.enqueue(new Callback<CommentResponseModel>() {
                        @Override
                        public void onResponse(Call<CommentResponseModel> call, Response<CommentResponseModel> response) {
                            int status = response.body().getStatus();
                            if (status == 200) {
                                Toast.makeText(ConnectPostActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                edit_Comment_post.setText("");
                                getComments(postId);
                            }
                        }

                        @Override
                        public void onFailure(Call<CommentResponseModel> call, Throwable t) {

                        }
                    });
                }
            }
        });

        heart_ic.setOnClickListener(v -> {
            addForFavourites(userId, postId);
            if (fav_status.equals("false")) {
                heart_ic.setImageResource(R.drawable.heart_filled_ic);
                fav_status = "true";
            } else {
                heart_ic.setImageResource(R.drawable.heart_outline_ic);
                fav_status = "false";
            }
        });

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addForSaved(userId,postId);
                if (save_status.equals("false")) {
                    bookmark.setImageResource(R.drawable.fillded_save_ic);
                    save_status = "true";
                } else {
                    bookmark.setImageResource(R.drawable.bookmark_ic);
                    save_status = "false";
                }
            }
        });

    }

    private void addForSaved(String userId, String postId) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<SavePostResponseModel> call = service.savePosts(userId,postId);
        call.enqueue(new Callback<SavePostResponseModel>() {
            @Override
            public void onResponse(Call<SavePostResponseModel> call, Response<SavePostResponseModel> response) {
                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200){
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SavePostResponseModel> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }


    private void checkForSaveStatus() {
        if (saved_status.equals("true")) {
            bookmark.setImageResource(R.drawable.fillded_save_ic);
        } else if (saved_status.equals("false")) {
            bookmark.setImageResource(R.drawable.bookmark_ic);
        }
    }

    private void getComments(String postId) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<GetCommentsResponseModel> call = service.getComments(postId);
        call.enqueue(new Callback<GetCommentsResponseModel>() {
            @Override
            public void onResponse(Call<GetCommentsResponseModel> call, Response<GetCommentsResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                List<CommentsModel> dataList = response.body().getData();
                if (status == 200) {
                    showUserComments(recyclerView_userComments, dataList);
                }
            }

            @Override
            public void onFailure(Call<GetCommentsResponseModel> call, Throwable t) {

                t.getMessage();
            }
        });
    }

    private void showUserComments(RecyclerView recyclerView, List<CommentsModel> datalist) {

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CommentsAdapter(datalist, this);
        recyclerView.setAdapter(adapter);
    }


//

//    }

    private void addForFavourites(String user_id, String postId) {

        Call<LikeResponseModel> call = service.likePost(user_id, postId);
        call.enqueue(new Callback<LikeResponseModel>() {
            @Override
            public void onResponse(Call<LikeResponseModel> call, Response<LikeResponseModel> response) {
                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
                    Toast.makeText(ConnectPostActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();

                    if (response.body().getData().equals("liked")) {
                        post_Likes.setText(String.valueOf(Integer.parseInt(postLikes) + 1));
                    } else {
                        post_Likes.setText(String.valueOf(Integer.parseInt(postLikes) - 1));
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeResponseModel> call, Throwable t) {
                Toast.makeText(ConnectPostActivity.this, t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void init() {
        // Getting Intent data from intent

        String userName = getIntent().getStringExtra("userName");
        String userProfile = getIntent().getStringExtra("userProfile");
        String postDetails = getIntent().getStringExtra("postDescription");
        String postImage = getIntent().getStringExtra("postImage");
        String postComments = getIntent().getStringExtra("postComments");
        String activeAgo = getIntent().getStringExtra("activeAgo");
        postId = getIntent().getStringExtra("postId");
        favourite_status = getIntent().getStringExtra("like_status");
        saved_status = getIntent().getStringExtra("saved_status");
        postLikes = getIntent().getStringExtra("postLikes");
        fav_status = favourite_status;
        recyclerView_userComments = findViewById(R.id.recyclerView_userComments);
        // initializing views
        post_comments = findViewById(R.id.postcomment);
        post_comments.setText(postComments);
        post_Likes = findViewById(R.id.post_Likes);
        post_Likes.setText(postLikes);

        text_activeAgo = findViewById(R.id.text_activeAgo);
        Utilities.TimeAgo2 timeAgo2 = new Utilities.TimeAgo2();
        String MyFinalValue = timeAgo2.covertTimeToText(activeAgo);
        text_activeAgo.setText(MyFinalValue);

        text_userName = findViewById(R.id.text_userName);
        text_userName.setText(userName);
        userProfileImage = findViewById(R.id.img_userProfile);
        Glide.with(ConnectPostActivity.this).load(userProfile).fitCenter().into(userProfileImage);
        img_Post = findViewById(R.id.img_Post);
        Glide.with(ConnectPostActivity.this).load(postImage).fitCenter().into(img_Post);
        post_detailComment = findViewById(R.id.post_detailComment);
        post_detailComment.setText(postDetails);
        edit_Comment_post = findViewById(R.id.edit_Comment_post);
        imageButton = findViewById(R.id.btn_Send_comment);

    }

    public void checkForFavouriteStatus() {
        if (favourite_status.equals("true")) {
            heart_ic.setImageResource(R.drawable.heart_filled_ic);
        } else if (favourite_status.equals("false")) {
            heart_ic.setImageResource(R.drawable.heart_outline_ic);
        }
    }

}