package com.mtechsoft.socialceo.activities;

import androidx.viewpager.widget.ViewPager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.SplashViewPagerAdapter;
import com.mtechsoft.socialceo.model.SplashViewPagerModel;
import com.github.appintro.AppIntro;
import java.util.ArrayList;
import java.util.List;

public class AppIntroActivity extends AppIntro {
    ViewPager splashViewPager;
    SplashViewPagerAdapter adapter;
    ImageView next_arrow;
    TextView text_Skip;
    private List<SplashViewPagerModel> listItems;
    String[] splashHeadings = {"Connect with others", " Buy & Sell Products or Services", "Collaborate", "Startups"};
    String[] splashDetails = {"Digital services ...Lorem ipsum dolor sit amet, consetetur sadipscing elitr", "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diamb tempor invidunt.", "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diamb tempor invidunt.", "Upload your startup project and raise funds. Lorem ipsum dolor sit amet."};
    int[] splashImages = {R.drawable.tab, R.drawable.img_laptop, R.drawable.collaborate, R.drawable.startup};
    boolean[] showDetailsButton = {false, false, false, true};
    RelativeLayout bottomBtnsLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_intro);
        splashViewPager = findViewById(R.id.splash_ViewPager);
        bottomBtnsLayout = findViewById(R.id.bottomBtnsLayout);
        next_arrow = findViewById(R.id.next_arrow);
        text_Skip = findViewById(R.id.text_Skip);
        text_Skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(AppIntroActivity.this, SocialConnectActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });

        next_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                splashViewPager.setCurrentItem(splashViewPager.getCurrentItem() + 1);
            }
        });
        showIntroSlides(splashViewPager);

        splashViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        bottomBtnsLayout.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        bottomBtnsLayout.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        bottomBtnsLayout.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        bottomBtnsLayout.setVisibility(View.GONE);
                    default:
                        bottomBtnsLayout.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        bottomBtnsLayout.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        bottomBtnsLayout.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        bottomBtnsLayout.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        bottomBtnsLayout.setVisibility(View.GONE);
                    default:
                        bottomBtnsLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void showIntroSlides(ViewPager viewPager) {
        listItems = new ArrayList<>();
        for (int i = 0; i <= splashHeadings.length - 1; i++) {
            SplashViewPagerModel listItem = new SplashViewPagerModel();
            listItem.setSplashImage(splashImages[i]);
            listItem.setSplashHeading(splashHeadings[i]);
            listItem.setSplashDetails(splashDetails[i]);
            listItem.setShowGetStarted(showDetailsButton[i]);
            listItems.add(listItem);
        }
        adapter = new SplashViewPagerAdapter(this, listItems);
        viewPager.setAdapter(adapter);
    }

}