package com.mtechsoft.socialceo.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.fragments.networksavedpostsfragment.NetworkSavedPostsFragment;
import com.mtechsoft.socialceo.model.CreatePostResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.google.android.material.snackbar.Snackbar;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostForNetworkActivity extends AppCompatActivity {
    private static final String TAG = PostForNetworkActivity.class.getSimpleName();
    public static final int REQUEST_IMAGE = 100;

    //Declare views here

    MaterialSpinner spinner_Collaborate;
    Button btn_Post;
    ImageView upload_ImageNetWorkPost,back;
    String whoCanCollaborate;
    String userId, postImageUrl;
    EditText edit_DescriptionLength, edit_PostDescription;
    GetDataService service;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_for_network);

        back=findViewById (R.id.back_ic);
        progressDialog = new ProgressDialog(PostForNetworkActivity.this);
        progressDialog.setMessage("Uploading for Network....");
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        userId = String.valueOf(Utilities.getInt(PostForNetworkActivity.this, "userId"));

        spinner_Collaborate = findViewById(R.id.spinner_Collaborate);
        edit_DescriptionLength = findViewById(R.id.edit_DescriptionLength);
        edit_PostDescription = findViewById(R.id.edit_PostDescription);
        btn_Post = findViewById(R.id.btn_Post);
        back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        spinner_Collaborate.setItems("Only You", "Only Followers", "All Users");
        spinner_Collaborate.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
                whoCanCollaborate = item;
            }
        });

        upload_ImageNetWorkPost = findViewById(R.id.upload_Image);

        upload_ImageNetWorkPost.setOnClickListener(v -> Dexter.withActivity(PostForNetworkActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check());



        btn_Post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                String postWordsLength = edit_DescriptionLength.getText().toString();
                String postDescription = edit_PostDescription.getText().toString();
                if (whoCanCollaborate.isEmpty() || postWordsLength.isEmpty() || postDescription.isEmpty() || postImageUrl.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please Enter the Missing Field", Toast.LENGTH_LONG).show();
                }
                else {
                    uploadPostForNetWork(userId, postImageUrl, postDescription, postWordsLength, whoCanCollaborate);
                }
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                String imageEncoded, input;
                Bitmap bitmapImage = null;
                // You can update this bitmap to your server
                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                input = imageEncoded;
                input = input.replace("\n", "");
                input = input.trim();
                input = "data:image/png;base64,"+input;
                // loading profile image from local cache
                assert uri != null;
                loadPhotos(uri.toString());
                postImageUrl = input;

            }
        }
    }
    private void loadPhotos(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(upload_ImageNetWorkPost);
        upload_ImageNetWorkPost.setColorFilter(ContextCompat.getColor(PostForNetworkActivity.this, android.R.color.transparent));

    }
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(PostForNetworkActivity.this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }
    private void launchCameraIntent() {
        Intent intent = new Intent(PostForNetworkActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void launchGalleryIntent() {
        Intent intent = new Intent(PostForNetworkActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(PostForNetworkActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", PostForNetworkActivity.this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void uploadPostForNetWork(String userId, String imageUrl, String postDescription, String postLength, String whoCanCollaborate) {
        Call<CreatePostResponseModel> call = service.createPostForNetwork( userId, imageUrl, postDescription, postLength, whoCanCollaborate, "network");
        call.enqueue(new Callback<CreatePostResponseModel>() {
            @Override
            public void onResponse(Call<CreatePostResponseModel> call, Response<CreatePostResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){

                    Toast.makeText(getApplicationContext(), "Successfully Posted" , Toast.LENGTH_LONG).show();
                    finish ();
                    Intent mainIntent = new Intent(PostForNetworkActivity.this, MainActivity.class);
                    Utilities.saveInt(PostForNetworkActivity.this, "setCurrentPage", 1);
                    mainIntent.putExtra("EXTRA_SESSION_ID", 1);
                    Utilities.saveInt(PostForNetworkActivity.this, "Network", 1);
                    startActivity(mainIntent);
                    //finish();
                }
                else if (status == 400){
                    Toast.makeText(getApplicationContext(), response.body().getMessage()  + "Failed" , Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<CreatePostResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }
}