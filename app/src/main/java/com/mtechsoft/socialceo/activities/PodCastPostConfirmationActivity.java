package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.EventResponseModel;
import com.mtechsoft.socialceo.model.PodCastResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PodCastPostConfirmationActivity extends AppCompatActivity {

    Button btn_Confirm;
    String userId,image,title,category,price,date,tags,link,type,location,seat;
    TextView textView_category,textView_title,textView_details,pod_Name;
    ImageView back;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pod_cast_post_confirmation);
        btn_Confirm = findViewById(R.id.btn_Confirm_pod);
        textView_category= findViewById(R.id.event_Category);
        textView_title= findViewById(R.id.pod_Name);
        textView_details= findViewById(R.id.pod_Detail);
        pod_Name= findViewById(R.id.pod_Name);
//        textView_date= findViewById(R.id.text_Date_event);
//        textView_fee= findViewById(R.id.event_fee);
        back=(ImageView)findViewById (R.id.back_ic);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating PodCast...");


        userId = Utilities.getString(this,"userPostId");
        image = Utilities.getString(this,"podImage");
        title = Utilities.getString(this,"podtitle");
        category = Utilities.getString(this,"podCategory");
        tags = Utilities.getString(this,"podTags");
        link = Utilities.getString(this,"podLink");
        type = Utilities.getString(this,"podType");




        pod_Name.setText(category);
        textView_category.setText(category);
        textView_title.setText(title);
        textView_details.setText(link);
        back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });

        btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
//                String fee = textView_fee.getText().toString();
//                String date = textView_date.getText().toString();
                String category = textView_category.getText().toString();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<PodCastResponseModel> call = service.createPodCast(userId,image,title,category,link,tags,"podcast");
                call.enqueue(new Callback<PodCastResponseModel>() {
                    @Override
                    public void onResponse(Call<PodCastResponseModel> call, Response<PodCastResponseModel> response) {
                        progressDialog.dismiss();
                        int status = response.body().getStatus();
                        String message = response.body().getMessage();
                        if (status == 200)
                        {
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                            Intent mainIntent =  new Intent(PodCastPostConfirmationActivity.this, MainActivity.class);
                            Utilities.saveInt(PodCastPostConfirmationActivity.this, "Network", 5);
                            startActivity(mainIntent);
                            finish();
                        }else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(PodCastPostConfirmationActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<PodCastResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        t.printStackTrace();
                    }
                });
            }

        });
    }
}