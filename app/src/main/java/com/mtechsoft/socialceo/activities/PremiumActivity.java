package com.mtechsoft.socialceo.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.Arrays;
import java.util.List;

import io.paperdb.Paper;

public class PremiumActivity extends AppCompatActivity {

    BillingClient billingClient;

    Button btn_monthly,btn_yearly,btn_freeTrial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);

        btn_monthly = findViewById(R.id.btn_monthly);
        btn_yearly = findViewById(R.id.btn_yearly);
        btn_freeTrial = findViewById(R.id.btn_trial);

        Paper.init(PremiumActivity.this);
        setupbillingclint();
        btn_monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Paper.book().read("inapp", false)) {

                } else {
                    SkuDetailsParams params = SkuDetailsParams.newBuilder()
                            .setSkusList(Arrays.asList("android.test.purchased"))
                            .setType(BillingClient.SkuType.INAPP)
                            .build();

                    billingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                        @Override
                        public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
//                            Toast.makeText(MainActivity.this, "" + billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();
//                             Toast.makeText(AddPicturesActivity.this, "" + list.get(0).getTitle(), Toast.LENGTH_SHORT).show();
                                BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                                        .setSkuDetails(list.get(0))
                                        .build();
                                billingClient.launchBillingFlow(PremiumActivity.this, billingFlowParams);
                            } else {
                                Toast.makeText(PremiumActivity.this, "Cannot Remove ads", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
        btn_yearly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Paper.book().read("inapp", false)) {

                } else {
                    SkuDetailsParams params = SkuDetailsParams.newBuilder()
                            .setSkusList(Arrays.asList("android.test.purchased"))
                            .setType(BillingClient.SkuType.INAPP)
                            .build();

                    billingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                        @Override
                        public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
//                            Toast.makeText(MainActivity.this, "" + billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();
//                                Toast.makeText(AddPicturesActivity.this, "" + list.get(0).getTitle(), Toast.LENGTH_SHORT).show();

                                BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                                        .setSkuDetails(list.get(0))
                                        .build();
                                billingClient.launchBillingFlow(PremiumActivity.this, billingFlowParams);
                            } else {
                                Toast.makeText(PremiumActivity.this, "Cannot Remove ads", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
        btn_freeTrial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),SignInActivity.class);
                Utilities.saveString(getApplicationContext(),"trial","yes");
                startActivity(intent);
            }
        });


    }
    private void setupbillingclint() {

        billingClient = BillingClient.newBuilder(PremiumActivity.this).enablePendingPurchases().setListener(new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
                for (Purchase purchase : list) {
                    handlePurchase(purchase);
                }
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {


                    Toast.makeText(PremiumActivity.this, "Successfully Remove ads", Toast.LENGTH_LONG).show();
                    Paper.book().write("inapp", true);
                    Intent intent = new Intent(getApplicationContext(),SignInActivity.class);
                    startActivity(intent);
                    finish();
//                    showEditDialog();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
                    Toast.makeText(PremiumActivity.this, "Purchases Restored...", Toast.LENGTH_LONG).show();
                    Paper.book().write("inapp", true);
                    Intent intent = new Intent(getApplicationContext(),SignInActivity.class);
                    startActivity(intent);
                    finish();
//                    showEditDialog();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.BILLING_UNAVAILABLE) {
                    Toast.makeText(PremiumActivity.this, "Billing Unavailable..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ERROR) {
                    Toast.makeText(PremiumActivity.this, "Billing Error..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_DISCONNECTED) {
                    Toast.makeText(PremiumActivity.this, "Service Disconnected..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_TIMEOUT) {
                    Toast.makeText(PremiumActivity.this, "Service Timeout..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE) {
                    Toast.makeText(PremiumActivity.this, "Service Unavailable..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
                    Toast.makeText(PremiumActivity.this, "Billing not Completed..!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PremiumActivity.this, "Billing Error..!", Toast.LENGTH_SHORT).show();
                }


            }
        }).build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
                    Toast.makeText(PremiumActivity.this, "startConnection" + billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {


//                Toast.makeText(MainActivity.this, "You are disconnected from billing", Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void handlePurchase(Purchase purchase) {
        // Purchase retrieved from BillingClient#queryPurchases or your PurchasesUpdatedListener.

        // Verify the purchase.
        // Ensure entitlement was not already granted for this purchaseToken.
        // Grant entitlement to the user.

        ConsumeParams consumeParams =
                ConsumeParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();
        Toast.makeText(PremiumActivity.this, String.valueOf("token:  " + purchase.getPurchaseToken()), Toast.LENGTH_SHORT).show();
        ConsumeResponseListener listener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(BillingResult billingResult, String purchaseToken) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // Handle the success of the consume operation.
                }
            }
        };
        billingClient.consumeAsync(consumeParams, listener);
    }

}