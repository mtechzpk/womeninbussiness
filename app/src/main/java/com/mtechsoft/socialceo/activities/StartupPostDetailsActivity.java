package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.CourseAdapter;
import com.mtechsoft.socialceo.adapters.StartupPostDetailTabsAdapter;
import com.mtechsoft.socialceo.model.CourseModel;
import com.google.android.material.tabs.TabLayout;
import com.mtechsoft.socialceo.model.SavePostResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartupPostDetailsActivity extends AppCompatActivity {
    Button btn_beOneBackers;
    TabLayout tabLayout;
    ViewPager viewPagerIntroUpdate;
    ImageView imageView,back;
    ImageButton bookmark;
    String save_status ="true";
    private CourseAdapter adapter;
    private List<CourseModel> listItems;
    String userId,postId,isSaved,image_startup,name_startup,price_startup,beckers_startup,goal_startup,location_startup;
    TextView text_Location_startup,text_startUpName,text_beckers,text_StartUpPrice,text_pledgeOfGoals;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup_post_details);
        imageView = findViewById(R.id.image_event_detail);
        back=(ImageView)findViewById (R.id.back_ic);
        back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
//        showStartupImages(ViewPager_startUpImage, text_startUpImagesNumber);

        bookmark = findViewById(R.id.btn_Save_startup);
        text_Location_startup= findViewById(R.id.text_Location_startup);
        text_startUpName = findViewById(R.id.text_startUpName);
        text_beckers = findViewById(R.id.text_beckers);
        text_StartUpPrice = findViewById(R.id.text_StartUpPrice);
        text_pledgeOfGoals = findViewById(R.id.text_pledgeOfGoals);

        userId = String.valueOf(Utilities.getInt(getApplicationContext(), "userId"));

         beckers_startup = Utilities.getString(getApplicationContext(),"beckers_startup");
         goal_startup = Utilities.getString(getApplicationContext(),"goal_startup");
         image_startup = Utilities.getString(getApplicationContext(),"image_startup");
         name_startup  = Utilities.getString(getApplicationContext(),"name_startup");
        price_startup = Utilities.getString(getApplicationContext(),"price_startup");
        location_startup = Utilities.getString(getApplicationContext(),"location_start");
        isSaved = Utilities.getString(getApplicationContext(),"isSavedStartup");
        postId = Utilities.getString(getApplicationContext(),"postIdStartup");

         text_startUpName.setText(name_startup);
         text_beckers.setText(beckers_startup);
         text_Location_startup.setText(location_startup);
         text_StartUpPrice.setText("$"+price_startup);
         text_pledgeOfGoals.setText("Pledge of $"+goal_startup+" goal");

         Glide.with(this).load(image_startup).centerCrop().into(imageView);

        checkForSaveStatus();

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePost(userId,postId);
                if (save_status.equals("false")) {
                    bookmark.setImageResource(R.drawable.fillded_save_ic);
                    save_status = "true";
                } else {
                    bookmark.setImageResource(R.drawable.bookmark_ic);
                    save_status = "false";
                }
            }
        });

        // Tab layout
        tabLayout = (TabLayout)findViewById(R.id.tabLayout);
        viewPagerIntroUpdate = (ViewPager)  findViewById(R.id.viewPagerIntroUpdate);
        StartupPostDetailTabsAdapter myPagerAdapter = new StartupPostDetailTabsAdapter(getSupportFragmentManager());
        viewPagerIntroUpdate.setAdapter(myPagerAdapter);
        tabLayout.setupWithViewPager(viewPagerIntroUpdate);
    }

    private void savePost(String userId, String postId) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<SavePostResponseModel> modelCall = service.savePosts(userId,postId);
        modelCall.enqueue(new Callback<SavePostResponseModel>() {
            @Override
            public void onResponse(Call<SavePostResponseModel> call, Response<SavePostResponseModel> response) {
                int status = response.body().getStatus();
                if (status == 200){
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SavePostResponseModel> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void checkForSaveStatus() {
        if (isSaved.equals("true")) {
            bookmark.setImageResource(R.drawable.fillded_save_ic);
        } else if (isSaved.equals("false")) {
            bookmark.setImageResource(R.drawable.bookmark_ic);
        }
    }


//    public void showStartupImages(ViewPager viewPager, TextView textView){
//        listItems = new ArrayList<>();
//        for (int i = 0; i <= 3; i++) {
//            CourseModel listItem = new CourseModel();
//            listItem.setCourseImage(R.drawable.post_laptop);
//            listItems.add(listItem);
//        }
//
//        textView.setText("/" + listItems.size());
//        adapter = new CourseAdapter(listItems, this);
//        viewPager.setAdapter(adapter);
//    }
}
