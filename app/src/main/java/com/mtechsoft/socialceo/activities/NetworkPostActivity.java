package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.SMS.SMSActivity;
import com.mtechsoft.socialceo.model.LikeResponseModel;
import com.mtechsoft.socialceo.model.SavePostResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkPostActivity extends AppCompatActivity {
    Button btn_Message;
    CircleImageView img_userProfile;
    ImageView img_Post,back_ic;
    TextView text_userName, post_detail, text_PostLikes;
    ImageView heart_ic,bookmark;
    String favourite_status, userId, postId, postLikes,saved_status;
    String fav_status = "true";
    String save_status = "true";
    GetDataService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_post);

        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        userId = String.valueOf(Utilities.getInt(NetworkPostActivity.this, "userId"));

        init();
        heart_ic = findViewById(R.id.heart_ic);
        checkForFavouriteStatus();
        bookmark = findViewById(R.id.bookmark);
        checkForSaveStatus();

        btn_Message = findViewById(R.id.btn_Message);
        back_ic = (ImageView) findViewById(R.id.back_ic);
        btn_Message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(NetworkPostActivity.this, SMSActivity.class);
                startActivity(mainIntent);
            }
        });
        back_ic.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        heart_ic.setOnClickListener(v -> {
            addForFavourites(userId, postId);
            if (fav_status.equals("false")) {
                heart_ic.setImageResource(R.drawable.heart_filled_ic);
                fav_status = "true";
            } else {
                heart_ic.setImageResource(R.drawable.heart_outline_ic);
                fav_status = "false";
            }
        });

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addForSaved(userId,postId);
                if (save_status.equals("false")) {
                    bookmark.setImageResource(R.drawable.fillded_save_ic);
                    save_status = "true";
                } else {
                    bookmark.setImageResource(R.drawable.bookmark_ic);
                    save_status = "false";
                }
            }
        });

    }

    private void addForSaved(String userId, String postId) {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<SavePostResponseModel> call = service.savePosts(userId,postId);
        call.enqueue(new Callback<SavePostResponseModel>() {
            @Override
            public void onResponse(Call<SavePostResponseModel> call, Response<SavePostResponseModel> response) {
                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200){
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SavePostResponseModel> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }


    private void checkForSaveStatus() {
        if (saved_status.equals("true")) {
            bookmark.setImageResource(R.drawable.fillded_save_ic);
        } else if (favourite_status.equals("false")) {
            bookmark.setImageResource(R.drawable.bookmark_ic);
        }

    }
    private void addForFavourites(String user_id, String postId) {

        Call<LikeResponseModel> call = service.likePost(user_id, postId);
        call.enqueue(new Callback<LikeResponseModel>() {
            @Override
            public void onResponse(Call<LikeResponseModel> call, Response<LikeResponseModel> response) {
                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
                    Toast.makeText(NetworkPostActivity.this,response.body().getMessage(), Toast.LENGTH_LONG).show();

                    if (response.body().getData().equals("liked")) {
                        text_PostLikes.setText(String.valueOf(Integer.parseInt(postLikes) + 1));
                    } else {
                        text_PostLikes.setText(String.valueOf(Integer.parseInt(postLikes)));
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeResponseModel> call, Throwable t) {
                Toast.makeText(NetworkPostActivity.this, t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }






    private void init(){
        // Getting Intent data from intent
        String user_id = getIntent().getStringExtra("user_id");
        String userName = getIntent().getStringExtra("userName");
        String userProfile = getIntent().getStringExtra("userProfile");
        String postDetails = getIntent().getStringExtra("postDescription");
        String postImage = getIntent().getStringExtra("postImage");
        postId = getIntent().getStringExtra("postId");
        favourite_status = getIntent().getStringExtra("like_status");
        saved_status = getIntent().getStringExtra("saved_status");
        postLikes = getIntent().getStringExtra("postLikes");
        fav_status = favourite_status;


        Utilities.saveString(NetworkPostActivity.this,"rec_id",user_id);
        Utilities.saveString(NetworkPostActivity.this,"rec_name",userName);
        Utilities.saveString(NetworkPostActivity.this,"rec_image",userProfile);

        // initializing views
        text_userName = findViewById(R.id.text_userName);
        text_userName.setText(userName);
        text_PostLikes = findViewById(R.id.text_PostLikes);
        text_PostLikes.setText(postLikes);
        img_userProfile = findViewById(R.id.img_userProfile);
        Glide.with(NetworkPostActivity.this).load(userProfile).fitCenter().into(img_userProfile);
        img_Post = findViewById(R.id.img_Post);
        Glide.with(NetworkPostActivity.this).load(postImage).fitCenter().into(img_Post);
        post_detail = findViewById(R.id.post_detail);
        post_detail.setText(postDetails);
    }
    public void checkForFavouriteStatus() {
        if (favourite_status.equals("true")) {
            heart_ic.setImageResource(R.drawable.heart_filled_ic);
        } else if (favourite_status.equals("false")) {
            heart_ic.setImageResource(R.drawable.heart_outline_ic);
        }
    }

}