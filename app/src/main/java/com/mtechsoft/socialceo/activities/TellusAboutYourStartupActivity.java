package com.mtechsoft.socialceo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.utilities.Utilities;

public class TellusAboutYourStartupActivity extends AppCompatActivity {


    Button button_continue;
    ImageView back;
    CheckBox checkBox_corporated,checkBox_cofounders,checkBox_funding;
    String corporated,cofounders,funding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tellus_about_your_startup);

        button_continue = (Button) findViewById(R.id.button_continue);
        back = (ImageView) findViewById(R.id.button_back);

        checkBox_cofounders = findViewById(R.id.cofounders);
        checkBox_corporated = findViewById(R.id.corporated);
        checkBox_funding = findViewById(R.id.funding);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkBox_corporated.isChecked()){
                    corporated = checkBox_corporated.getText().toString();
                    Utilities.saveString(getApplicationContext(),"corporated",corporated);
                }

                if (checkBox_cofounders.isChecked()) {
                    cofounders = checkBox_cofounders.getText().toString();
                    Utilities.saveString(getApplicationContext(),"cofounders",cofounders);
                }
                if (checkBox_funding.isChecked()) {
                    funding = checkBox_funding.getText().toString();
                    Utilities.saveString(getApplicationContext(),"funding",funding);
                }

                Intent intent = new Intent(getApplicationContext(),AlongisYourStartupActivity.class);
                startActivity(intent);
            }
        });
    }
}