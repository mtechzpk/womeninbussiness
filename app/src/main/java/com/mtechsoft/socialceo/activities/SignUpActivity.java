package com.mtechsoft.socialceo.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.RegisterResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.TextUtilities;
import com.shasin.notificationbanner.Banner;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {
    GetDataService service;
    ProgressDialog progressDialog;
    Button btn_SignUp;
    EditText edit_FullName, edit_Email, edit_Mobile, edit_Password, edit_ConfirmPassword;

    private BillingClient billingClient;
    ArrayList<String> monthly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        btn_SignUp = findViewById(R.id.btn_SignUp);
        // creating Servic
        edit_FullName = findViewById(R.id.edit_FullName);
        edit_Email = findViewById(R.id.edit_Email);
        edit_Mobile = findViewById(R.id.edit_Mobile);
        edit_Password = findViewById(R.id.edit_Password);
        edit_ConfirmPassword = findViewById(R.id.edit_ConfirmPassword);
        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("Signing Up....");


        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fullName = edit_FullName.getText().toString();
                String mobileNumber = edit_Mobile.getText().toString();
                String password = edit_Password.getText().toString();
                String confirmPassword = edit_ConfirmPassword.getText().toString();
                CharSequence emailText = edit_Email.getText();
                String emailString = emailText.toString();
                boolean isValidEmail = TextUtilities.isValidEmail(emailText);

                if (fullName.isEmpty()){
                    Banner.make(v, SignUpActivity.this, Banner.ERROR, "Your Name is Empty", Banner.TOP);
                    Banner.getInstance().setDuration(2000);
                    Banner.getInstance().show();
                }

                else if (emailString.isEmpty()){
                    Banner.make(v, SignUpActivity.this, Banner.ERROR, "Your Email is Empty", Banner.TOP);
                    Banner.getInstance().setDuration(2000);
                    Banner.getInstance().show();
                }
                else if (!isValidEmail){
                    Banner.make(v, SignUpActivity.this, Banner.ERROR, "Your Email is invalid", Banner.TOP);
                    Banner.getInstance().setDuration(2000);
                    Banner.getInstance().show();
                }
                else if (password.isEmpty()){
                    Banner.make(v, SignUpActivity.this, Banner.ERROR, "Your Password is invalid", Banner.TOP);
                    Banner.getInstance().setDuration(2000);
                    Banner.getInstance().show();
                }
                else if (password.length() < 6){
                    Banner.make(v, SignUpActivity.this, Banner.ERROR, "Must be at least 6 characters", Banner.TOP);
                    Banner.getInstance().setDuration(2000);
                    Banner.getInstance().show();
                }
                else if (confirmPassword.isEmpty()){
                    Banner.make(v, SignUpActivity.this, Banner.ERROR, "Your Password is invalid", Banner.TOP);
                    Banner.getInstance().setDuration(2000);
                    Banner.getInstance().show();
                }
                else if (!password.equals(confirmPassword)) {
                    Banner.make(v, SignUpActivity.this, Banner.ERROR, "Password Must Match", Banner.TOP);
                    Banner.getInstance().setDuration(2000);
                    Banner.getInstance().show();
                }
                else {
                    progressDialog.show();
                    sendRegisterRequest(fullName,mobileNumber,password,confirmPassword,emailString);
                }

            }
        });
    }



    private void sendRegisterRequest(String fullname,String mobileNumber,String password,String confirmPassword,String email ) {

        Call<RegisterResponseModel> call = service.registerUser(fullname,email,mobileNumber,password,confirmPassword);

        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.dismiss();
                int status  = response.body().getStatus();
                if (status == 200){
                    Toast.makeText(getApplicationContext(), "Welcome To Women In Business", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(SignUpActivity.this,PremiumActivity.class);
                    startActivity(intent);

                }
                else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "You may have an error", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void setupbillingclint() {

        billingClient = BillingClient.newBuilder(SignUpActivity.this).enablePendingPurchases().setListener(new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
                for (Purchase purchase : list) {
                    handlePurchase(purchase);
                }
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {

                    Toast.makeText(SignUpActivity.this, "Successfully Remove ads", Toast.LENGTH_LONG).show();
                    Paper.book().write("inapp", true);
//                    showEditDialog();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
                    Toast.makeText(SignUpActivity.this, "Purchases Restored...", Toast.LENGTH_LONG).show();
                    Paper.book().write("inapp", true);
//                    showEditDialog();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.BILLING_UNAVAILABLE) {
                    Toast.makeText(SignUpActivity.this, "Billing Unavailable..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ERROR) {
                    Toast.makeText(SignUpActivity.this, "Billing Error..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_DISCONNECTED) {
                    Toast.makeText(SignUpActivity.this, "Service Disconnected..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_TIMEOUT) {
                    Toast.makeText(SignUpActivity.this, "Service Timeout..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE) {
                    Toast.makeText(SignUpActivity.this, "Service Unavailable..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
                    Toast.makeText(SignUpActivity.this, "Billing not Completed..!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SignUpActivity.this, "Billing Error..!", Toast.LENGTH_SHORT).show();
                }


            }
        }).build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
                    Toast.makeText(SignUpActivity.this, "startConnection" + billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onBillingServiceDisconnected() {


//                Toast.makeText(MainActivity.this, "You are disconnected from billing", Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void handlePurchase(Purchase purchase) {
        // Purchase retrieved from BillingClient#queryPurchases or your PurchasesUpdatedListener.

        // Verify the purchase.
        // Ensure entitlement was not already granted for this purchaseToken.
        // Grant entitlement to the user.

        ConsumeParams consumeParams =
                ConsumeParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();
        Toast.makeText(SignUpActivity.this, String.valueOf("token:  " + purchase.getPurchaseToken()), Toast.LENGTH_SHORT).show();
        ConsumeResponseListener listener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(BillingResult billingResult, String purchaseToken) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // Handle the success of the consume operation.
                }
            }
        };

        billingClient.consumeAsync(consumeParams, listener);
    }
}