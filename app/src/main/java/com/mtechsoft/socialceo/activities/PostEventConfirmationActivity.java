package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.EventResponseModel;
import com.mtechsoft.socialceo.model.PostToSellResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostEventConfirmationActivity extends AppCompatActivity {

    Button btn_Confirm;
    String userId,image,title,category,price,date,tags,desc,type,location,seat;
    TextView textView_category,textView_fee,textView_date,event_Name;
    ImageView back;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_event_confirmation);
        btn_Confirm = findViewById(R.id.btn_Confirm_event);
        textView_category = findViewById(R.id.event_Category);
        textView_date = findViewById(R.id.text_Date_event);
        textView_fee = findViewById(R.id.event_fee);
        event_Name = findViewById(R.id.event_Name);
        back = (ImageView) findViewById(R.id.back_ic);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating Event...");

        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");
        image = intent.getStringExtra("eventImage");
        title = intent.getStringExtra("title");
        category = intent.getStringExtra("eventCategory");
        price = intent.getStringExtra("ticketPrice");
        tags = intent.getStringExtra("eventTags");
        date = intent.getStringExtra("eventDate");
        desc = intent.getStringExtra("eventDesc");
        type = intent.getStringExtra("eventType");
        seat = intent.getStringExtra("eventSeat");
        location = intent.getStringExtra("eventlocation");


        event_Name.setText(title);

        if (price.equals("free"))
        {
            textView_fee.setText(price);
        }
        else
        {
            textView_fee.setText("$"+price);
        }

        textView_date.setText(date);
        textView_category.setText(category);
        back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });

        btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                String fee = textView_fee.getText().toString();
                String date = textView_date.getText().toString();
                String category = textView_category.getText().toString();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<EventResponseModel> call = service.createEvent(userId,image,title,category,fee,date,desc,tags,type,location,seat);
                call.enqueue(new Callback<EventResponseModel>() {
                    @Override
                    public void onResponse(Call<EventResponseModel> call, Response<EventResponseModel> response) {
                        progressDialog.dismiss();
                        int status = response.body().getStatus();
                        String message = response.body().getMessage();
                        if (status == 200)
                        {
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                            Intent mainIntent =  new Intent(PostEventConfirmationActivity.this, MainActivity.class);
                            Utilities.saveInt(PostEventConfirmationActivity.this, "Network", 3);
                            startActivity(mainIntent);
                            finish();
                        }else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(PostEventConfirmationActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<EventResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        t.printStackTrace();
                    }
                });
            }

        });
    }
}