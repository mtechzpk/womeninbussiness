package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.RestService;
import com.mtechsoft.socialceo.UrlController;
import com.mtechsoft.socialceo.fragments.home.HomeFragment;
import com.mtechsoft.socialceo.model.PostToSellResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostForSaleConfirmationActivity extends AppCompatActivity {
    Button btn_Confirm;
    String userId,image,title,category,price,link,desc,type,location;
    TextView textView_category,textView_fee,textView_desc,textView_link,course_Name;
    ProgressDialog progressDialog;
    ImageView back_ic;
    Uri uri;
    File filee;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_for_sale_confirmation);
        btn_Confirm = findViewById(R.id.btn_Confirm);
        textView_category= findViewById(R.id.tv_category);
        textView_desc= findViewById(R.id.tv_description);
        textView_link= findViewById(R.id.tv_link);
        back_ic= (ImageView) findViewById(R.id.back_ic);
        textView_fee= findViewById(R.id.tv_fee);
        course_Name= findViewById(R.id.course_Name);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating Post...");

        Intent intent = getIntent();
       userId = intent.getStringExtra("userId");
       image = intent.getStringExtra("postImage");
       uri = Uri.parse(image);



        if (image.contains("image")){

            String filePath = getRealPathFromURIPath(uri, PostForSaleConfirmationActivity.this);
            filee = new File(filePath);

        }else {

            String filePath = getRealPathFromURIPath(uri, PostForSaleConfirmationActivity.this);
            filee = new File(filePath);        }

       title = intent.getStringExtra("productName");
       category = intent.getStringExtra("category");
       price = intent.getStringExtra("price");
       link = intent.getStringExtra("link");
       desc = intent.getStringExtra("desc");
       type = intent.getStringExtra("type");


       textView_fee.setText("$"+price);
       textView_desc.setText(desc);
       textView_category.setText(category);
       textView_link.setText (link);
       course_Name.setText(category);

        back_ic.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });

        btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
              String fee = textView_fee.getText().toString();
              String desc = textView_desc.getText().toString();
              String category = textView_category.getText().toString();


                RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), filee);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("post_file", filee.getName(), requestBody);
                RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), userId);
                RequestBody titlee = RequestBody.create(MediaType.parse("text/plain"), title);
                RequestBody categoryy = RequestBody.create(MediaType.parse("text/plain"), category);
                RequestBody feeee = RequestBody.create(MediaType.parse("text/plain"), fee);
                RequestBody linkkk = RequestBody.create(MediaType.parse("text/plain"), link);
                RequestBody descc = RequestBody.create(MediaType.parse("text/plain"), desc);
                RequestBody typee = RequestBody.create(MediaType.parse("text/plain"), type);
                RequestBody loc = RequestBody.create(MediaType.parse("text/plain"), "location");

                RestService restService = UrlController.createService(RestService.class);
                Call<ResponseBody> call = restService.createPostToSell(fileToUpload,user_id,titlee,categoryy,feeee,linkkk,descc,typee,loc,
                        UrlController.AddHeaders(PostForSaleConfirmationActivity.this));
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        progressDialog.dismiss();
                        JSONObject responseobj = null;
                        try {
                            responseobj = new JSONObject(response.body().string());
                            int status = responseobj.getInt("status");
                            if (status == 200) {

                            Toast.makeText(getApplicationContext(), "Successfully Posted ",Toast.LENGTH_SHORT).show();
                            Intent mainIntent =  new Intent(PostForSaleConfirmationActivity.this, MainActivity.class);
                            Utilities.saveInt(PostForSaleConfirmationActivity.this, "setCurrentPage", 2);
                            Utilities.saveInt(PostForSaleConfirmationActivity.this, "Network", 2);
                            startActivity(mainIntent);
                            finish();

                            }else{

                            progressDialog.dismiss();
                            Toast.makeText(PostForSaleConfirmationActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

//                        int status = response.body().getStatus();
//                        String message = response.body().getMessage();
//
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        progressDialog.dismiss();
                        t.printStackTrace();
                    }
                });
            }

        });
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }
}