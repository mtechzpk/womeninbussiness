package com.mtechsoft.socialceo.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.mtechsoft.socialceo.R;

public class PaymentMethodSellingActivity extends AppCompatActivity {
    AlertDialog successAlert;
    Button btn_PostForSale, btn_BackToHome;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method_selling);
        btn_PostForSale = findViewById(R.id.btn_PostForSale);
        back=(ImageView)findViewById (R.id.back_ic);
        back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        btn_PostForSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSuccessfullyPaidAlert();
            }
        });
    }

    public void showSuccessfullyPaidAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(PaymentMethodSellingActivity.this);
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_successfullyposted, null);
        builder.setView(customLayout);
        successAlert= builder.create();
        successAlert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        successAlert.show();
        btn_BackToHome = customLayout.findViewById(R.id.btn_BackToHome);
        btn_BackToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successAlert.cancel();
                Intent mainIntent = new Intent(PaymentMethodSellingActivity.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });
    }
}