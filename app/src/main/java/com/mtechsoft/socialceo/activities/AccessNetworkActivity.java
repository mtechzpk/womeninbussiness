package com.mtechsoft.socialceo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.utilities.Utilities;

public class AccessNetworkActivity extends AppCompatActivity {

    Button button_continue;
    ImageView back;
    RadioGroup radioGroup_network;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_network);

        button_continue = (Button) findViewById(R.id.button_continue);
        back = (ImageView) findViewById(R.id.button_back);
        radioGroup_network = findViewById(R.id.radio_group_network);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        radioGroup_network.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = findViewById(checkedId);
            }
        });

        button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = radioGroup_network.getCheckedRadioButtonId();
                if (selectedId == -1){
                    Toast.makeText(AccessNetworkActivity.this, "Select Any Option", Toast.LENGTH_SHORT).show();
                }else
                {
                    RadioButton radioButton = findViewById(selectedId);
                    Toast.makeText(AccessNetworkActivity.this, radioButton.getText(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(),InvestmentTypeActivity.class);
                    String text = radioButton.getText().toString();
                    Utilities.saveString(getApplicationContext(),"networkPlan",text);
                    startActivity(intent);
                }
            }
        });
    }
}