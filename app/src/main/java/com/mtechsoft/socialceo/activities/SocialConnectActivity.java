package com.mtechsoft.socialceo.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.shasin.notificationbanner.Banner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import io.paperdb.Paper;

public class SocialConnectActivity extends AppCompatActivity {
    TextView txtSignIn;
    LoginButton loginButton;
    Button btn_Itunes;
    String fb_id, fb_name;
    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    BillingClient billingClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_connect);
        txtSignIn = findViewById(R.id.txtSignIn);
        loginButton = findViewById(R.id.login_button);
        btn_Itunes = findViewById(R.id.btn_Itunes);
        Paper.init(SocialConnectActivity.this);


//        btn_Itunes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Paper.book().read("inapp", false)) {
//
//                } else {
//                    SkuDetailsParams params = SkuDetailsParams.newBuilder()
//                            .setSkusList(Arrays.asList("android.test.purchased"))
//                            .setType(BillingClient.SkuType.INAPP)
//                            .build();
//
//                    billingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
//                        @Override
//                        public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
//                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
////                            Toast.makeText(MainActivity.this, "" + billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();
////                                Toast.makeText(AddPicturesActivity.this, "" + list.get(0).getTitle(), Toast.LENGTH_SHORT).show();
//
//                                BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
//                                        .setSkuDetails(list.get(0))
//                                        .build();
//                                billingClient.launchBillingFlow(SocialConnectActivity.this, billingFlowParams);
//                            } else {
//                                Toast.makeText(SocialConnectActivity.this, "Cannot Remove ads", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    });
//                }
//            }
//        });

        setupbillingclint();
        txtSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SocialConnectActivity.this,SignInActivity.class);
                startActivity(intent);
                finish();
            }
        });

        callbackManager = CallbackManager.Factory.create();
        loginButton.setPermissions(Arrays.asList("user_gender"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                loginUser(fb_id,fb_name);

            }

            @Override
            public void onCancel() {

                Toast.makeText(SocialConnectActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {

                Toast.makeText(SocialConnectActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loginUser(String fb_id, String fb_name) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.d("info",object.toString());

                        try {
                            fb_name = object.getString("name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            fb_id =object.getString("id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle bundle = new Bundle();
        bundle.putString("fields","gender,name,email,id,first_name,last_name");
    }

    AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if (currentAccessToken==null)
            {
                LoginManager.getInstance().logOut();
            }
        }
    };

    private void setupbillingclint() {

        billingClient = BillingClient.newBuilder(getApplicationContext()).enablePendingPurchases().setListener(new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
                for (Purchase purchase : list) {
                    handlePurchase(purchase);
                }
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {

                    Toast.makeText(getApplicationContext(), "Successfully Remove ads", Toast.LENGTH_LONG).show();
                    Paper.book().write("inapp", true);
//                    showEditDialog();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
                    Toast.makeText(getApplicationContext(), "Purchases Restored...", Toast.LENGTH_LONG).show();
                    Paper.book().write("inapp", true);
//                    showEditDialog();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.BILLING_UNAVAILABLE) {
                    Toast.makeText(getApplicationContext(), "Billing Unavailable..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ERROR) {
                    Toast.makeText(getApplicationContext(), "Billing Error..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_DISCONNECTED) {
                    Toast.makeText(getApplicationContext(), "Service Disconnected..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_TIMEOUT) {
                    Toast.makeText(getApplicationContext(), "Service Timeout..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE) {
                    Toast.makeText(getApplicationContext(), "Service Unavailable..!", Toast.LENGTH_SHORT).show();
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
                    Toast.makeText(getApplicationContext(), "Billing not Completed..!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Billing Error..!", Toast.LENGTH_SHORT).show();
                }


            }
        }).build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
                    Toast.makeText(getApplicationContext(), "startConnection" + billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onBillingServiceDisconnected() {


//                Toast.makeText(MainActivity.this, "You are disconnected from billing", Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void handlePurchase(Purchase purchase) {
        // Purchase retrieved from BillingClient#queryPurchases or your PurchasesUpdatedListener.

        // Verify the purchase.
        // Ensure entitlement was not already granted for this purchaseToken.
        // Grant entitlement to the user.

        ConsumeParams consumeParams =
                ConsumeParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();
        Toast.makeText(getApplicationContext(), String.valueOf("token:  " + purchase.getPurchaseToken()), Toast.LENGTH_SHORT).show();
        ConsumeResponseListener listener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(BillingResult billingResult, String purchaseToken) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // Handle the success of the consume operation.
                }
            }
        };

        billingClient.consumeAsync(consumeParams, listener);
    }
}