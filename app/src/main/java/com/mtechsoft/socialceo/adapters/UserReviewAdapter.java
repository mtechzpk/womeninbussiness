package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.UserReviewModel;
import com.willy.ratingbar.BaseRatingBar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserReviewAdapter extends RecyclerView.Adapter<UserReviewAdapter.ViewHolder>{
    List<UserReviewModel> listItems;
    Context context;

    public UserReviewAdapter(List<UserReviewModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_userview, parent, false);
        return new UserReviewAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserReviewModel listItem = listItems.get(position);
        holder.userName.setText(listItem.getUserName());
        holder.reviewTime.setText(listItem.getReviewTime());
        holder.reviewDetails.setText(listItem.getReviewDetails());
        holder.ratingBar.setRating(listItem.getReviewRating());
        holder.userProfile.setImageResource(listItem.getUserProfile());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView userName, reviewTime, reviewDetails;
        public CircleImageView userProfile;
        public BaseRatingBar ratingBar;
        public ViewHolder(View view) {
            super(view);
            userName = view.findViewById(R.id.text_userName);
            reviewTime = view.findViewById(R.id.text_ReviewTime);
            reviewDetails = view.findViewById(R.id.text_ReviewDetails);
            userProfile = view.findViewById(R.id.img_userProfile);
            ratingBar = view.findViewById(R.id.review_Rating);
        }
    }
}
