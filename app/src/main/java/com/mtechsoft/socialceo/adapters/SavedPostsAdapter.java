package com.mtechsoft.socialceo.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mtechsoft.socialceo.fragments.PodCastSavedPostFragment;
import com.mtechsoft.socialceo.fragments.buyandsellsavedpostfragment.BuyAndSellSavedPostsFragment;
import com.mtechsoft.socialceo.fragments.connectsavedfragment.ConnectSavedPostFragment;
import com.mtechsoft.socialceo.fragments.eventssavedpostsfragment.EventsSavedPostsFragment;
import com.mtechsoft.socialceo.fragments.networksavedpostsfragment.NetworkSavedPostsFragment;
import com.mtechsoft.socialceo.fragments.startupsavedpostsfragment.StartupSavedPostsFragment;

public class SavedPostsAdapter extends FragmentStatePagerAdapter {


    public SavedPostsAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    public SavedPostsAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new ConnectSavedPostFragment();
            case 1:
                return new NetworkSavedPostsFragment();
            case 2 :
                return new BuyAndSellSavedPostsFragment();
            case 3 :
                return new EventsSavedPostsFragment();
            case 4:
                return new PodCastSavedPostFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Connect";
            case 1:
                return "Network";
            case 2:
                return "Buy & Sell";
            case 3:
                return "Events";
            case 4:
                return "PodCast";
            default:
                return null;
        }
    }
    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

}
