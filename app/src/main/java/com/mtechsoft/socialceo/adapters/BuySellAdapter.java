package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.activities.BuyAndSellCourseActivity;
import com.mtechsoft.socialceo.model.BuyAndSellModel;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;


public class BuySellAdapter extends RecyclerView.Adapter<BuySellAdapter.ViewHolder> {
    List<BuyAndSellModel> listItems;
    Context context;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";
    public BuySellAdapter(List<BuyAndSellModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_singe_buy_sell, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        BuyAndSellModel listItem = listItems.get(position);
        holder.courseName.setText(listItem.getTitle());
        String price = listItem.getPrice();
        if (!price.isEmpty()){
            if (price.equals("free"))
            {
                holder.coursePrice.setText(listItem.getPrice());
            }
            else {
                holder.coursePrice.setText("$"+listItem.getPrice());
            }

        }

        Glide.with(holder.itemView)
                .load(Images_BaseUrl+listItem.getPost_Image())
                .placeholder(R.drawable.laptop_post)
                .fitCenter()
                .into(holder.imageCourse);


        holder.itemView.setOnClickListener(v -> {
            Intent mainIntent = new Intent(holder.itemView.getContext(), BuyAndSellCourseActivity.class);
            mainIntent.putExtra("userProfile", Images_BaseUrl+ listItem.getUserDetails().getProfile_image());
            mainIntent.putExtra("userName", listItem.getUserDetails().getUserName());
            mainIntent.putExtra("postDescription", listItem.getPost_description());
            mainIntent.putExtra("title",listItem.getTitle());
            mainIntent.putExtra("price",listItem.getPrice());
            mainIntent.putExtra("hours",listItem.getCreated_at());
            mainIntent.putExtra("postImage", Images_BaseUrl+listItem.getPost_Image());
            mainIntent.putExtra("postId", listItem.getPostId());
            mainIntent.putExtra("location_sell",listItem.getLocation());
            mainIntent.putExtra("postId",String.valueOf(listItem.getPostId()));
            mainIntent.putExtra("isSaved",listItem.getIs_saved());
            mainIntent.putExtra("link",listItem.getLink_of_product_or_service());

            Utilities.saveString(context,"desc_course",listItem.getPost_description());
            holder.itemView.getContext().startActivity(mainIntent);
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView courseName, coursePrice;
        public ImageView imageCourse;
        public ViewHolder(View view) {
            super(view);
            imageCourse = view.findViewById(R.id.course_Image);
            courseName = view.findViewById(R.id.text_CourseName);
            coursePrice = view.findViewById(R.id.text_Price);
        }
    }
}
