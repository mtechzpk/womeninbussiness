package com.mtechsoft.socialceo.adapters;

import com.mtechsoft.socialceo.model.UpcomingEventsModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpComingEventsResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<UpcomingEventsModel> data;

    public UpComingEventsResponseModel() {
    }

    public UpComingEventsResponseModel(int status, String message, List<UpcomingEventsModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UpcomingEventsModel> getData() {
        return data;
    }

    public void setData(List<UpcomingEventsModel> data) {
        this.data = data;
    }


}
