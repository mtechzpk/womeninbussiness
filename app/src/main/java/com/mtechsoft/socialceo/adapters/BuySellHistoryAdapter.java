package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.BuySellHistoryModel;

import java.util.List;

public class BuySellHistoryAdapter extends RecyclerView.Adapter<BuySellHistoryAdapter.ViewHolder> {
    List<BuySellHistoryModel> listItems;
    Context context;

    public BuySellHistoryAdapter(List<BuySellHistoryModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_history_item, parent, false);
        return new BuySellHistoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BuySellHistoryModel listItem = listItems.get(position);
        holder.titleName.setText(listItem.getTitle());
        holder.dateTime.setText(listItem.getDateTime());
        holder.rating.setText(listItem.getRating());
        holder.price.setText(listItem.getPrice());
        holder.image.setImageResource(listItem.getImage());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView titleName, rating, price, dateTime;
        public ImageView image;
        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            titleName = view.findViewById(R.id.text_titleName);
            rating = view.findViewById(R.id.text_Rating);
            price = view.findViewById(R.id.text_Price);
            dateTime = view.findViewById(R.id.text_dateTime);
        }
    }
}
