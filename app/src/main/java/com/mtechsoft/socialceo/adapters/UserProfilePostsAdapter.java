package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.SinglePostModel;

import java.util.List;

public class UserProfilePostsAdapter extends RecyclerView.Adapter<UserProfilePostsAdapter.ViewHolder> {

    List<SinglePostModel> listItems;
    Context context;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";
    public UserProfilePostsAdapter(List<SinglePostModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_user_profile_posts, parent, false);
        return new UserProfilePostsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SinglePostModel listItem  = listItems.get(position);
        Glide.with(holder.itemView)
                .load(Images_BaseUrl+listItem.getPostImage())
                .fitCenter()
                .into(holder.postImage);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView postImage;
        public ViewHolder(View view) {
            super(view);
            postImage = view.findViewById(R.id.img_Post);
        }
    }
}
