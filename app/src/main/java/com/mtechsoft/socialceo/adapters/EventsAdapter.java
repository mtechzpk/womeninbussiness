package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.activities.EventPostDetailActivity;
import com.mtechsoft.socialceo.model.EventsDataModel;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {
    List<EventsDataModel> listItems;
    Context context;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";
    public EventsAdapter(List<EventsDataModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_single_event, parent, false);
        return new EventsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        EventsDataModel listItem = listItems.get(position);
        holder.eventName.setText(listItem.getDescription());
        if (listItem.getPrice().equals("free"))
        {
            holder.eventPrice.setText(listItem.getPrice());
        }
        else {
            holder.eventPrice.setText("$ "+listItem.getPrice());
        }

        holder.eventDate.setText(listItem.getEvent_date());

        Glide.with(holder.itemView)
                .load(Images_BaseUrl+listItem.getImage())
                .fitCenter()
                .into(holder.eventImage);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String imageEventDetails = Images_BaseUrl+listItem.getImage();
                String nameEventDetails = listItem.getDescription();
                String priceEventDetails = listItem.getPrice();
                String dateEventDetails = listItem.getEvent_date();
                String tagsEventDetails = listItem.getTags();
                String locationEventDetails = listItem.getLocation();
                String postId = String.valueOf(listItem.getPost_id());
                String seatEventDetails = listItem.getAvailable_seats();
                String isSaved = listItem.getIs_saved();

                Intent intent = new Intent(context, EventPostDetailActivity.class);

                Utilities.saveString(context,"locationEventDetails",locationEventDetails);
                Utilities.saveString(context,"imageEventDetails",imageEventDetails);
                Utilities.saveString(context,"nameEventDetails",nameEventDetails);
                Utilities.saveString(context,"priceEventDetails",priceEventDetails);
                Utilities.saveString(context,"dateEventDetails",dateEventDetails);
                Utilities.saveString(context,"tagsEventDetails",tagsEventDetails);
                Utilities.saveString(context,"seatEventDetails",seatEventDetails);
                Utilities.saveString(context,"postEventId",postId);
                Utilities.saveString(context,"isSavedEvent",isSaved);

              context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView eventName, eventDate, eventPrice;
        public ImageView eventImage;
        public ViewHolder(View view) {
            super(view);
            eventImage = view.findViewById(R.id.event_Image);
            eventName = view.findViewById(R.id.text_EventDetails);
            eventDate = view.findViewById(R.id.text_Date);
            eventPrice = view.findViewById(R.id.text_Price);
        }
    }
}
