package com.mtechsoft.socialceo.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mtechsoft.socialceo.fragments.startupcomments.StartupCommentsFragment;
import com.mtechsoft.socialceo.fragments.startupintroduction.StartupIntroductionFragment;
import com.mtechsoft.socialceo.fragments.startupupdates.StartupUpdatesFragment;

public class StartupPostDetailTabsAdapter extends FragmentStatePagerAdapter {

    public StartupPostDetailTabsAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new StartupIntroductionFragment();
            case 1:
                return new StartupUpdatesFragment();
            case 2:
                return new StartupCommentsFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Introduction";
            case 1:
                return "Updates";
            case 2:
                return "Comments";
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
