package com.mtechsoft.socialceo.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mtechsoft.socialceo.fragments.PodCastFragment;
import com.mtechsoft.socialceo.fragments.buysell.BuyAndSellFragment;
import com.mtechsoft.socialceo.fragments.connect.ConnectFragment;
import com.mtechsoft.socialceo.fragments.events.EventsFragment;
import com.mtechsoft.socialceo.fragments.network.NetworkFragment;
import com.mtechsoft.socialceo.fragments.startup.StartupFragment;

public class HomeTabsAdapter extends FragmentStatePagerAdapter {


    public HomeTabsAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ConnectFragment();
            case 1:
                return new NetworkFragment();
            case 2 :
                return new BuyAndSellFragment();
            case 3 :
                return new EventsFragment();
            case 4:
                return new PodCastFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Connect";
            case 1:
                return "Network";
            case 2:
                return "Buy & Sell";
            case 3:
                return "Events";
            case 4:
                return "PodCasts";
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

}
