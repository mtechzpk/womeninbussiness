package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.NotificationsModel;

import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    List<NotificationsModel> listItems;
    Context context;

    public NotificationsAdapter(List<NotificationsModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_single_notification, parent, false);
        return new NotificationsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationsModel listItem = listItems.get(position);
        holder.userProfileImage.setImageResource(listItem.getUserImage());
        holder.notificationTime.setText(listItem.getNotifcationTime());
        holder.notificationDetails.setText(listItem.getNotificationDetails());
        holder.userName.setText(listItem.getUserName());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView userName, notificationDetails, notificationTime;
        public ImageView userProfileImage;

        public ViewHolder(@NonNull View view) {
            super(view);
            userName = view.findViewById(R.id.user_Name);
            notificationDetails = view.findViewById(R.id.notification_details);
            notificationTime = view.findViewById(R.id.notification_time);
            userProfileImage = view.findViewById(R.id.userProfileImage);
        }
    }
}
