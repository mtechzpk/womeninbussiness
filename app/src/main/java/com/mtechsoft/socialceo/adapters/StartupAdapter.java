package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.activities.StartupPostDetailsActivity;
import com.mtechsoft.socialceo.model.StartupDataModel;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

public class StartupAdapter extends RecyclerView.Adapter<StartupAdapter.ViewHolder> {
    List<StartupDataModel> listItems;
    Context context;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";
    public StartupAdapter(List<StartupDataModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_single_startup, parent, false);
        return new StartupAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        StartupDataModel listItem = listItems.get(position);
        holder.startupName.setText(listItem.getTitle());
//        holder.startupPrice.setText(listItem.getStartup_investment());
        holder.startupPrice.setText(listItem.getStartup_investment()+"$");
        holder.startupBackers.setText("2000 backers");
        holder.startupPledges.setText("Pledge of $" + listItem.getPledge_goal_amount() + " goal");
        Glide.with(holder.itemView)
                .load(Images_BaseUrl+listItem.getImage())
                .fitCenter()
                .into(holder.startupImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String image_startup = Images_BaseUrl+listItem.getImage();
                String name_startup = listItem.getTitle();
                String goal_startup = listItem.getPledge_goal_amount();
                String beckers = "2000 backers";
                String price = listItem.getStartup_investment();
                String location = listItem.getLocation();
                String isSaved = listItem.getIs_saved();
                String postId = String.valueOf(listItem.getPost_id());
                Intent intent = new Intent(context, StartupPostDetailsActivity.class);

                Utilities.saveString(context,"image_startup",image_startup);
                Utilities.saveString(context,"name_startup",name_startup);
                Utilities.saveString(context,"goal_startup",goal_startup);
                Utilities.saveString(context,"beckers_startup",beckers);
                Utilities.saveString(context,"price_startup",price);
                Utilities.saveString(context,"location_start",location);
                Utilities.saveString(context,"isSavedStartup",isSaved);
                Utilities.saveString(context,"postIdStartup",postId);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView startupName, startupBackers, startupPrice, startupPledges;
        public ImageView startupImage;
        public ViewHolder(View view) {
            super(view);
            startupImage = view.findViewById(R.id.startup_Image);
            startupName = view.findViewById(R.id.text_ProjectTitle);
            startupBackers = view.findViewById(R.id.text_Backers);
            startupPrice = view.findViewById(R.id.text_Price);
            startupPledges = view.findViewById(R.id.text_pledge);

        }
    }
}
