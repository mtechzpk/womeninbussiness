package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.CommentsModel;

import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
    List<CommentsModel> listItems;
    Context context;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";

    public CommentsAdapter(List<CommentsModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_single_comment, parent, false);
        return new CommentsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CommentsModel listItem = listItems.get(position);
        holder.userName.setText(listItem.getUser_name());
        holder.userComment.setText(listItem.getComment());
        Glide.with(context).load(Images_BaseUrl+listItem.getProfile_image()).into(holder.userProfile);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView userName, userComment, userCommentTime;
        public ImageView userProfile;
        public ViewHolder(View view) {
            super(view);
            userName = view.findViewById(R.id.text_userName);
            userComment = view.findViewById(R.id.text_userComment);
            userCommentTime = view.findViewById(R.id.text_userCommentTime);
            userProfile = view.findViewById(R.id.img_userProfile);
        }
    }
}
