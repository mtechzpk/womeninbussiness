package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.UserDetailsModel;

import java.util.List;

public class ActiveMembersAdapter extends RecyclerView.Adapter<ActiveMembersAdapter.ViewHolder> {

    List<UserDetailsModel> listItems;
    Context context;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";

    public ActiveMembersAdapter(List<UserDetailsModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ActiveMembersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_single_member_near, parent, false);
        return new ActiveMembersAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ActiveMembersAdapter.ViewHolder holder, int position) {
        UserDetailsModel listItem = listItems.get(position);
      holder.text_userName.setText(listItem.getUserName());

        Glide.with(holder.itemView)
                .load(Images_BaseUrl+listItem.getProfile_image())
                .fitCenter()
                .placeholder(R.drawable.ariana_user_ic)
                .into(holder.img_userProfile);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView text_userName;
        public ImageView img_userProfile;
        public ViewHolder(@NonNull View view) {
            super(view);

            text_userName = view.findViewById(R.id.text_userName_near);
            img_userProfile = view.findViewById(R.id.img_userProfile_near);

        }
    }
}