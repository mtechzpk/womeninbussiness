package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.UserProfileModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileAdapter extends RecyclerView.Adapter<UserProfileAdapter.ViewHolder> {
    List<UserProfileModel> listItems;
    Context context;

    public UserProfileAdapter(List<UserProfileModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_user_image_profile, parent, false);
        return new UserProfileAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserProfileModel listItem = listItems.get(position);
        holder.userProfileImage.setImageResource(listItem.getUserProfileImage());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView userProfileImage;
        public ViewHolder(@NonNull View view) {
            super(view);
            userProfileImage = view.findViewById(R.id.img_userProfile);
        }
    }
}
