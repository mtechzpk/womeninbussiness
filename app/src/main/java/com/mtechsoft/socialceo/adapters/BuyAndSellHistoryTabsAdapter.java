package com.mtechsoft.socialceo.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mtechsoft.socialceo.fragments.buyinghistory.BuyingHistoryFragment;
import com.mtechsoft.socialceo.fragments.sellinghistory.SellingHistoryFragment;

public class BuyAndSellHistoryTabsAdapter extends FragmentStatePagerAdapter {

    public BuyAndSellHistoryTabsAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new BuyingHistoryFragment();
            case 1:
                return new SellingHistoryFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Buying";
            case 1:
                return "Selling";
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
