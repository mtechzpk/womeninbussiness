package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.activities.ConnectPostActivity;
import com.mtechsoft.socialceo.model.UserPostModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserPostAdapter extends RecyclerView.Adapter<UserPostAdapter.ViewHolder> {
    List<UserPostModel> listItems;
    Context context;

    public UserPostAdapter(List<UserPostModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_post_design, parent, false);
        return new UserPostAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        UserPostModel listItem = listItems.get(position);
        holder.userName.setText(listItem.getUserName());
        holder.postLikes.setText(listItem.getUserLikes());
        holder.postComments.setText(listItem.getUserComments());
        holder.postDetail.setText(listItem.getUserPostDescription());
        holder.userProfile.setImageResource(listItem.getUserPostImage());
        holder.postImage.setImageResource(listItem.getUserPostImage());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(holder.itemView.getContext(), ConnectPostActivity.class);
                holder.itemView.getContext().startActivity(mainIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView userName, postLikes, postComments, postDetail, postTime;
        public ImageView postImage;
        public CircleImageView userProfile;
        public ViewHolder(View view) {
            super(view);
            userProfile = view.findViewById(R.id.img_userProfile);
            postImage = view.findViewById(R.id.img_Post);
            userName = view.findViewById(R.id.text_userName);
            postLikes = view.findViewById(R.id.text_PostLikes);
            postComments = view.findViewById(R.id.text_PostComments);
            postDetail = view.findViewById(R.id.text_PostDescription);
            postTime = view.findViewById(R.id.text_PostTime);

        }
    }
}
