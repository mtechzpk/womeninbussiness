package com.mtechsoft.socialceo.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mtechsoft.socialceo.fragments.details.DetailsFragment;
import com.mtechsoft.socialceo.fragments.reviews.ReviewsFragment;

public class UserDetailsReviewTabAdapter extends FragmentStatePagerAdapter {

    public UserDetailsReviewTabAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new DetailsFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Nullable
    @Override    public CharSequence getPageTitle(int position){
        switch (position) {
            case 0:
                return "Details";
            default:
                return null;
        }
    }
}
