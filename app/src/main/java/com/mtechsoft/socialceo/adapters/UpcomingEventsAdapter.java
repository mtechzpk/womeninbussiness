package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.UpcomingEventsModel;
import java.util.List;

public class UpcomingEventsAdapter extends RecyclerView.Adapter<UpcomingEventsAdapter.ViewHolder>{
    List<UpcomingEventsModel> listItems;
    Context context;
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";
    public UpcomingEventsAdapter(List<UpcomingEventsModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.upcoming_events_single_item, parent, false);
        return new UpcomingEventsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UpcomingEventsModel listItem = listItems.get(position);
        holder.eventDate.setText(listItem.getEvent_date());
        holder.eventPrice.setText(listItem.getPrice());
        holder.eventTitle.setText(listItem.getTitle());

        Glide.with(holder.itemView)
                .load(Images_BaseUrl+listItem.getImage())
                .fitCenter()
                .placeholder(R.drawable.ariana_user_ic)
                .into(holder.eventImage);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView eventImage;
        public TextView eventDate, eventPrice, eventTitle;

        public ViewHolder(@NonNull View view) {
            super(view);
            eventImage = view.findViewById(R.id.image_event);
            eventDate = view.findViewById(R.id.event_date);
            eventPrice = view.findViewById(R.id.event_Price);
            eventTitle = view.findViewById(R.id.event_details);

        }
    }
}
