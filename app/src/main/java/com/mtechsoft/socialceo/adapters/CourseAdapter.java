package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.model.CourseModel;

import java.util.List;

public class CourseAdapter extends PagerAdapter {
    List<CourseModel> listItems;
    Context context;

    public CourseAdapter(List<CourseModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }


    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_single_course , null );
        CourseModel listItem = listItems.get(position);
        ImageView courseImage = (ImageView) view.findViewById(R.id.img_Course);
        courseImage.setImageResource(listItem.getCourseImage());
        container.addView(view,0);
        return  view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //remove object from conatiner
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {

        return super.getItemPosition(object);
    }
}
