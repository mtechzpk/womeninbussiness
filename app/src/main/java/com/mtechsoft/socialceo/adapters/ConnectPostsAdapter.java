package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.activities.ConnectPostActivity;
import com.mtechsoft.socialceo.activities.UserProfileActivity;
import com.mtechsoft.socialceo.model.ConnectPostModel;
import com.mtechsoft.socialceo.model.SavePostResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectPostsAdapter extends RecyclerView.Adapter<ConnectPostsAdapter.ViewHolder> {
    List<ConnectPostModel> listItems;
    Context context;
    CallBack callBack;
    String favourite_status, saved_Status;
    String fav_status = "true";
    String save_status = "true";
    private static final String Images_BaseUrl = "http://mtecsoft.com/social_ceo/public/";

    public ConnectPostsAdapter() {

    }

    public ConnectPostsAdapter(List<ConnectPostModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    public ConnectPostsAdapter(List<ConnectPostModel> listItems, Context context, CallBack callBack) {
        this.listItems = listItems;
        this.context = context;
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public ConnectPostsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_post_design, parent, false);
        return new ConnectPostsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ConnectPostsAdapter.ViewHolder holder, int position) {


        ConnectPostModel listItem = listItems.get(position);

        holder.bind(position,holder.postLikes,holder.heart_ic);
        holder.userName.setText(listItem.getUserDetails().getUserName());
        holder.postLikes.setText(String.valueOf(listItem.getTotal_likes()));
        holder.postDetail.setText(listItem.getPost_description());
        holder.postComments.setText(String.valueOf(listItem.getComments().size()));


        DateFormat theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = theDateFormat.parse(listItem.getCreated_at ());
        } catch (ParseException parseException) {
            // Date is invalid. Do what you want.
        } catch (Exception exception) {
            // Generic catch. Do what you want.
        }
        theDateFormat = new SimpleDateFormat("MMM dd, yyyy");
        String[] arrayString = listItem.getCreated_at ().split(";");
        String time = arrayString[0];
        time = time.substring(time.indexOf("2020-10-12T") + 12, time.length());
//        tvDate.setText(theDateFormat.format(date));
        DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = null;
        try {
            d = f1.parse(time)
            ;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("h:mm a");
        String timeFormate=f2.format(d).toLowerCase(); // "12:18am"
        holder.postTime.setText (timeFormate);



        //postTime
        favourite_status = listItem.getIs_Liked();
        fav_status = favourite_status;
        checkForFavouriteStatus(holder.heart_ic, favourite_status);

        saved_Status = listItem.getIs_saved();
        save_status = saved_Status;
        checkForSavedStatus(holder.saved_ic, save_status);

        Glide.with(holder.itemView)
                .load(Images_BaseUrl+ listItem.getUserDetails().getProfile_image())
                .fitCenter()
                .placeholder(R.drawable.ariana_user_ic)
                .into(holder.userProfile);

        Glide.with(holder.itemView)
                .load(Images_BaseUrl+listItem.getPost_Image())
                .fitCenter()
                .into(holder.postImage);
        holder.userProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(holder.itemView.getContext(), UserProfileActivity.class);
                profileIntent.putExtra("userId", listItem.getUserDetails().getUserId());

                holder.itemView.getContext().startActivity(profileIntent);
            }
        });
        holder.postImage.setOnClickListener(v -> {
            Intent mainIntent = new Intent(holder.itemView.getContext(), ConnectPostActivity.class);
            mainIntent.putExtra("userProfile", Images_BaseUrl+ listItem.getUserDetails().getProfile_image());
            mainIntent.putExtra("userName", listItem.getUserDetails().getUserName());
            mainIntent.putExtra("postLikes", String.valueOf(listItem.getTotal_likes()));
            mainIntent.putExtra("postDescription", listItem.getPost_description());
            mainIntent.putExtra("activeAgo", listItem.getCreated_at());
            mainIntent.putExtra("postImage", Images_BaseUrl+listItem.getPost_Image());
            mainIntent.putExtra("postComments", String.valueOf(listItem.getComments().size()));
            mainIntent.putExtra("postId", String.valueOf(listItem.getPostId()));
            mainIntent.putExtra("like_status", listItem.getIs_Liked());
            mainIntent.putExtra("saved_status", listItem.getIs_saved());
            holder.itemView.getContext().startActivity(mainIntent);
        });


        holder.saved_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePost(String.valueOf(listItem.getUser_id()),String.valueOf(listItem.getPostId()));
                if (save_status.equals("false")){
                    holder.saved_ic.setImageResource(R.drawable.fillded_save_ic);
                    save_status="true";
                }
                else {

                    holder.saved_ic.setImageResource(R.drawable.bookmark_ic);
                    save_status="false";

                }

            }
        });

    }

    private void savePost(String userId,String postId) {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<SavePostResponseModel> modelCall = service.savePosts(userId,postId);
        modelCall.enqueue(new Callback<SavePostResponseModel>() {
            @Override
            public void onResponse(Call<SavePostResponseModel> call, Response<SavePostResponseModel> response) {
                int status = response.body().getStatus();
                if (status == 200){
                    Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SavePostResponseModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public void checkForFavouriteStatus(ImageView heart_Ic, String favourite_status){
        if (favourite_status !=null && favourite_status.equals("true")){
            heart_Ic.setImageResource(R.drawable.heart_filled_ic);
        }
        else {
            heart_Ic.setImageResource(R.drawable.heart_outline_ic);
        }
    }

    public void checkForSavedStatus(ImageView bookMark_ic, String saved_status){
        if (saved_status !=null && saved_status.equals("true")){
            bookMark_ic.setImageResource(R.drawable.fillded_save_ic);
        }
        else {
            bookMark_ic.setImageResource(R.drawable.bookmark_ic);
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView userName, postLikes, postComments, postDetail, postTime;
        public ImageView postImage, heart_ic, saved_ic;
        public CircleImageView userProfile;
        RelativeLayout layout_postLike;
        public ViewHolder(View view) {
            super(view);
            userProfile = view.findViewById(R.id.img_userProfile);
            postImage = view.findViewById(R.id.img_Post);
            userName = view.findViewById(R.id.text_userName);
            postLikes = view.findViewById(R.id.text_PostLikes);
            postComments = view.findViewById(R.id.text_PostComments);
            postDetail = view.findViewById(R.id.text_PostDescription);
            postTime = view.findViewById(R.id.text_PostTime);
            heart_ic = view.findViewById(R.id.heart_ic);
            layout_postLike = view.findViewById(R.id.layout_postLike);
            saved_ic = view.findViewById(R.id.saved_ic);

        }

        private void bind(int pos,TextView postLikes,ImageView heart_ic) {
            ConnectPostModel connectPostModel = listItems.get(pos);
            initClickListener();
        }

        private void initClickListener() {
            heart_ic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBack.onLikePost(getAdapterPosition(),postLikes,heart_ic);
                }
            });
        }
    }
    public interface CallBack{
        void onLikePost(int pos,TextView textView,ImageView imageView);
    }

}

