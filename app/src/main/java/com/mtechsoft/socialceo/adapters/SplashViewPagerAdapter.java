package com.mtechsoft.socialceo.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.activities.SocialConnectActivity;
import com.mtechsoft.socialceo.model.SplashViewPagerModel;

import java.util.List;

public class SplashViewPagerAdapter extends PagerAdapter {
    Context context;
    List<SplashViewPagerModel> listItems;

    public SplashViewPagerAdapter(Context context, List<SplashViewPagerModel> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull final ViewGroup container, int position) {

        final View view = LayoutInflater.from(context).inflate(R.layout.single_splash_item , null );
        SplashViewPagerModel listItem = listItems.get(position);
        ImageView splahImage = (ImageView) view.findViewById(R.id.splash_Image);
        TextView splashHeading = view.findViewById(R.id.splash_Heading);
        TextView splashDetails = view.findViewById(R.id.splash_details);
        splahImage.setImageResource(listItem.getSplashImage());
        splashHeading.setText(listItem.getSplashHeading());
        splashDetails.setText(listItem.getSplashDetails());
        Button btn_GetStarted = view.findViewById(R.id.btn_getStarted);
        if (listItem.getShowGetStarted()){
            btn_GetStarted.setVisibility(View.VISIBLE);
            btn_GetStarted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mainIntent = new Intent(view.getContext(), SocialConnectActivity.class);
                    view.getContext().startActivity(mainIntent);

                }
            });
        }
        else
        {
            btn_GetStarted.setVisibility(View.GONE);
        }
        container.addView(view,0);
        return  view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //remove object from conatiner
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }


}
