package com.mtechsoft.socialceo;


import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by apple on 12/18/17.
 */

public interface RestService {

    @Multipart
    @POST("create_post")
    Call<ResponseBody>  createPostToSell (
            @Part MultipartBody.Part file,
            @Part("user_id") RequestBody user_id,
            @Part("title") RequestBody title,
            @Part("category") RequestBody category,
            @Part("price") RequestBody price,
            @Part("link_of_product_or_service") RequestBody link_of_product_or_service,
            @Part("description") RequestBody description,
            @Part("post_type") RequestBody post_type,
            @Part("location") RequestBody location,
            @HeaderMap Map<String, String> headers


    );
  }
