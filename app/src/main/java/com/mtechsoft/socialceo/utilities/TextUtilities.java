package com.mtechsoft.socialceo.utilities;

import android.text.TextUtils;
import android.util.Patterns;

public class TextUtilities {

    public static boolean isValidEmail(CharSequence target) {
        boolean isTrue =  (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
        return isTrue;
    }

}
