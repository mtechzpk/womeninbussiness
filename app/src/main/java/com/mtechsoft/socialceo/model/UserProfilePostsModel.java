package com.mtechsoft.socialceo.model;

public class UserProfilePostsModel {
    private int profilePostImage;

    public UserProfilePostsModel() {
    }

    public UserProfilePostsModel(int profilePostImage) {
        this.profilePostImage = profilePostImage;
    }

    public int getProfilePostImage() {
        return profilePostImage;
    }

    public void setProfilePostImage(int profilePostImage) {
        this.profilePostImage = profilePostImage;
    }
}
