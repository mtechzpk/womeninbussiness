package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class CommentsModel {

    @SerializedName("id")
    private int id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("post_id")
    private String post_id;
    @SerializedName("comment")
    private String comment;
    @SerializedName("user_name")
    private String user_name;
    @SerializedName("profile_image")
    private String profile_image;

    public CommentsModel(int id, String user_id, String post_id, String comment, String user_name, String profile_image) {
        this.id = id;
        this.user_id = user_id;
        this.post_id = post_id;
        this.comment = comment;
        this.user_name = user_name;
        this.profile_image = profile_image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }
}