package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class EventResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private EventDataModel data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EventDataModel getData() {
        return data;
    }

    public void setData(EventDataModel data) {
        this.data = data;
    }
}
