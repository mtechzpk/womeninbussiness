package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NetworkResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<NetworkPostModel> data;

    public NetworkResponseModel() {
    }

    public NetworkResponseModel(int status, String message, List<NetworkPostModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NetworkPostModel> getData() {
        return data;
    }

    public void setData(List<NetworkPostModel> data) {
        this.data = data;
    }
}
