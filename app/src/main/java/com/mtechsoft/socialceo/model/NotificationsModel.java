package com.mtechsoft.socialceo.model;

public class NotificationsModel {
    private int userImage;
    private String userName;
    private String notificationDetails;
    private String notifcationTime;

    public NotificationsModel() {
    }

    public NotificationsModel(int userImage, String userName, String notificationDetails, String notifcationTime) {
        this.userImage = userImage;
        this.userName = userName;
        this.notificationDetails = notificationDetails;
        this.notifcationTime = notifcationTime;
    }

    public int getUserImage() {
        return userImage;
    }

    public void setUserImage(int userImage) {
        this.userImage = userImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNotificationDetails() {
        return notificationDetails;
    }

    public void setNotificationDetails(String notificationDetails) {
        this.notificationDetails = notificationDetails;
    }

    public String getNotifcationTime() {
        return notifcationTime;
    }

    public void setNotifcationTime(String notifcationTime) {
        this.notifcationTime = notifcationTime;
    }
}
