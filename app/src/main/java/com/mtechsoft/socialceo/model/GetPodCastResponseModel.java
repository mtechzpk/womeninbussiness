package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPodCastResponseModel {


    @SerializedName("status")
    private int status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<GetPodCastDataModel> data;

    public GetPodCastResponseModel(int status, String message, List<GetPodCastDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetPodCastDataModel> getData() {
        return data;
    }

    public void setData(List<GetPodCastDataModel> data) {
        this.data = data;
    }
}
