package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class PostToSellDataModel {

    @SerializedName("user_id")
    private String  user_id;
    @SerializedName("post_type")
    private String post_type;
     @SerializedName("post_file")
    private String  post_file;
    @SerializedName("title")
    private String title;
     @SerializedName("description")
    private String  description;
    @SerializedName("category")
    private String category;
     @SerializedName("price")
    private String  price;
    @SerializedName("link_of_product_or_service")
    private String link_of_product_or_service;
    @SerializedName("location")
    private String location;
    @SerializedName("id")
    private int id;

    public PostToSellDataModel(String user_id, String post_type, String post_file, String title, String description, String category, String price, String link_of_product_or_service,String location ,int id) {
        this.user_id = user_id;
        this.post_type = post_type;
        this.post_file = post_file;
        this.title = title;
        this.description = description;
        this.category = category;
        this.price = price;
        this.link_of_product_or_service = link_of_product_or_service;
        this.location = location;
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getImage() {
        return post_file;
    }

    public void setImage(String image) {
        this.post_file = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLink_of_product_or_service() {
        return link_of_product_or_service;
    }

    public void setLink_of_product_or_service(String link_of_product_or_service) {
        this.link_of_product_or_service = link_of_product_or_service;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

