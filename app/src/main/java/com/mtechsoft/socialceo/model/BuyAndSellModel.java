package com.mtechsoft.socialceo.model;
import com.google.gson.annotations.SerializedName;

public class BuyAndSellModel {
    @SerializedName("id")
    private int postId;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("post_type")
    private String post_type;
    @SerializedName("image")
    private String post_Image;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String post_description;
    @SerializedName("category")
    private String category;
    @SerializedName("price")
    private String price;
    @SerializedName("location")
    private String location;
    @SerializedName("is_saved")
    private String is_saved;
    @SerializedName("link_of_product_or_service")
    private String link_of_product_or_service;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("user_details")
    private UserDetailsModel userDetails;

    public BuyAndSellModel() {
    }

    public BuyAndSellModel(int postId, int user_id, String post_type, String post_Image, String title, String post_description, String category, String price, String location, String is_saved, String link_of_product_or_service, String created_at, UserDetailsModel userDetails) {
        this.postId = postId;
        this.user_id = user_id;
        this.post_type = post_type;
        this.post_Image = post_Image;
        this.title = title;
        this.post_description = post_description;
        this.category = category;
        this.price = price;
        this.location = location;
        this.is_saved = is_saved;
        this.link_of_product_or_service = link_of_product_or_service;
        this.created_at = created_at;
        this.userDetails = userDetails;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getPost_Image() {
        return post_Image;
    }

    public void setPost_Image(String post_Image) {
        this.post_Image = post_Image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPost_description() {
        return post_description;
    }

    public void setPost_description(String post_description) {
        this.post_description = post_description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIs_saved() {
        return is_saved;
    }

    public void setIs_saved(String is_saved) {
        this.is_saved = is_saved;
    }

    public String getLink_of_product_or_service() {
        return link_of_product_or_service;
    }

    public void setLink_of_product_or_service(String link_of_product_or_service) {
        this.link_of_product_or_service = link_of_product_or_service;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public UserDetailsModel getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetailsModel userDetails) {
        this.userDetails = userDetails;
    }
}

