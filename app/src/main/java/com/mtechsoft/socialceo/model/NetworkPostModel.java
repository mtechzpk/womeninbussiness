package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class NetworkPostModel {

    @SerializedName("id")
    private int postId;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("post_type")
    private String post_type;
    @SerializedName("image")
    private String post_Image;
    @SerializedName("description")
    private String post_description;
    @SerializedName("network_post_type")
    private String network_post_type;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("is_saved")
    private String is_saved;
    @SerializedName("is_liked")
    private String is_liked;
    @SerializedName("total_likes")
    private int total_likes;

    @SerializedName("user_details")
    private UserDetailsModel userDetails;


    public NetworkPostModel() {
    }

    public NetworkPostModel(int postId, int user_id, String post_type, String post_Image, String post_description, String network_post_type, String created_at, String is_saved, String is_liked, int total_likes, UserDetailsModel userDetails) {
        this.postId = postId;
        this.user_id = user_id;
        this.post_type = post_type;
        this.post_Image = post_Image;
        this.post_description = post_description;
        this.network_post_type = network_post_type;
        this.created_at = created_at;
        this.is_saved = is_saved;
        this.is_liked = is_liked;
        this.total_likes = total_likes;
        this.userDetails = userDetails;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getPost_Image() {
        return post_Image;
    }

    public void setPost_Image(String post_Image) {
        this.post_Image = post_Image;
    }

    public String getPost_description() {
        return post_description;
    }

    public void setPost_description(String post_description) {
        this.post_description = post_description;
    }

    public String getNetwork_post_type() {
        return network_post_type;
    }

    public void setNetwork_post_type(String network_post_type) {
        this.network_post_type = network_post_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIs_saved() {
        return is_saved;
    }

    public void setIs_saved(String is_saved) {
        this.is_saved = is_saved;
    }

    public String getIs_liked() {
        return is_liked;
    }

    public void setIs_liked(String is_liked) {
        this.is_liked = is_liked;
    }

    public int getTotal_likes() {
        return total_likes;
    }

    public void setTotal_likes(int total_likes) {
        this.total_likes = total_likes;
    }

    public UserDetailsModel getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetailsModel userDetails) {
        this.userDetails = userDetails;
    }
}