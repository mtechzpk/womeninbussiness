package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class StartupPostResponseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private StartupPostDataModel data;

    public StartupPostResponseModel(int status, String message, StartupPostDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StartupPostDataModel getData() {
        return data;
    }

    public void setData(StartupPostDataModel data) {
        this.data = data;
    }

}
