package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class GetPodCastDataModel {

    @SerializedName("id")
    private int id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("post_type")
    private String post_type;

    @SerializedName("image")
    private String image;

    @SerializedName("title")
    private String title;

    @SerializedName("category")
    private String category;

    @SerializedName("description")
    private String link;

    @SerializedName("tags")
    private String tags;

    @SerializedName("service_fee")
    private String service_fee;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("is_saved")
    private String is_saved;


    @SerializedName("user_details")
    private UserDetailsModel user_details;


    public GetPodCastDataModel(int id, String user_id, String post_type, String image, String title, String category, String link, String tags, String service_fee, String created_at, String is_saved, UserDetailsModel user_details) {
        this.id = id;
        this.user_id = user_id;
        this.post_type = post_type;
        this.image = image;
        this.title = title;
        this.category = category;
        this.link = link;
        this.tags = tags;
        this.service_fee = service_fee;
        this.created_at = created_at;
        this.is_saved = is_saved;
        this.user_details = user_details;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getService_fee() {
        return service_fee;
    }

    public void setService_fee(String service_fee) {
        this.service_fee = service_fee;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIs_saved() {
        return is_saved;
    }

    public void setIs_saved(String is_saved) {
        this.is_saved = is_saved;
    }

    public UserDetailsModel getUser_details() {
        return user_details;
    }

    public void setUser_details(UserDetailsModel user_details) {
        this.user_details = user_details;
    }
}
