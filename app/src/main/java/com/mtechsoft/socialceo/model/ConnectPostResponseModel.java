package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConnectPostResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<ConnectPostModel> data;

    public ConnectPostResponseModel() {
    }

    public ConnectPostResponseModel(int status, String message, List<ConnectPostModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ConnectPostModel> getData() {
        return data;
    }

    public void setData(List<ConnectPostModel> data) {
        this.data = data;
    }
}
