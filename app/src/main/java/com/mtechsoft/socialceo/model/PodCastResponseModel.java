package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class PodCastResponseModel {

    @SerializedName("status")
    private int status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private PodCastPostDataModel data;


    public PodCastResponseModel(int status, String message, PodCastPostDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PodCastPostDataModel getData() {
        return data;
    }

    public void setData(PodCastPostDataModel data) {
        this.data = data;
    }
}
