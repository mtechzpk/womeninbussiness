package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class PostToSellResponseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private PostToSellDataModel data;

    public PostToSellResponseModel(int status, String message, PostToSellDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PostToSellDataModel getData() {
        return data;
    }

    public void setData(PostToSellDataModel data) {
        this.data = data;
    }
}

