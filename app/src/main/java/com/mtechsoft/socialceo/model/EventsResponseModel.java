package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventsResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<EventsDataModel> data;

    public EventsResponseModel() {
    }

    public EventsResponseModel(int status, String message, List<EventsDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<EventsDataModel> getData() {
        return data;
    }

    public void setData(List<EventsDataModel> data) {
        this.data = data;
    }
}
