package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class UserDetailsResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private UserDetailedDataModel data;

    public UserDetailsResponseModel() {
    }

    public UserDetailsResponseModel(int status, String message, UserDetailedDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDetailedDataModel getData() {
        return data;
    }

    public void setData(UserDetailedDataModel data) {
        this.data = data;
    }
}
