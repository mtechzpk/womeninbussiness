package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class StartupDataModel {
    @SerializedName("id")
    private int post_id;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("post_type")
    private String post_type;
    @SerializedName("image")
    private String image;
    @SerializedName("title")
    private String title;
    @SerializedName("startup_investment")
    private String startup_investment;
    @SerializedName("pledge_goal_amount")
    private String pledge_goal_amount;
    @SerializedName("startup_end_date")
    private String startup_end_date;
    @SerializedName("description")
    private String description;
    @SerializedName("investment_type")
    private String investment_type;
    @SerializedName("location")
    private String location;
    @SerializedName("next_step_for_startup")
    private String next_step_for_startup;
    @SerializedName("feel_about_next_step")
    private String feel_about_next_step;
    @SerializedName("startup_option_1")
    private String startup_option_1;
    @SerializedName("startup_option_2")
    private String startup_option_2;
    @SerializedName("startup_option_3")
    private String startup_option_3;
    @SerializedName("how_far_along_startup")
    private String how_far_along_startup;
    @SerializedName("money_need_for_startup")
    private String money_need_for_startup;
    @SerializedName("planned_time_for_startup")
    private String planned_time_for_startup;
    @SerializedName("do_you_have_enough_money")
    private String do_you_have_enough_money;
    @SerializedName("access_to_startup_network")
    private String access_to_startup_network;
    @SerializedName("is_saved")
    private String is_saved;
    @SerializedName("user_details")
    private UserDetailsModel user_details;

    public StartupDataModel() {
    }

    public StartupDataModel(int post_id, int user_id, String post_type, String image, String title, String startup_investment, String pledge_goal_amount, String startup_end_date, String description, String investment_type, String location, String next_step_for_startup, String feel_about_next_step, String startup_option_1, String startup_option_2, String startup_option_3, String how_far_along_startup, String money_need_for_startup, String planned_time_for_startup, String do_you_have_enough_money, String access_to_startup_network, String is_saved, UserDetailsModel user_details) {
        this.post_id = post_id;
        this.user_id = user_id;
        this.post_type = post_type;
        this.image = image;
        this.title = title;
        this.startup_investment = startup_investment;
        this.pledge_goal_amount = pledge_goal_amount;
        this.startup_end_date = startup_end_date;
        this.description = description;
        this.investment_type = investment_type;
        this.location = location;
        this.next_step_for_startup = next_step_for_startup;
        this.feel_about_next_step = feel_about_next_step;
        this.startup_option_1 = startup_option_1;
        this.startup_option_2 = startup_option_2;
        this.startup_option_3 = startup_option_3;
        this.how_far_along_startup = how_far_along_startup;
        this.money_need_for_startup = money_need_for_startup;
        this.planned_time_for_startup = planned_time_for_startup;
        this.do_you_have_enough_money = do_you_have_enough_money;
        this.access_to_startup_network = access_to_startup_network;
        this.is_saved = is_saved;
        this.user_details = user_details;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartup_investment() {
        return startup_investment;
    }

    public void setStartup_investment(String startup_investment) {
        this.startup_investment = startup_investment;
    }

    public String getPledge_goal_amount() {
        return pledge_goal_amount;
    }

    public void setPledge_goal_amount(String pledge_goal_amount) {
        this.pledge_goal_amount = pledge_goal_amount;
    }

    public String getStartup_end_date() {
        return startup_end_date;
    }

    public void setStartup_end_date(String startup_end_date) {
        this.startup_end_date = startup_end_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInvestment_type() {
        return investment_type;
    }

    public void setInvestment_type(String investment_type) {
        this.investment_type = investment_type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNext_step_for_startup() {
        return next_step_for_startup;
    }

    public void setNext_step_for_startup(String next_step_for_startup) {
        this.next_step_for_startup = next_step_for_startup;
    }

    public String getFeel_about_next_step() {
        return feel_about_next_step;
    }

    public void setFeel_about_next_step(String feel_about_next_step) {
        this.feel_about_next_step = feel_about_next_step;
    }

    public String getStartup_option_1() {
        return startup_option_1;
    }

    public void setStartup_option_1(String startup_option_1) {
        this.startup_option_1 = startup_option_1;
    }

    public String getStartup_option_2() {
        return startup_option_2;
    }

    public void setStartup_option_2(String startup_option_2) {
        this.startup_option_2 = startup_option_2;
    }

    public String getStartup_option_3() {
        return startup_option_3;
    }

    public void setStartup_option_3(String startup_option_3) {
        this.startup_option_3 = startup_option_3;
    }

    public String getHow_far_along_startup() {
        return how_far_along_startup;
    }

    public void setHow_far_along_startup(String how_far_along_startup) {
        this.how_far_along_startup = how_far_along_startup;
    }

    public String getMoney_need_for_startup() {
        return money_need_for_startup;
    }

    public void setMoney_need_for_startup(String money_need_for_startup) {
        this.money_need_for_startup = money_need_for_startup;
    }

    public String getPlanned_time_for_startup() {
        return planned_time_for_startup;
    }

    public void setPlanned_time_for_startup(String planned_time_for_startup) {
        this.planned_time_for_startup = planned_time_for_startup;
    }

    public String getDo_you_have_enough_money() {
        return do_you_have_enough_money;
    }

    public void setDo_you_have_enough_money(String do_you_have_enough_money) {
        this.do_you_have_enough_money = do_you_have_enough_money;
    }

    public String getAccess_to_startup_network() {
        return access_to_startup_network;
    }

    public void setAccess_to_startup_network(String access_to_startup_network) {
        this.access_to_startup_network = access_to_startup_network;
    }

    public String getIs_saved() {
        return is_saved;
    }

    public void setIs_saved(String is_saved) {
        this.is_saved = is_saved;
    }

    public UserDetailsModel getUser_details() {
        return user_details;
    }

    public void setUser_details(UserDetailsModel user_details) {
        this.user_details = user_details;
    }
}

