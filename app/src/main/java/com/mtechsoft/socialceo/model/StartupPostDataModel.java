package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class StartupPostDataModel {
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("post_type")
    private String post_type;
    @SerializedName("image")
    private String image;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("service_fee")
    private String service_fee;
    @SerializedName("startup_investment")
    private String startup_investment;
    @SerializedName("pledge_goal_amount")
    private String userpledge_goal_amount_id;
    @SerializedName("startup_end_date")
    private String startup_end_date;
    @SerializedName("investment_type")
    private String investment_type;
    @SerializedName("location")
    private String location;
    @SerializedName("next_step_for_startup")
    private String next_step_for_startup;
    @SerializedName("feel_about_next_step")
    private String feel_about_next_step;
    @SerializedName("startup_option_1")
    private String startup_option_1;
    @SerializedName("startup_option_2")
    private String startup_option_2;
    @SerializedName("startup_option_3")
    private String startup_option_3;
    @SerializedName("how_far_along_startup")
    private String how_far_along_startup;
    @SerializedName("money_need_for_startup")
    private String money_need_for_startup;
    @SerializedName("planned_time_for_startup")
    private String planned_time_for_startup;
    @SerializedName("do_you_have_enough_money")
    private String do_you_have_enough_money;
    @SerializedName("access_to_startup_network")
    private String access_to_startup_network;
    @SerializedName("id")
    private int id;

    public StartupPostDataModel(String user_id, String post_type, String image, String title, String description, String service_fee, String startup_investment, String userpledge_goal_amount_id, String startup_end_date, String investment_type, String location, String next_step_for_startup, String feel_about_next_step, String startup_option_1, String startup_option_2, String startup_option_3, String how_far_along_startup, String money_need_for_startup, String planned_time_for_startup, String do_you_have_enough_money, String access_to_startup_network, int id) {
        this.user_id = user_id;
        this.post_type = post_type;
        this.image = image;
        this.title = title;
        this.description = description;
        this.service_fee = service_fee;
        this.startup_investment = startup_investment;
        this.userpledge_goal_amount_id = userpledge_goal_amount_id;
        this.startup_end_date = startup_end_date;
        this.investment_type = investment_type;
        this.location = location;
        this.next_step_for_startup = next_step_for_startup;
        this.feel_about_next_step = feel_about_next_step;
        this.startup_option_1 = startup_option_1;
        this.startup_option_2 = startup_option_2;
        this.startup_option_3 = startup_option_3;
        this.how_far_along_startup = how_far_along_startup;
        this.money_need_for_startup = money_need_for_startup;
        this.planned_time_for_startup = planned_time_for_startup;
        this.do_you_have_enough_money = do_you_have_enough_money;
        this.access_to_startup_network = access_to_startup_network;
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getService_fee() {
        return service_fee;
    }

    public void setService_fee(String service_fee) {
        this.service_fee = service_fee;
    }

    public String getStartup_investment() {
        return startup_investment;
    }

    public void setStartup_investment(String startup_investment) {
        this.startup_investment = startup_investment;
    }

    public String getUserpledge_goal_amount_id() {
        return userpledge_goal_amount_id;
    }

    public void setUserpledge_goal_amount_id(String userpledge_goal_amount_id) {
        this.userpledge_goal_amount_id = userpledge_goal_amount_id;
    }

    public String getStartup_end_date() {
        return startup_end_date;
    }

    public void setStartup_end_date(String startup_end_date) {
        this.startup_end_date = startup_end_date;
    }

    public String getInvestment_type() {
        return investment_type;
    }

    public void setInvestment_type(String investment_type) {
        this.investment_type = investment_type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNext_step_for_startup() {
        return next_step_for_startup;
    }

    public void setNext_step_for_startup(String next_step_for_startup) {
        this.next_step_for_startup = next_step_for_startup;
    }

    public String getFeel_about_next_step() {
        return feel_about_next_step;
    }

    public void setFeel_about_next_step(String feel_about_next_step) {
        this.feel_about_next_step = feel_about_next_step;
    }

    public String getStartup_option_1() {
        return startup_option_1;
    }

    public void setStartup_option_1(String startup_option_1) {
        this.startup_option_1 = startup_option_1;
    }

    public String getStartup_option_2() {
        return startup_option_2;
    }

    public void setStartup_option_2(String startup_option_2) {
        this.startup_option_2 = startup_option_2;
    }

    public String getStartup_option_3() {
        return startup_option_3;
    }

    public void setStartup_option_3(String startup_option_3) {
        this.startup_option_3 = startup_option_3;
    }

    public String getHow_far_along_startup() {
        return how_far_along_startup;
    }

    public void setHow_far_along_startup(String how_far_along_startup) {
        this.how_far_along_startup = how_far_along_startup;
    }

    public String getMoney_need_for_startup() {
        return money_need_for_startup;
    }

    public void setMoney_need_for_startup(String money_need_for_startup) {
        this.money_need_for_startup = money_need_for_startup;
    }

    public String getPlanned_time_for_startup() {
        return planned_time_for_startup;
    }

    public void setPlanned_time_for_startup(String planned_time_for_startup) {
        this.planned_time_for_startup = planned_time_for_startup;
    }

    public String getDo_you_have_enough_money() {
        return do_you_have_enough_money;
    }

    public void setDo_you_have_enough_money(String do_you_have_enough_money) {
        this.do_you_have_enough_money = do_you_have_enough_money;
    }

    public String getAccess_to_startup_network() {
        return access_to_startup_network;
    }

    public void setAccess_to_startup_network(String access_to_startup_network) {
        this.access_to_startup_network = access_to_startup_network;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
