package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class NearDataModel {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("profile_image")
    private String profile_image;

    public NearDataModel(int id, String name, String profile_image) {
        this.id = id;
        this.name = name;
        this.profile_image = profile_image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

}