package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserDetailedDataModel {
    @SerializedName("id")
    private int userId;
    @SerializedName("name")
    private String userName;
    @SerializedName("email")
    private String userEmail;
    @SerializedName("phone")
    private String userPhone;
    @SerializedName("profile_image")
    private String profile_image;
    @SerializedName("user_posts")
    private List<SinglePostModel> userPosts;

    public UserDetailedDataModel() {
    }

    public UserDetailedDataModel(int userId, String userName, String userEmail, String userPhone, String profile_image, List<SinglePostModel> userPosts) {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
        this.profile_image = profile_image;
        this.userPosts = userPosts;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public List<SinglePostModel> getUserPosts() {
        return userPosts;
    }

    public void setUserPosts(List<SinglePostModel> userPosts) {
        this.userPosts = userPosts;
    }
}
