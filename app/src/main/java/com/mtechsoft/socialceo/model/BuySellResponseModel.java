package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuySellResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<BuyAndSellModel> data;

    public BuySellResponseModel() {
    }

    public BuySellResponseModel(int status, String message, List<BuyAndSellModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BuyAndSellModel> getData() {
        return data;
    }

    public void setData(List<BuyAndSellModel> data) {
        this.data = data;
    }
}
