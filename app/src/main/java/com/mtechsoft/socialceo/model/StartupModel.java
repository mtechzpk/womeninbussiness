package com.mtechsoft.socialceo.model;

public class StartupModel {
    private int startupImage;
    private String startupName;
    private String startupBackers;
    private String startupPrice;
    private String startupPledges;

    public StartupModel() {
    }

    public StartupModel(int startupImage, String startupName, String startupBackers, String startupPrice, String startupPledges) {
        this.startupImage = startupImage;
        this.startupName = startupName;
        this.startupBackers = startupBackers;
        this.startupPrice = startupPrice;
        this.startupPledges = startupPledges;
    }

    public int getStartupImage() {
        return startupImage;
    }

    public void setStartupImage(int startupImage) {
        this.startupImage = startupImage;
    }

    public String getStartupName() {
        return startupName;
    }

    public void setStartupName(String startupName) {
        this.startupName = startupName;
    }

    public String getStartupBackers() {
        return startupBackers;
    }

    public void setStartupBackers(String startupBackers) {
        this.startupBackers = startupBackers;
    }

    public String getStartupPrice() {
        return startupPrice;
    }

    public void setStartupPrice(String startupPrice) {
        this.startupPrice = startupPrice;
    }

    public String getStartupPledges() {
        return startupPledges;
    }

    public void setStartupPledges(String startupPledges) {
        this.startupPledges = startupPledges;
    }
}
