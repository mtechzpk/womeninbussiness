package com.mtechsoft.socialceo.model;

public class MemberNearModel {

    private int userProfileImage;
    private String userName;

    public MemberNearModel() {
    }

    public MemberNearModel(int userProfileImage, String userName) {
        this.userProfileImage = userProfileImage;
        this.userName = userName;
    }

    public int getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(int userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
