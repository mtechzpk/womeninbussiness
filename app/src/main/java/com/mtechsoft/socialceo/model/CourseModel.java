package com.mtechsoft.socialceo.model;

public class CourseModel {
    private int courseImage;

    public CourseModel() {
    }

    public CourseModel(int courseImage) {
        this.courseImage = courseImage;
    }

    public int getCourseImage() {
        return courseImage;
    }

    public void setCourseImage(int courseImage) {
        this.courseImage = courseImage;
    }


}
