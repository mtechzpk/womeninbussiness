package com.mtechsoft.socialceo.model;

public class BuySellModel {

    private int imgCourse;
    private String courseName;
    private String courseRating;
    private String coursePrice;

    public BuySellModel() {
    }

    public BuySellModel(int imgCourse, String courseName, String courseRating, String coursePrice) {
        this.imgCourse = imgCourse;
        this.courseName = courseName;
        this.courseRating = courseRating;
        this.coursePrice = coursePrice;
    }

    public int getImgCourse() {
        return imgCourse;
    }

    public void setImgCourse(int imgCourse) {
        this.imgCourse = imgCourse;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseRating() {
        return courseRating;
    }

    public void setCourseRating(String courseRating) {
        this.courseRating = courseRating;
    }

    public String getCoursePrice() {
        return coursePrice;
    }

    public void setCoursePrice(String coursePrice) {
        this.coursePrice = coursePrice;
    }
}
