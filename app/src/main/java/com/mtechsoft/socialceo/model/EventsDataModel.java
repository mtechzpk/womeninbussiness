package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class EventsDataModel {
    @SerializedName("id")
    private int post_id;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("post_type")
    private String post_type;
    @SerializedName("image")
    private String image;
    @SerializedName("title")
    private String title;
    @SerializedName("category")
    private String category;
    @SerializedName("price")
    private String price;
    @SerializedName("location")
    private String location;
    @SerializedName("event_date")
    private String event_date;
    @SerializedName("description")
    private String description;
    @SerializedName("tags")
    private String tags;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("is_saved")
    private String is_saved;
    @SerializedName("available_seats")
    private String available_seats;
    @SerializedName("user_details")
    private UserDetailsModel user_details;

    public EventsDataModel() {
    }

    public EventsDataModel(int post_id, int user_id, String post_type, String image, String title, String category, String price, String location, String event_date, String description, String tags, String created_at, String is_saved, String available_seats, UserDetailsModel user_details) {
        this.post_id = post_id;
        this.user_id = user_id;
        this.post_type = post_type;
        this.image = image;
        this.title = title;
        this.category = category;
        this.price = price;
        this.location = location;
        this.event_date = event_date;
        this.description = description;
        this.tags = tags;
        this.created_at = created_at;
        this.is_saved = is_saved;
        this.available_seats = available_seats;
        this.user_details = user_details;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIs_saved() {
        return is_saved;
    }

    public void setIs_saved(String is_saved) {
        this.is_saved = is_saved;
    }

    public String getAvailable_seats() {
        return available_seats;
    }

    public void setAvailable_seats(String available_seats) {
        this.available_seats = available_seats;
    }

    public UserDetailsModel getUser_details() {
        return user_details;
    }

    public void setUser_details(UserDetailsModel user_details) {
        this.user_details = user_details;
    }
}

