package com.mtechsoft.socialceo.model;

public class UserPostModel {
    private String userName;
    private String userLikes;
    private String userComments;
    private String userPostTime;
    private int userProfileImage;
    private int userPostImage;
    private String userPostDescription;

    public UserPostModel() {
    }
    public UserPostModel(String userName, String userLikes, String userComments, String userPostTime, int userProfileImage, int userPostImage, String userPostDescription) {
        this.userName = userName;
        this.userLikes = userLikes;
        this.userComments = userComments;
        this.userPostTime = userPostTime;
        this.userProfileImage = userProfileImage;
        this.userPostImage = userPostImage;
        this.userPostDescription = userPostDescription;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLikes() {
        return userLikes;
    }

    public void setUserLikes(String userLikes) {
        this.userLikes = userLikes;
    }

    public String getUserComments() {
        return userComments;
    }

    public void setUserComments(String userComments) {
        this.userComments = userComments;
    }

    public String getUserPostTime() {
        return userPostTime;
    }

    public void setUserPostTime(String userPostTime) {
        this.userPostTime = userPostTime;
    }

    public int getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(int userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public int getUserPostImage() {
        return userPostImage;
    }

    public void setUserPostImage(int userPostImage) {
        this.userPostImage = userPostImage;
    }

    public String getUserPostDescription() {
        return userPostDescription;
    }

    public void setUserPostDescription(String userPostDescription) {
        this.userPostDescription = userPostDescription;
    }
}
