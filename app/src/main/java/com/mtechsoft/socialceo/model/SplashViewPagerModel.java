package com.mtechsoft.socialceo.model;

public class SplashViewPagerModel {
    public int splashImage;
    public String splashHeading;
    public String splashDetails;
    public Boolean showGetStarted;

    public SplashViewPagerModel() {
    }

    public SplashViewPagerModel(int splashImage, String splashHeading, String splashDetails, Boolean showGetStarted) {
        this.splashImage = splashImage;
        this.splashHeading = splashHeading;
        this.splashDetails = splashDetails;
        this.showGetStarted = showGetStarted;
    }

    public int getSplashImage() {
        return splashImage;
    }

    public void setSplashImage(int splashImage) {
        this.splashImage = splashImage;
    }

    public String getSplashHeading() {
        return splashHeading;
    }

    public void setSplashHeading(String splashHeading) {
        this.splashHeading = splashHeading;
    }

    public String getSplashDetails() {
        return splashDetails;
    }

    public void setSplashDetails(String splashDetails) {
        this.splashDetails = splashDetails;
    }

    public Boolean getShowGetStarted() {
        return showGetStarted;
    }

    public void setShowGetStarted(Boolean showGetStarted) {
        this.showGetStarted = showGetStarted;
    }
}
