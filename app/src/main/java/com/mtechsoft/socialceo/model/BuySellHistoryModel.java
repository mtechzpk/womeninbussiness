package com.mtechsoft.socialceo.model;

public class BuySellHistoryModel {
    private int image;
    private String title;
    private String price;
    private String rating;
    private String dateTime;
    public BuySellHistoryModel() {
    }

    public BuySellHistoryModel(int image, String title, String price, String rating, String dateTime) {
        this.image = image;
        this.title = title;
        this.price = price;
        this.rating = rating;
        this.dateTime = dateTime;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
