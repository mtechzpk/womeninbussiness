package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ActiveUsersResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<UserDetailsModel> userDetails;

    public ActiveUsersResponseModel() {

    }

    public ActiveUsersResponseModel(int status, String message, List<UserDetailsModel> userDetails) {
        this.status = status;
        this.message = message;
        this.userDetails = userDetails;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserDetailsModel> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(List<UserDetailsModel> userDetails) {
        this.userDetails = userDetails;
    }
}
