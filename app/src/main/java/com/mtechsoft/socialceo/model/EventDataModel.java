package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class EventDataModel {
    @SerializedName("user_id")
    private String  user_id;
    @SerializedName("post_type")
    private String post_type;
    @SerializedName("image")
    private String  image;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String  description;
    @SerializedName("category")
    private String category;
    @SerializedName("price")
    private String  price;
    @SerializedName("event_date")
    private String event_date;
    @SerializedName("tags")
    private String tags;
    @SerializedName("id")
    private int id;

    public EventDataModel(String user_id, String post_type, String image, String title, String description, String category, String price, String event_date, String tags, int id) {
        this.user_id = user_id;
        this.post_type = post_type;
        this.image = image;
        this.title = title;
        this.description = description;
        this.category = category;
        this.price = price;
        this.event_date = event_date;
        this.tags = tags;
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
