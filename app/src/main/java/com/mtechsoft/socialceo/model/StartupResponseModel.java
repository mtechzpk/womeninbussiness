package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StartupResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<StartupDataModel> data;

    public StartupResponseModel() {
    }

    public StartupResponseModel(int status, String message, List<StartupDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StartupDataModel> getData() {
        return data;
    }

    public void setData(List<StartupDataModel> data) {
        this.data = data;
    }
}
