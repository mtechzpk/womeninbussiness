package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class RegisterDataModel {
        @SerializedName("id")
        private int id;
        @SerializedName("name")
        private String name;
        @SerializedName("email")
        private String email;
        @SerializedName("email_verified_at")
        private String email_verified_at;
        @SerializedName("phone")
        private String phone;
        @SerializedName("profile_image")
        private String profile_image;
        @SerializedName("token")
        private String token;
        @SerializedName("latitude")
        private String latitude;
        @SerializedName("longitude")
        private String longitude;
        @SerializedName("is_active_user")
        private String is_active_user;
        @SerializedName("payment_method_name")
        private String payment_method_name;
        @SerializedName("name_on_card")
        private String name_on_card;
        @SerializedName("card_number")
        private String card_number;
        @SerializedName("expiry_date")
        private String expiry_date;
        @SerializedName("cvv")
        private String cvv;
        @SerializedName("notifications")
        private String notifications;
        @SerializedName("is_online")
        private String is_online;

        public RegisterDataModel() {
        }

    public RegisterDataModel(int id, String name, String email, String email_verified_at, String phone, String profile_image, String latitude, String longitude, String is_active_user, String payment_method_name, String name_on_card, String card_number, String expiry_date, String cvv, String notifications, String is_online) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.email_verified_at = email_verified_at;
        this.phone = phone;
        this.profile_image = profile_image;
        this.latitude = latitude;
        this.longitude = longitude;
        this.is_active_user = is_active_user;
        this.payment_method_name = payment_method_name;
        this.name_on_card = name_on_card;
        this.card_number = card_number;
        this.expiry_date = expiry_date;
        this.cvv = cvv;
        this.notifications = notifications;
        this.is_online = is_online;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail_verified_at() {
        return email_verified_at;
    }

    public void setEmail_verified_at(String email_verified_at) {
        this.email_verified_at = email_verified_at;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIs_active_user() {
        return is_active_user;
    }

    public void setIs_active_user(String is_active_user) {
        this.is_active_user = is_active_user;
    }

    public String getPayment_method_name() {
        return payment_method_name;
    }

    public void setPayment_method_name(String payment_method_name) {
        this.payment_method_name = payment_method_name;
    }

    public String getName_on_card() {
        return name_on_card;
    }

    public void setName_on_card(String name_on_card) {
        this.name_on_card = name_on_card;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getNotifications() {
        return notifications;
    }

    public void setNotifications(String notifications) {
        this.notifications = notifications;
    }

    public String getIs_online() {
        return is_online;
    }

    public void setIs_online(String is_online) {
        this.is_online = is_online;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
