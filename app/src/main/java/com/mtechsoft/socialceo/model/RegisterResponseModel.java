package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class RegisterResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private RegisterDataModel data;

    public RegisterResponseModel() {
    }

    public RegisterResponseModel(int status, String message, RegisterDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RegisterDataModel getData() {
        return data;
    }

    public void setData(RegisterDataModel data) {
        this.data = data;
    }


}

