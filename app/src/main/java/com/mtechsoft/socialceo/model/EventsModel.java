package com.mtechsoft.socialceo.model;

public class EventsModel {
    private int eventImage;
    private String eventName;
    private String eventDate;
    private String eventPrice;

    public EventsModel() {
    }

    public EventsModel(int eventImage, String eventName, String eventDate, String eventPrice) {
        this.eventImage = eventImage;
        this.eventName = eventName;
        this.eventDate = eventDate;
        this.eventPrice = eventPrice;
    }

    public int getEventImage() {
        return eventImage;
    }

    public void setEventImage(int eventImage) {
        this.eventImage = eventImage;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventPrice() {
        return eventPrice;
    }

    public void setEventPrice(String eventPrice) {
        this.eventPrice = eventPrice;
    }
}
