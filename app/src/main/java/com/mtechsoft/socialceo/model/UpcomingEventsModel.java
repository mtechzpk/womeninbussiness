package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class UpcomingEventsModel {
    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("price")
    private String price;
    @SerializedName("event_date")
    private String event_date;

    public UpcomingEventsModel() {
    }

    public UpcomingEventsModel(int id, String title, String image, String price, String event_date) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.price = price;
        this.event_date = event_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }
}
