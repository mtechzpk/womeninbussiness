package com.mtechsoft.socialceo.model;

public class UserReviewModel {
    private String userName;
    private String reviewTime;
    private String reviewDetails;
    private float reviewRating;
    private int userProfile;

    public UserReviewModel() {
    }

    public UserReviewModel(String userName, String reviewTime, String reviewDetails, float reviewRating, int userProfile) {
        this.userName = userName;
        this.reviewTime = reviewTime;
        this.reviewDetails = reviewDetails;
        this.reviewRating = reviewRating;
        this.userProfile = userProfile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(String reviewTime) {
        this.reviewTime = reviewTime;
    }

    public String getReviewDetails() {
        return reviewDetails;
    }

    public void setReviewDetails(String reviewDetails) {
        this.reviewDetails = reviewDetails;
    }

    public float getReviewRating() {
        return reviewRating;
    }

    public void setReviewRating(float reviewRating) {
        this.reviewRating = reviewRating;
    }

    public int getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(int userProfile) {
        this.userProfile = userProfile;
    }
}
