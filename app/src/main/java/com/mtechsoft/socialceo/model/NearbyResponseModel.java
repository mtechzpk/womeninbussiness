package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;
import com.mtechsoft.socialceo.model.NearDataModel;

import java.util.List;

public class NearbyResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<NearDataModel> data;

    public NearbyResponseModel(int status, String message, List<NearDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NearDataModel> getData() {
        return data;
    }

    public void setData(List<NearDataModel> data) {
        this.data = data;
    }
}