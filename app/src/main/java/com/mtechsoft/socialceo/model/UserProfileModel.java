package com.mtechsoft.socialceo.model;

public class UserProfileModel {
    private int userProfileImage;

    public UserProfileModel() {
    }

    public UserProfileModel(int userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public int getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(int userProfileImage) {
        this.userProfileImage = userProfileImage;
    }
}
