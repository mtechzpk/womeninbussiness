package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class SinglePostModel {
    @SerializedName("id")
    private int postId;
    @SerializedName("post_type")
    private String post_type;
    @SerializedName("image")
    private String postImage;

    public SinglePostModel() {
    }

    public SinglePostModel(int postId, String post_type, String postImage) {
        this.postId = postId;
        this.post_type = post_type;
        this.postImage = postImage;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }
}
