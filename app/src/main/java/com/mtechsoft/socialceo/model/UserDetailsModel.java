package com.mtechsoft.socialceo.model;

import com.google.gson.annotations.SerializedName;

public class UserDetailsModel {
    @SerializedName("id")
    private int userId;
    @SerializedName("name")
    private String userName;
    @SerializedName("profile_image")
    private String profile_image;
    @SerializedName("token")
    private String token;

    public UserDetailsModel() {
    }

    public UserDetailsModel(int userId, String userName, String profile_image) {
        this.userId = userId;
        this.userName = userName;
        this.profile_image = profile_image;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
