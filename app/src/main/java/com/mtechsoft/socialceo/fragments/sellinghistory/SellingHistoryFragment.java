package com.mtechsoft.socialceo.fragments.sellinghistory;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.BuySellHistoryAdapter;
import com.mtechsoft.socialceo.model.BuySellHistoryModel;

import java.util.ArrayList;
import java.util.List;

public class SellingHistoryFragment extends Fragment {
    private RecyclerView recyclerView_sellingHistory;
    private RecyclerView.Adapter adapter;
    private List<BuySellHistoryModel> listItems;



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_selling_history, container, false);
        recyclerView_sellingHistory = view.findViewById(R.id.recyclerView_sellingHistory);
        showsHistory(recyclerView_sellingHistory);
        return view;
    }



    public void showsHistory(RecyclerView recyclerView){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        listItems = new ArrayList<>();
        for (int i = 0; i <= 3; i++) {
            BuySellHistoryModel listItem = new BuySellHistoryModel();
            listItem.setPrice("150$");
            listItem.setDateTime("Aug 19, 2020");
            listItem.setTitle("Thanks, for this course I will buy it again");
            listItem.setRating("4.5");
            listItem.setImage(R.drawable.post_laptop);
            listItems.add(listItem);
        }
        adapter = new BuySellHistoryAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }
}