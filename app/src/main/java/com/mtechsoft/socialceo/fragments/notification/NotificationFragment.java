package com.mtechsoft.socialceo.fragments.notification;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.NotificationsAdapter;
import com.mtechsoft.socialceo.model.NotificationsModel;

import java.util.ArrayList;
import java.util.List;

public class NotificationFragment extends Fragment {

    private RecyclerView recyclerView_notifications;
    private RecyclerView.Adapter adapter;
    private List<NotificationsModel> listItems;




    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        recyclerView_notifications = view.findViewById(R.id.recyclerView_notifications);
        showNotifications(recyclerView_notifications);
        return view;
    }



    public void showNotifications(RecyclerView recyclerView){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        listItems = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            NotificationsModel listItem = new NotificationsModel();
            listItem.setUserName("Lisa");
            listItem.setUserImage(R.drawable.ariana_user_ic);
            listItem.setNotifcationTime("2h Ago");
            listItem.setNotificationDetails("Just create an event");
            listItems.add(listItem);
        }
        adapter = new NotificationsAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }
}