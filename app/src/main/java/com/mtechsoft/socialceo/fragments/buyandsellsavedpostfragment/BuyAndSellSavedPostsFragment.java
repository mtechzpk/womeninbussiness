package com.mtechsoft.socialceo.fragments.buyandsellsavedpostfragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.BuySellAdapter;
import com.mtechsoft.socialceo.model.BuyAndSellModel;
import com.mtechsoft.socialceo.model.BuySellResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyAndSellSavedPostsFragment extends Fragment {
    private RecyclerView recyclerView_courses;
    private RecyclerView.Adapter adapter;
    GetDataService service;
    String userId;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_buy_and_sell_saved_posts, container, false);

        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        recyclerView_courses = view.findViewById(R.id.recyclerView_courses);
        requestForSavedBuySellPosts();

        return view;
    }


    private void requestForSavedBuySellPosts() {

        Call<BuySellResponseModel> call = service.getSavedBuyAndSellPosts(userId);

        call.enqueue(new Callback<BuySellResponseModel>() {
            @Override
            public void onResponse(Call<BuySellResponseModel> call, Response<BuySellResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                List<BuyAndSellModel> dataList = response.body().getData();

                if (status == 200){
                    showBuySellItems(recyclerView_courses, dataList);
                }
            }

            @Override
            public void onFailure(Call<BuySellResponseModel> call, Throwable t) {

            }
        });
    }
    public void showBuySellItems(RecyclerView recyclerView, List<BuyAndSellModel> listItems){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BuySellAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }

}