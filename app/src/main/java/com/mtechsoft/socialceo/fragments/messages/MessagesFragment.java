package com.mtechsoft.socialceo.fragments.messages;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.SMS.ChatAdaptor;
import com.mtechsoft.socialceo.SMS.Database;
import com.mtechsoft.socialceo.SMS.OnItemClickListener;
import com.mtechsoft.socialceo.SMS.SMSActivity;
import com.mtechsoft.socialceo.SMS.UserChat;
import com.mtechsoft.socialceo.adapters.ActiveMembersAdapter;
import com.mtechsoft.socialceo.adapters.NotificationsAdapter;
import com.mtechsoft.socialceo.model.ActiveUsersResponseModel;
import com.mtechsoft.socialceo.model.NotificationsModel;
import com.mtechsoft.socialceo.model.UserDetailsModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MessagesFragment extends Fragment implements OnItemClickListener {
    private RecyclerView recyclerView_activeMembers, recyclerView_messages;
    private RecyclerView.Adapter adapter;

    private List<NotificationsModel> messageItems;
    private int [] userImages = {R.drawable.ariana_user_ic, R.drawable.maria_alina_user, R.drawable.maria_user_ic, R.drawable.ariana_user_ic, R.drawable.maria_alina_user, R.drawable.maria_user_ic, R.drawable.ariana_user_ic, R.drawable.maria_alina_user, R.drawable.maria_user_ic};
    String userId;
    private List<UserDetailsModel> activeUsersList;
    GetDataService service;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    List<UserChat> list = new ArrayList<>();
    Context context;
    String currnet_user = "";
    RelativeLayout empty_msg;
    ProgressBar progress_bar;
    LinearLayout contextView;
    DatabaseReference reference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_messages, container, false);

        init(view);

        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);


        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        requestForActiveMembers(userId);




        currnet_user = String.valueOf(Utilities.getInt(getContext(), "userId"));
        getUserChats();
        recyclerView_messages.setAdapter(new ChatAdaptor(list, this, context));

        return view;
    }

    private void init(View view) {

        context = getContext();
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        recyclerView_activeMembers = view.findViewById(R.id.recyclerView_activeMembers);
        recyclerView_messages = view.findViewById(R.id.chatsRecyclerView);
        progress_bar =view.findViewById(R.id.progressBar);
        empty_msg = view.findViewById(R.id.empty_msg);
    }


    private void requestForActiveMembers(String userId) {

        Call<ActiveUsersResponseModel> call = service.getAllActiveUsers(userId);

        call.enqueue(new Callback<ActiveUsersResponseModel>() {
            @Override
            public void onResponse(Call<ActiveUsersResponseModel> call, Response<ActiveUsersResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    activeUsersList = response.body().getUserDetails();
                    showActiveMembers(recyclerView_activeMembers, activeUsersList);
                }

            }

            @Override
            public void onFailure(Call<ActiveUsersResponseModel> call, Throwable t) {
            }
        });
    }

    private void showActiveMembers(RecyclerView recyclerView, List<UserDetailsModel> listItems) {
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new ActiveMembersAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View item, int position) {


        String owner_id = list.get(position).getUnique_key();
        String owner_name = list.get(position).getUid();
        String owner_token = list.get(position).getToken();

        Utilities.saveString(context,"rec_id",owner_id);
        Utilities.saveString(context,"rec_name",owner_name);
        Utilities.saveString(context,"rec_token",owner_token);

        Intent chat_intent = new Intent(getContext(), SMSActivity.class);
        startActivity(chat_intent);

        progress_bar.setVisibility(View.GONE);
    }


    private void getUserChats() {

        progress_bar.setVisibility(View.VISIBLE);
        reference = FirebaseDatabase.getInstance().getReference().child(
                Database.NODE_USER_CHATS).child(
                currnet_user);
        reference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    list.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        UserChat message = snapshot.getValue(UserChat.class);
                        message.setKey(snapshot.getKey());
                        list.add(message);
                    }

                    recyclerView_messages.getAdapter().notifyDataSetChanged();
                    if (list.size() > 0) {
                        empty_msg.setVisibility(View.GONE);
                    } else {
                        empty_msg.setVisibility(View.VISIBLE);

                    }
                    progress_bar.setVisibility(View.GONE);
                } else {
                    if (list.size() > 0) {
                        empty_msg.setVisibility(View.GONE);
                    } else {
                        empty_msg.setVisibility(View.VISIBLE);

                    }
                    progress_bar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                progress_bar.setVisibility(View.GONE);

            }

        });


    }

}