package com.mtechsoft.socialceo.fragments.home;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.activities.EventPostActivity;
import com.mtechsoft.socialceo.activities.ImagePickerActivity;
import com.mtechsoft.socialceo.activities.PodCastActivity;
import com.mtechsoft.socialceo.activities.PostForNetworkActivity;
import com.mtechsoft.socialceo.activities.PostForSaleActivity;
import com.mtechsoft.socialceo.activities.StartupPostActivity;
import com.mtechsoft.socialceo.adapters.HomeTabsAdapter;
import com.mtechsoft.socialceo.model.CreatePostResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class HomeFragment extends Fragment {

    private static final String TAG = FragmentActivity.class.getSimpleName ();
    public static final int REQUEST_IMAGE = 100;
    String userId;
    FloatingActionButton floatingActionButton;
    ImageView connectBottomSheetCross, img_PostUpload;
    View connectBottomSheet;
    TabLayout tabLayout;
    ViewPager viewPagerHomeTabs;
    Button btnPost;
    String image_UrlConnect;
    EditText editText_WhatYouSay;
    GetDataService service;
    ProgressDialog progressDialog;
    int currentItem = 0;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach (context);
        getActivity ();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
    }


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate (R.layout.fragment_home, container, false);
        tabLayout = view.findViewById (R.id.home_tabLayout);
        viewPagerHomeTabs = view.findViewById (R.id.viewPagerHomeTabs);
        final HomeTabsAdapter myPagerAdapter = new HomeTabsAdapter
                (getActivity ().getSupportFragmentManager (), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        currentItem = Utilities.getInt (getContext (), "setCurrentPage");
        viewPagerHomeTabs.setAdapter (myPagerAdapter);
        tabLayout.setupWithViewPager (viewPagerHomeTabs);
        setMarginBetweenTabs (tabLayout);
        floatingActionButton = view.findViewById (R.id.btn_floatingActionBar);
        userId = String.valueOf(Utilities.getInt (getContext (), "userId"));

        connectBottomSheetCross = view.findViewById (R.id.cross_btn);
        connectBottomSheet = view.findViewById (R.id.Bottom_Sheet);
        final BottomSheetBehavior<View> behavior = BottomSheetBehavior.from (connectBottomSheet);
        // Initializing Service and Progress BaR
        progressDialog = new ProgressDialog (getContext ());
        progressDialog.setMessage ("Uploading Image for...");
        service = RetrofitClientInstance.getRetrofitInstance ().create (GetDataService.class);
        img_PostUpload = view.findViewById (R.id.img_PostUpload);
        editText_WhatYouSay = view.findViewById (R.id.editText_WhatYousay);
        btnPost = view.findViewById (R.id.btn_Post);

        if (Utilities.getInt (getContext (),"Network")==0) {
            tabLayout.selectTab (tabLayout.getTabAt (0), true);
            Utilities.saveInt (getContext (),"Network",0);
        }
        if (Utilities.getInt (getContext (),"Network")==1) {
            tabLayout.selectTab (tabLayout.getTabAt (1), true);
        Utilities.saveInt (getContext (),"Network",0);
        }

        if (Utilities.getInt (getContext (),"Network")==2) {
            tabLayout.selectTab (tabLayout.getTabAt (2), true);
            Utilities.saveInt (getContext (),"Network",0);
        }
        if (Utilities.getInt (getContext (),"Network")==3) {
            tabLayout.selectTab (tabLayout.getTabAt (3), true);
            Utilities.saveInt (getContext (),"Network",0);
        }
        if (Utilities.getInt (getContext (),"Network")==4) {
            tabLayout.selectTab (tabLayout.getTabAt (4), true);
            Utilities.saveInt (getContext (),"Network",0);
        }
        if (Utilities.getInt (getContext (),"Network")==5) {
            tabLayout.selectTab (tabLayout.getTabAt (5), true);
            Utilities.saveInt (getContext (),"Network",0);
        }


        btnPost.setOnClickListener (v -> {
            progressDialog.show ();
            behavior.setState (BottomSheetBehavior.STATE_COLLAPSED);
            String whatYouSay = editText_WhatYouSay.getText ().toString ();
            if (image_UrlConnect.isEmpty () || whatYouSay.isEmpty ()) {
                Toast.makeText (getContext (), "Please Enter the missing things", Toast.LENGTH_LONG).show ();
            } else {
                postForConnect (userId, whatYouSay, image_UrlConnect, "connect");
                editText_WhatYouSay.setText ("");
            }

        });
        connectBottomSheet.setOnClickListener (v -> {

        });
        floatingActionButton.setOnClickListener (v -> {
            switch (tabLayout.getSelectedTabPosition ()) {
                case 0:

                    if (behavior.getState () == BottomSheetBehavior.STATE_COLLAPSED) {
                        behavior.setState (BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        behavior.setState (BottomSheetBehavior.STATE_COLLAPSED);
                    }

                    break;
                case 1:
                    Intent mainIntent = new Intent (view.getContext (), PostForNetworkActivity.class);
                    view.getContext ().startActivity (mainIntent);
                    break;

                case 2:
                    Intent saleIntent = new Intent (view.getContext (), PostForSaleActivity.class);
                    view.getContext ().startActivity (saleIntent);
                    break;
                case 3:
                    Intent eventIntent = new Intent (view.getContext (), EventPostActivity.class);
                    view.getContext ().startActivity (eventIntent);
                    break;
                case 4:
                    Intent podCastIntent = new Intent (view.getContext (), PodCastActivity.class);
                    view.getContext ().startActivity (podCastIntent);
                    break;
                default:
            }
        });

        connectBottomSheetCross.setOnClickListener (v -> behavior.setState (BottomSheetBehavior.STATE_COLLAPSED));

        viewPagerHomeTabs.addOnPageChangeListener (new ViewPager.OnPageChangeListener () {
            public void onPageScrollStateChanged(int state) {
                behavior.setState (BottomSheetBehavior.STATE_COLLAPSED);
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
            }
        });


        img_PostUpload.setOnClickListener (v ->
                Dexter.withContext (getActivity ())
                        .withPermissions (Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener (new MultiplePermissionsListener () {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted ()) {
                                    showImagePickerOptions ();
                                }

                                if (report.isAnyPermissionPermanentlyDenied ()) {
                                    showSettingsDialog ();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest ();
                            }
                        }).check ());
        return view;
    }
    public void setMarginBetweenTabs(TabLayout tabLayout) {
        ViewGroup tabs = (ViewGroup) tabLayout.getChildAt(0);

        for (int i = 0; i < tabs.getChildCount() - 1; i++) {
            View tab = tabs.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            layoutParams.setMarginEnd(15);
            tab.setLayoutParams(layoutParams);
            tabLayout.requestLayout();
        }
    }

    private void loadPhotos(String url, Bitmap bitmap) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(img_PostUpload);
        img_PostUpload.setColorFilter(ContextCompat.getColor(getActivity(), android.R.color.transparent));

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Uri uri = data.getParcelableExtra("path");
                String imageEncoded, input;
                Bitmap bitmapImage = null;
                // You can update this bitmap to your server
                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                assert bitmapImage != null;
                bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                input = imageEncoded;
                input = input.replace("\n", "");
                input = input.trim();
                input = "data:image/png;base64,"+input;
                // loading profile image from local cache
                assert uri != null;
                loadPhotos(uri.toString(), bitmapImage);
                image_UrlConnect = input;
            }
        }
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(getActivity(), new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void launchGalleryIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void postForConnect(String userId, String description, String image_UrlConnect, String post_Type) {

        Call<CreatePostResponseModel> call = service.createPostForConnect( userId,description, image_UrlConnect, post_Type);

        call.enqueue(new Callback<CreatePostResponseModel>() {
            @Override
            public void onResponse(Call<CreatePostResponseModel> call, Response<CreatePostResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    Toast.makeText(getContext(),  "Successfully Posted", Toast.LENGTH_LONG).show();
                }
                else if (status == 400){
                    Toast.makeText(getContext(),  response.body().getMessage()  + "Failed" , Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<CreatePostResponseModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });
    }
}