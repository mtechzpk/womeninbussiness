package com.mtechsoft.socialceo.fragments.reviews;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.UserReviewAdapter;
import com.mtechsoft.socialceo.model.UserReviewModel;

import java.util.ArrayList;
import java.util.List;

public class ReviewsFragment extends Fragment {

    private RecyclerView recyclerView_userReview;
    private RecyclerView.Adapter adapter;
    private List<UserReviewModel> listItems;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_reviews, container, false);
        recyclerView_userReview = view.findViewById(R.id.recyclerView_userReview);
        showUserReviews(recyclerView_userReview);
        return view;
    }


    public void showUserReviews(RecyclerView recyclerView){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        listItems = new ArrayList<>();
        for (int i = 0; i <= 3; i++) {
            UserReviewModel listItem = new UserReviewModel();
            listItem.setUserName("Lisa");
            listItem.setUserProfile(R.drawable.ariana_user_ic);
            listItem.setReviewRating(5.0f);
            listItem.setReviewTime("2h Ago");
            listItem.setReviewDetails("Thanks, for this course I will buy it again");
            listItems.add(listItem);
        }
        adapter = new UserReviewAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }
}