package com.mtechsoft.socialceo.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.ConnectPostsAdapter;
import com.mtechsoft.socialceo.adapters.PodCastAdapter;
import com.mtechsoft.socialceo.model.ConnectPostModel;
import com.mtechsoft.socialceo.model.ConnectPostResponseModel;
import com.mtechsoft.socialceo.model.GetPodCastDataModel;
import com.mtechsoft.socialceo.model.GetPodCastResponseModel;
import com.mtechsoft.socialceo.model.PodCastResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PodCastSavedPostFragment extends Fragment {

    private RecyclerView recyclerView_userPosts;
    private RecyclerView.Adapter adapter;
    GetDataService service;
    String userId;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_connect_saved_post, container, false);
        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        recyclerView_userPosts = view.findViewById(R.id.recyclerView_userPosts);
        requestForSavedConnectedPosts(userId);

        return view;
    }


    private void requestForSavedConnectedPosts(String userId) {

        Call<GetPodCastResponseModel> call = service.gteSavedPodCastPost(userId);

        call.enqueue(new Callback<GetPodCastResponseModel>() {
            @Override
            public void onResponse(Call<GetPodCastResponseModel> call, Response<GetPodCastResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                List<GetPodCastDataModel> dataList = response.body().getData();

                if (status == 200){
                    showUserPosts(recyclerView_userPosts, dataList);
                }
            }

            @Override
            public void onFailure(Call<GetPodCastResponseModel> call, Throwable t) {
            }
        });
    }
    public void showUserPosts(RecyclerView recyclerView, List<GetPodCastDataModel> listItems){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new PodCastAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }
}