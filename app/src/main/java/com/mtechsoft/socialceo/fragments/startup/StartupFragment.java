package com.mtechsoft.socialceo.fragments.startup;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.StartupAdapter;
import com.mtechsoft.socialceo.model.StartupDataModel;
import com.mtechsoft.socialceo.model.StartupResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartupFragment extends Fragment {
    private RecyclerView recyclerView_Startups;
    private RecyclerView.Adapter adapter;
    GetDataService service;
    String userId;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_startups, container, false);

        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        recyclerView_Startups = view.findViewById(R.id.recyclerView_Startups);
        requestForStartUpPosts(userId);
        return view;
    }


    private void requestForStartUpPosts(String userId) {

        Call<StartupResponseModel> call = service.getStartupPosts(userId);

        call.enqueue(new Callback<StartupResponseModel>() {
            @Override
            public void onResponse(Call<StartupResponseModel> call, Response<StartupResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                List<StartupDataModel> dataList = response.body().getData();
                if (status == 200){
                    showStartUps(recyclerView_Startups, dataList);
                }
            }

            @Override
            public void onFailure(Call<StartupResponseModel> call, Throwable t) {
            }
        });
    }
    public void showStartUps(RecyclerView recyclerView, List<StartupDataModel> listItems){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new StartupAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }
}