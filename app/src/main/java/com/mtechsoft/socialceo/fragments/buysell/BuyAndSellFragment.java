package com.mtechsoft.socialceo.fragments.buysell;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.everseat.rangeseekbar.RangeSeekbar;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.BuySellAdapter;
import com.mtechsoft.socialceo.model.BuyAndSellModel;
import com.mtechsoft.socialceo.model.BuySellResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.GPSTracker;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BuyAndSellFragment extends Fragment {
    private RecyclerView recyclerView_courses;
    private RecyclerView.Adapter adapter;
    GetDataService service;
    View filterBottomSheet;
    ImageView img_Filter,  FilterBottomSheetCross;
    String userId;
    RadioGroup radioGroup_Categories;
    String cat;
    CheckBox free;
    CrystalRangeSeekbar price_seekbar;
    String min,max,is_free;
    Button btn_Apply;
    ProgressDialog progressDialog;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_buy_and_sell, container, false);
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        recyclerView_courses = view.findViewById(R.id.recyclerView_courses);
        radioGroup_Categories = view.findViewById(R.id.radioGroup_Categories_filter);
        free = view.findViewById(R.id.checkbox_Free);
        price_seekbar = view.findViewById(R.id.price_seek_bar);
        btn_Apply = view.findViewById(R.id.btn_Apply);
        requestForBuySellPosts();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading");




        img_Filter = view.findViewById(R.id.img_Filter);
        FilterBottomSheetCross = view.findViewById(R.id.cross_btn);
        filterBottomSheet = view.findViewById(R.id.BuySell_Filter);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(filterBottomSheet);
        filterBottomSheet.setOnClickListener(v -> {
            return;
        });

        img_Filter.setOnClickListener(v -> {
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
        });

        FilterBottomSheetCross.setOnClickListener(v -> behavior.setState(BottomSheetBehavior.STATE_COLLAPSED));



        radioGroup_Categories.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = view.findViewById(checkedId);
                cat = radioButton.getText().toString();
                Toast.makeText(getContext(), cat+"", Toast.LENGTH_SHORT).show();
            }
        });

        price_seekbar.setOnRangeSeekbarChangeListener (new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minNumber, Number maxNumber) {

                min = String.valueOf(minNumber);
                max = String.valueOf(maxNumber);
            }
        });



        btn_Apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (free.isChecked())
                {
                   is_free = "true";
                }
                else
                {
                    is_free = "false";
                }

                progressDialog.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<BuySellResponseModel> call = service.buyAndSellfilter(userId,"buy_and_sell",cat,is_free,max,min);
                call.enqueue(new Callback<BuySellResponseModel>() {
                    @Override
                    public void onResponse(Call<BuySellResponseModel> call, Response<BuySellResponseModel> response) {
                        assert response.body() != null;
                        int status = response.body().getStatus();
                        if(status==200)
                        {
                            progressDialog.dismiss();
                            List<BuyAndSellModel> dataList = response.body().getData();
                            showfilterItem(recyclerView_courses,dataList);
                            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<BuySellResponseModel> call, Throwable t) {

                        progressDialog.dismiss();
                        t.printStackTrace();
                    }
                });
            }
        });


        return view;
    }

    private void showfilterItem(RecyclerView recyclerView_courses, List<BuyAndSellModel> dataList) {

        recyclerView_courses.setHasFixedSize(true);
        recyclerView_courses.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BuySellAdapter(dataList, getContext());
        recyclerView_courses.setAdapter(adapter);

    }


    private void requestForBuySellPosts() {

        Call<BuySellResponseModel> call = service.getBuyAndSellPosts(userId);

        call.enqueue(new Callback<BuySellResponseModel>() {
            @Override
            public void onResponse(Call<BuySellResponseModel> call, Response<BuySellResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                List<BuyAndSellModel> dataList = response.body().getData();

                if (status == 200){
                    showBuySellItems(recyclerView_courses, dataList);
                }
            }

            @Override
            public void onFailure(Call<BuySellResponseModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
    public void showBuySellItems(RecyclerView recyclerView, List<BuyAndSellModel> listItems){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BuySellAdapter(listItems,getContext());
        recyclerView.setAdapter(adapter);
    }
}