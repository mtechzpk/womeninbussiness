package com.mtechsoft.socialceo.fragments.events;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.EventsAdapter;
import com.mtechsoft.socialceo.model.EventsDataModel;
import com.mtechsoft.socialceo.model.EventsResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventsFragment extends Fragment {

    private RecyclerView recyclerView_events;
    private RecyclerView.Adapter adapter;
    View EventsBottomSheet;
    ImageView img_Filter,  FilterBottomSheetCross;
    GetDataService service;
    String userId;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);

        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        recyclerView_events = view.findViewById(R.id.recyclerView_Events);
        requestForEventPosts(userId);

        img_Filter = view.findViewById(R.id.img_Filter);
        FilterBottomSheetCross = view.findViewById(R.id.cross_btn);
        EventsBottomSheet = view.findViewById(R.id.Events_Filter);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(EventsBottomSheet);
        EventsBottomSheet.setOnClickListener(v -> {
            return;
        });
        img_Filter.setOnClickListener(v -> {
            if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
            else {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        FilterBottomSheetCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
        return view;
    }


    private void requestForEventPosts(String userId) {

        Call<EventsResponseModel> call = service.getEventPosts(userId);

        call.enqueue(new Callback<EventsResponseModel>() {
            @Override
            public void onResponse(Call<EventsResponseModel> call, Response<EventsResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                List<EventsDataModel> dataList = response.body().getData();
                if (status == 200){
                    showEvents(recyclerView_events, dataList);
                }
            }

            @Override
            public void onFailure(Call<EventsResponseModel> call, Throwable t) {
            }
        });
    }
    public void showEvents(RecyclerView recyclerView, List<EventsDataModel> listItems){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new EventsAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }
}