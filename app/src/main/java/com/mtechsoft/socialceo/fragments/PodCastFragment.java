package com.mtechsoft.socialceo.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.EventsAdapter;
import com.mtechsoft.socialceo.adapters.PodCastAdapter;
import com.mtechsoft.socialceo.model.EventsDataModel;
import com.mtechsoft.socialceo.model.EventsResponseModel;
import com.mtechsoft.socialceo.model.GetPodCastDataModel;
import com.mtechsoft.socialceo.model.GetPodCastResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PodCastFragment extends Fragment {

    private RecyclerView recyclerView_events;
    private RecyclerView.Adapter adapter;
    View EventsBottomSheet;
    ImageView img_Filter,  FilterBottomSheetCross;
    GetDataService service;
    String userId;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pod_cast, container, false);

        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        recyclerView_events = view.findViewById(R.id.recyclerView_pod);
        requestForEventPosts(userId);

        img_Filter = view.findViewById(R.id.img_Filter);
        FilterBottomSheetCross = view.findViewById(R.id.cross_btn);
        EventsBottomSheet = view.findViewById(R.id.Events_Filter);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(EventsBottomSheet);
        EventsBottomSheet.setOnClickListener(v -> {
            return;
        });
        img_Filter.setOnClickListener(v -> {
            if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
            else {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        FilterBottomSheetCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
        return view;
    }


    private void requestForEventPosts(String userId) {

        Call<GetPodCastResponseModel> call = service.get_podcast_posts(userId);

        call.enqueue(new Callback<GetPodCastResponseModel>() {
            @Override
            public void onResponse(Call<GetPodCastResponseModel> call, Response<GetPodCastResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                List<GetPodCastDataModel> dataList = response.body().getData();
                if (status == 200){
                    showEvents(recyclerView_events, dataList);
                }
            }

            @Override
            public void onFailure(Call<GetPodCastResponseModel> call, Throwable t) {
            }
        });
    }
    public void showEvents(RecyclerView recyclerView, List<GetPodCastDataModel> listItems){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new PodCastAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }
}