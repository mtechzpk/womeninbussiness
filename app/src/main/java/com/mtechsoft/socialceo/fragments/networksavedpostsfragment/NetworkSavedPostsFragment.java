package com.mtechsoft.socialceo.fragments.networksavedpostsfragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.UserNetworkPostsAdapter;
import com.mtechsoft.socialceo.model.NetworkPostModel;
import com.mtechsoft.socialceo.model.NetworkResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkSavedPostsFragment extends Fragment {
    private RecyclerView recyclerView_userPosts;
    private RecyclerView.Adapter adapter;
    ProgressDialog progressDialog;
    GetDataService service;
    String userId;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_network_saved_posts, container, false);
        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        // Service  Here
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading....");

        recyclerView_userPosts = view.findViewById(R.id.recyclerView_userPosts);
        requestForNetworkPosts(recyclerView_userPosts);

        return view;
    }

    private void requestForNetworkPosts(RecyclerView recyclerView_userPosts) {
        progressDialog.show();
        Call<NetworkResponseModel> call = service.getSavedNetworkPosts(userId);
        call.enqueue(new Callback<NetworkResponseModel>() {

            @Override
            public void onResponse(Call<NetworkResponseModel> call, Response<NetworkResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                List<NetworkPostModel> dataList = response.body().getData();
                if (status == 200){
                    showUserPosts(recyclerView_userPosts, dataList);
                }
            }
            @Override
            public void onFailure(Call<NetworkResponseModel> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
    public void showUserPosts(RecyclerView recyclerView, List<NetworkPostModel> listItems){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new UserNetworkPostsAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }
}