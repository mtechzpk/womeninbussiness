package com.mtechsoft.socialceo.fragments.connect;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.activities.ConnectPostActivity;
import com.mtechsoft.socialceo.adapters.ConnectPostsAdapter;
import com.mtechsoft.socialceo.model.ConnectPostModel;
import com.mtechsoft.socialceo.model.ConnectPostResponseModel;
import com.mtechsoft.socialceo.model.LikeResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectFragment extends Fragment implements ConnectPostsAdapter.CallBack {
    private RecyclerView recyclerView_userPosts;
    private RecyclerView.Adapter adapter;
    GetDataService service;
    String userId;
    List<ConnectPostModel> dataList;
    String fav_status = "true";

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_connect, container, false);
        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        recyclerView_userPosts = view.findViewById(R.id.recyclerView_userPosts);
        requestForConnectPosts(userId);
        return view;
    }

    private void requestForConnectPosts(String userId) {

        Call<ConnectPostResponseModel> call = service.getConnectPosts(userId);

        call.enqueue(new Callback<ConnectPostResponseModel>() {
            @Override
            public void onResponse(Call<ConnectPostResponseModel> call, Response<ConnectPostResponseModel> response) {
                assert response.body() != null;
                int status = response.body().getStatus();
                dataList = response.body().getData();

                if (status == 200) {
                    showUserPosts(recyclerView_userPosts, dataList);
                }
            }

            @Override
            public void onFailure(Call<ConnectPostResponseModel> call, Throwable t) {
            }
        });
    }

    public void showUserPosts(RecyclerView recyclerView, List<ConnectPostModel> listItems) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ConnectPostsAdapter(listItems, getContext(), ConnectFragment.this);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onLikePost(int pos, TextView likes, ImageView heart_ic) {


        String like = String.valueOf(dataList.get(pos).getTotal_likes());


        Call<LikeResponseModel> call = service.likePost(userId, String.valueOf(dataList.get(pos).getPostId()));
        call.enqueue(new Callback<LikeResponseModel>() {
            @Override
            public void onResponse(Call<LikeResponseModel> call, Response<LikeResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    Toast.makeText(getContext(),response.body().getMessage() , Toast.LENGTH_LONG).show();

                    if (response.body().getData().equals("liked")){
                        likes.setText(String.valueOf(Integer.parseInt(like)+1));
                        heart_ic.setImageResource(R.drawable.heart_filled_ic);

                        }
                    else {
                        heart_ic.setImageResource(R.drawable.heart_outline_ic);
                        likes.setText(String.valueOf(Integer.parseInt(like)));

                    }
                    Utilities.saveString(getContext(),"likes",like);

                }
            }

            @Override
            public void onFailure(Call<LikeResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });


    }
}