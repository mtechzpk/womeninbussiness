package com.mtechsoft.socialceo.fragments.search;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.ActiveMembersAdapter;
import com.mtechsoft.socialceo.adapters.MemberNearAdapter;
import com.mtechsoft.socialceo.adapters.UpComingEventsResponseModel;
import com.mtechsoft.socialceo.adapters.UpcomingEventsAdapter;
import com.mtechsoft.socialceo.model.ActiveUsersResponseModel;
import com.mtechsoft.socialceo.model.NearDataModel;
import com.mtechsoft.socialceo.model.UpcomingEventsModel;
import com.mtechsoft.socialceo.model.UserDetailsModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.model.NearbyResponseModel;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.GPSTracker;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {

    private RecyclerView recyclerView_membersNear, recyclerView_activeMembers, recyclerView_upcomingEvents;
    private RecyclerView.Adapter adapter;
    private List<NearDataModel> nearUserlist;
    private List<UserDetailsModel> activeUsersList;
    private List<UpcomingEventsModel> upcomingEventsList;

    GetDataService service;
    String userId,latitude,longitude,country,city,postalCode,address;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search, container, false);

        GPSTracker gpsTracker = new GPSTracker(getActivity());
        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
            country = gpsTracker.getCountryName(getActivity());
            postalCode = gpsTracker.getPostalCode(getActivity());
            city = gpsTracker.getLocality(getActivity());
            address = gpsTracker.getAddressLine(getActivity());

            Utilities.saveString(getActivity(),"latitude_near",latitude);
            Utilities.saveString(getActivity(),"longitude_near",longitude);

            Toast.makeText(getActivity(), latitude+" "+longitude ,Toast.LENGTH_SHORT).show();
        }
        else{
            gpsTracker.showSettingsAlert();
        }

        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        userId = String.valueOf(Utilities.getInt(getActivity(), "userId"));

        recyclerView_membersNear = view.findViewById(R.id.recyclerView_membersNear);
        recyclerView_activeMembers = view.findViewById(R.id.recyclerView_activeMembers);
        requestForActiveMembers(userId);

        recyclerView_upcomingEvents = view.findViewById(R.id.recyclerView_upcomingEvents);
        requestForUpComingEvents();

      shownearUsers(userId,latitude,longitude);
        return view;
    }

    private void shownearUsers(String userId, String latitude, String longitude) {

        Call<NearbyResponseModel> call = service.getallNearByUser(userId,latitude,longitude);
        call.enqueue(new Callback<NearbyResponseModel>() {
            @Override
            public void onResponse(Call<NearbyResponseModel> call, Response<NearbyResponseModel> response) {
                assert response.body() != null;
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200){
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    nearUserlist = response.body().getData();
                    showNearMembers(recyclerView_membersNear,nearUserlist);
                }
            }

            @Override
            public void onFailure(Call<NearbyResponseModel> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void showNearMembers(RecyclerView recyclerView, List<NearDataModel> dataModelList) {
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new MemberNearAdapter(dataModelList, getContext());
        recyclerView.setAdapter(adapter);
    }


    private void requestForActiveMembers(String userId) {
        Call<ActiveUsersResponseModel> call = service.getAllActiveUsers(userId);

        call.enqueue(new Callback<ActiveUsersResponseModel>() {
            @Override
            public void onResponse(Call<ActiveUsersResponseModel> call, Response<ActiveUsersResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    activeUsersList = response.body().getUserDetails();
                    showActiveMembers(recyclerView_activeMembers, activeUsersList);
                }
            }

            @Override
            public void onFailure(Call<ActiveUsersResponseModel> call, Throwable t) {
            }
        });
    }

    private void showActiveMembers(RecyclerView recyclerView, List<UserDetailsModel> listItems) {
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new ActiveMembersAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }


    private void requestForUpComingEvents() {
        Call<UpComingEventsResponseModel> call = service.getUpComingEvents();

        call.enqueue(new Callback<UpComingEventsResponseModel>() {
            @Override
            public void onResponse(Call<UpComingEventsResponseModel> call, Response<UpComingEventsResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    upcomingEventsList = response.body().getData();
                    showUpcomingEvents(recyclerView_upcomingEvents, upcomingEventsList);
                }
            }

            @Override
            public void onFailure(Call<UpComingEventsResponseModel> call, Throwable t) {
            }
        });
    }

    private void showUpcomingEvents(RecyclerView recyclerView, List<UpcomingEventsModel> listItems) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new UpcomingEventsAdapter(listItems, getContext());
        recyclerView.setAdapter(adapter);
    }
}