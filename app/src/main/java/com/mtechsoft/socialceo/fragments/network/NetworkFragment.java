package com.mtechsoft.socialceo.fragments.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mtechsoft.socialceo.R;
import com.mtechsoft.socialceo.adapters.UserNetworkPostsAdapter;
import com.mtechsoft.socialceo.model.LikeResponseModel;
import com.mtechsoft.socialceo.model.NetworkPostModel;
import com.mtechsoft.socialceo.model.NetworkResponseModel;
import com.mtechsoft.socialceo.network.GetDataService;
import com.mtechsoft.socialceo.network.RetrofitClientInstance;
import com.mtechsoft.socialceo.utilities.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkFragment extends Fragment implements UserNetworkPostsAdapter.CallBack {
    private RecyclerView recyclerView_userPosts;
    private RecyclerView.Adapter adapter;
    ProgressDialog progressDialog;
    GetDataService service;
    List<NetworkPostModel> dataList;
    String userId;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_net_work, container, false);

        userId = String.valueOf(Utilities.getInt(getContext(), "userId"));
        // Service  Here
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading....");
        recyclerView_userPosts = view.findViewById(R.id.recyclerView_userPosts);
        requestForNetworkPosts(recyclerView_userPosts);
        return view;
    }

    private void requestForNetworkPosts(RecyclerView recyclerView_userPosts) {
        progressDialog.show();
        Call<NetworkResponseModel> call = service.getNetworkPosts(userId);
        call.enqueue(new Callback<NetworkResponseModel>() {

            @Override
            public void onResponse(Call<NetworkResponseModel> call, Response<NetworkResponseModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                int status  = response.body().getStatus();
                 dataList = response.body().getData();
                if (status == 200){
                    showUserPosts(recyclerView_userPosts, dataList);
                }
            }
            @Override
            public void onFailure(Call<NetworkResponseModel> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
    public void showUserPosts(RecyclerView recyclerView, List<NetworkPostModel> listItems){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new UserNetworkPostsAdapter(listItems, getContext(),NetworkFragment.this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onLikePost(int pos, TextView likes, ImageView heart_ic) {
        String like = String.valueOf(dataList.get(pos).getTotal_likes());


        Call<LikeResponseModel> call = service.likePost(userId, String.valueOf(dataList.get(pos).getPostId()));
        call.enqueue(new Callback<LikeResponseModel>() {
            @Override
            public void onResponse(Call<LikeResponseModel> call, Response<LikeResponseModel> response) {
                assert response.body() != null;
                int status  = response.body().getStatus();
                if (status == 200){
                    if (response.body().getData().equals("liked")){
                        likes.setText(String.valueOf(Integer.parseInt(like)+1));
                        heart_ic.setImageResource(R.drawable.heart_filled_ic);

                    }
                    else {
                        heart_ic.setImageResource(R.drawable.heart_outline_ic);
                        likes.setText(String.valueOf(Integer.parseInt(like)));
                    }

                }
            }

            @Override
            public void onFailure(Call<LikeResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });


    }
}